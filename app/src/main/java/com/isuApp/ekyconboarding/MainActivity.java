package com.isuApp.ekyconboarding;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.isu.ekyc.checkStatus.commonOnboarding.OnBoardingEntry;
import com.isu.ekyc.utils.EkycSdkConstants;
import com.isuApp.ekyconboarding.databinding.ActivityMainBinding;
import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        EkycSdkConstants.TYPE = "";
        binding.edit.setText("krutitest");
        binding.run.setOnClickListener(v -> intentToEkycSdk());
    }

    private void intentToEkycSdk() {
        String username =binding.edit.getText().toString();
        if (!username.isEmpty()){
            EkycSdkConstants.ADMIN_NAME = "cscw297";
            EkycSdkConstants.USER_NAME = username;
            EkycSdkConstants.PACKAGE_NAME = "com.isu.iserveuprotech";
            EkycSdkConstants.USER_MOBILE_NO = "7809704443";
            EkycSdkConstants.NAME = "Ankita Nayak";
            EkycSdkConstants.USER_EMAIL = "ankita78@yopmail.com";
            EkycSdkConstants.TYPE = "";
            com.isu.ekyc.utils.SessionManager sessionManager;
            sessionManager = new com.isu.ekyc.utils.SessionManager(MainActivity.this);
            EkycSdkConstants.EkycStatusCode="";
            if (binding.token.getText().toString().isEmpty()) {
                sessionManager.createLoginSession("eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWRpcmVjdFVyaSI6Imh0dHBzOi8vZGFzaGJvYXJkLXJvb3BhbnRlci53ZWIuYXBwIiwicHJpdmlsZWdlcyI6WyJUUkFOU0FDVElPTiJdLCJiYW5rQ29kZSI6ImNzYyIsInVzZXJfbmFtZSI6ImtydXRpdGVzdCIsIm1vYmlsZU51bWJlciI6Nzg3MzgwMTQ2OCwiY3JlYXRlZCI6MTcwODkyNzA0ODE0NCwiYXV0aG9yaXRpZXMiOlsiUk9MRV9SRVRBSUxFUiJdLCJjbGllbnRfaWQiOiJjc2NhZG0tb2F1dGgyLWNsaWVudCIsImFkbWluTmFtZSI6ImNzY3cyOTciLCJpc1Bhc3N3b3JkUmVzZXRSZXF1aXJlZCI6ZmFsc2UsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJleHAiOjE3MDg5Mjg4NDgsImp0aSI6IjA2Mzc5OTMxLTQ1MjMtNGUxZi1iMjliLWVjYzYxYWIyYTg2MyJ9.WowOJfkmUJ0-x7m2mMRPh6ny-qZZvbIVYwsbgiS6EVgibGjlIt56pZVsAKagG-kQwBFcAXMMjuI3cyOtjxio5FdT0YlG5C2goIAhQYtlWeGBwBPBwDP7wGS4x1CKueb2AD6jI0e2B6q1oki9PfyiHp0D9jx1lIjrRu0Riy3w6l3TaYzK8WoFwms0HIK_BTjA0xTpr5WRsWttkhGacyS8LeykehJCIJz52IzizxdpemBXs7gqQG1oqwhQ4QVYVhb7iJt-JKiFwc82Z73Clk3j4qp9HUXQqb112pynMXgVyXq7qE4E_EtkAs1YUh13gVj67RtxJlb54UMh9A8ePGwLZg"
                        , EkycSdkConstants.ADMIN_NAME);
            }else {
                sessionManager.createLoginSession(binding.token.getText().toString(), EkycSdkConstants.ADMIN_NAME);
            }
            Intent intent = new Intent(this, OnBoardingEntry.class);
            startActivity(intent);
        }else {
            Toast.makeText(this, "Please enter username.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding = null;
    }
}