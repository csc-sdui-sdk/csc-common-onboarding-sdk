package com.isuApp.ekyconboarding;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;


//import com.isu.ekyc.CommonOnboardActivity;
//import com.isu.ekyc.posEkyc.OnboardActivity;
//import com.isu.ekyc.roomDbForUrl.newroomdatabase.UrlViewModelFactory;
//import com.isu.ekyc.roomDbForUrl.newroomdatabase.entity.UrlTable;
//import com.isu.ekyc.roomDbForUrl.newroomdatabase.repository.UrlRepository;
//import com.isu.ekyc.roomDbForUrl.newroomdatabase.viewmodel.UrlViewModel;
//import com.isu.ekyc.utils.EkycSdkConstants;
import com.isuApp.ekyconboarding.databinding.FragmentFirstBinding;

public class FirstFragment extends Fragment {

    private FragmentFirstBinding binding;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentFirstBinding.inflate(inflater, container, false);


        return binding.getRoot();


    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.buttonFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkCameraPermission();
                intentToSignUpSdk();
            }
        });

    }

    private void intentToEkycSdk() {

//
//        com.isu.ekyc.utils.SessionManager sessionManager;
//
//        Intent intent = new Intent(requireActivity(), CommonOnboardActivity.class);
//
//        startActivity(intent);
    }

    private void intentToSignUpSdk() {

//
//        EkycSdkConstants.TYPE="NEW";
//
//
//        com.isu.ekyc.utils.SessionManager sessionManager;
//
//        Intent intent = new Intent(requireActivity(), OnboardActivity.class);
//
//        startActivity(intent);
    }

    private void checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.CAMERA}, 2);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}