package com.isu.ekyc.otpverification.readsms;

/**
 * @author Nitish
 *
 */
public interface SMSListener {
    void onSuccess(String message);

    void onError(String message);
}
