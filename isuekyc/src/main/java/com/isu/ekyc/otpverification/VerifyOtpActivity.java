package com.isu.ekyc.otpverification;

import static com.isu.ekyc.utils.CSCStringConstants.SUB_STATUS_1;
import static com.isu.ekyc.utils.CSCStringConstants.SUB_STATUS_2;
import static com.isu.ekyc.utils.EkycSdkConstants.sResendOtpUrl;
import static com.isu.ekyc.utils.EkycSdkConstants.sSendEmailUrl;
import static com.isu.ekyc.utils.EkycSdkConstants.sVerifyEmailUrl;
import static com.isu.ekyc.utils.EkycSdkConstants.sVerifyOtpUrl;
import static com.isu.ekyc.utils.Utils.connectionCheck;
import static com.isu.ekyc.utils.Utils.exitTheProcess;
import static com.isu.ekyc.utils.Utils.handleErrorResponse;
import static com.isu.ekyc.utils.Utils.hideKeyboard;
import static com.isu.ekyc.utils.Utils.hideProgressDialog;
import static com.isu.ekyc.utils.Utils.showErrorAlert;
import static com.isu.ekyc.utils.Utils.showProgressDialog;
import static com.isu.ekyc.utils.Utils.showToast;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;
import com.isu.ekyc.R;
import com.isu.ekyc.checkStatus.api.OnboardApiService;
import com.isu.ekyc.checkStatus.api.RetrofitInstance;
import com.isu.ekyc.checkStatus.model.EmailRequest;
import com.isu.ekyc.checkStatus.model.EmailResponseModel;
import com.isu.ekyc.checkStatus.model.phone.MessageData;
import com.isu.ekyc.checkStatus.model.phone.PhoneResponseModel;
import com.isu.ekyc.checkStatus.model.phone.SendOtpRequestModel;
import com.isu.ekyc.checkStatus.model.phone.VerifyOTPRequestModel;
import com.isu.ekyc.configuration.OnBoardSharedPreference;
import com.isu.ekyc.configuration.Validate;
import com.isu.ekyc.databinding.ActivityVerifyOtpBinding;
import com.isu.ekyc.ekycDataBase.EkycDataTable;
import com.isu.ekyc.ekycDataBase.EkycViewModelFactory;
import com.isu.ekyc.ekycDataBase.UserEkycRepository;
import com.isu.ekyc.ekycDataBase.viewmodel.UserEkycViewModel;
import com.isu.ekyc.otpverification.readsms.SMSBroadcastReceiver;
import com.isu.ekyc.utils.CSCStringConstants;
import com.isu.ekyc.utils.EkycSdkConstants;
import com.isu.ekyc.utils.OnboardingIntentFactory;
import com.isu.ekyc.utils.SessionManager;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * <b>VerifyOtpActivity</b> this activity verifies otp send to both mobile number and email id
 */
public class VerifyOtpActivity extends AppCompatActivity {

    private static final String TAG = VerifyOtpActivity.class.getSimpleName();

    ActivityVerifyOtpBinding binding;

    private int mCounter = 30;

    private static final int ALLOWED = 30;

    private boolean mFirstResend = true;

    private String mMobileNumber;

    private String mOtp;

    private String mEmail;


    private OnBoardSharedPreference preference;

    private boolean mOwn = true;

    private String mUserName = "";

    private String mUsername;

    private String mAdminName;

    private String mParams;

    private EkycDataTable ekycDataTable;

    private UserEkycViewModel userEkycViewModel;
    private String mToken = "";

    private SessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Button btnSubmit;
        super.onCreate(savedInstanceState);
        binding = ActivityVerifyOtpBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        mToken = user.get(SessionManager.KEY_TOKEN);


        initUserConsentApi();

        mParams = getIntent().getStringExtra("params");
        ekycDataTable = new EkycDataTable();
        UserEkycRepository userEkycRepository = new UserEkycRepository(getApplication());
        userEkycViewModel = new ViewModelProvider(this, new EkycViewModelFactory(userEkycRepository)).get(UserEkycViewModel.class);
        if (userEkycViewModel.getAllData() != null) {
            ekycDataTable = userEkycViewModel.getAllData();
        }

        mUserName = EkycSdkConstants.USER_NAME;
        binding.edtcOtpp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int len = s.length();
                if (len == 6) {
                    hideKeyboard(VerifyOtpActivity.this, binding.edtcOtpp);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                mOtp = s.toString();
            }
        });

        mUsername = EkycSdkConstants.USER_NAME;
        mAdminName = EkycSdkConstants.ADMIN_NAME;
        binding.tvResend.setOnClickListener(v -> {
            hideKeyboard(this, v);
            if (Boolean.TRUE.equals(connectionCheck(this))) {
                mOtp = "";
                if (mMobileNumber != null) {
                    binding.edtcOtpp.setText(mOtp);
                    resendOtp(mMobileNumber, mUsername, mAdminName, mParams);
                } else if (mEmail != null) {
                    binding.edtcOtpp.setText(mOtp);
                    resendEmail(mUserName, mEmail);
                }
            }
        });

        btnSubmit = findViewById(R.id.login_proceed);
        btnSubmit.setOnClickListener(v -> {
            hideKeyboard(this, v);
            if (Boolean.TRUE.equals(connectionCheck(this))) {
                if (mEmail == null) {
                    submitOtpForVerification();
                } else {
                    submitEmailOtpForVerification();
                }
            }
        });
        if (getIntent() != null && getIntent().hasExtra("data")) {
            mMobileNumber = getIntent().getStringExtra("data");
            binding.tvMobile.setText(mMobileNumber);
        }
        if (getIntent() != null && getIntent().hasExtra("email")) {
            mEmail = getIntent().getStringExtra("email");
            binding.tvOtpMessage.setText(R.string.sdk_email_otp_message);
            binding.tvMobile.setText(mEmail);
        }
        statTimer();

        //On back pressed handled
        OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                exitTheProcess(VerifyOtpActivity.this, getString(R.string.sdk_do_you_want_to_exit));
            }
        };
        // Add the callback to the onBackPressedDispatcher
        getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback);
    }

    private void initUserConsentApi() {

        SMSBroadcastReceiver.initSMSListener(null);    // making SMS Retriever API disable
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        registerReceiver(smsVerificationReceiver, intentFilter);
        SmsRetriever.getClient(this).startSmsUserConsent(null);
    }

    private final BroadcastReceiver smsVerificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
                Bundle extras = intent.getExtras();
                Status smsRetrieverStatus = (Status) extras.get(SmsRetriever.EXTRA_STATUS);
                switch (smsRetrieverStatus.getStatusCode()) {
                    case CommonStatusCodes.SUCCESS:
                        // Get consent intent
                        Intent consentIntent = extras.getParcelable(SmsRetriever.EXTRA_CONSENT_INTENT);
                        try {
                            // Start activity to show consent dialog to user, activity must be started in
                            // 5 minutes, otherwise you'll receive another TIMEOUT intent
                            if (mStartForResult != null) {
                                mStartForResult.launch(consentIntent);
                            }
                        } catch (ActivityNotFoundException e) {
                            // Handle the exception ...
                        }
                        break;
                    case CommonStatusCodes.TIMEOUT:
                        // Time out occurred, handle the error.
                        break;
                }
            }
        }
    };
    ActivityResultLauncher<Intent> mStartForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
        @Override
        public void onActivityResult(ActivityResult result) {
            if (result.getResultCode() == Activity.RESULT_OK) {
                Intent intent = result.getData();
                // Handle the Intent
                String message = null;
                if (intent != null) {
                    message = intent.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                }
                // Extract one-time code from the message and complete verification
                // `sms` contains the entire text of the SMS message, so you will need
                // to parse the string.
                if (message != null) {
                    binding.edtcOtpp.setText(getOtpFromMessage(message));
                }
            }
        }
    });

    private String getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{6}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            return matcher.group(0);
        } else {
            return "";
        }
    }

    /**
     * submit otp for verification
     */
    private void submitOtpForVerification() {
        if (mOtp != null && !mOtp.isEmpty()) {
            verifyOtp(mMobileNumber, mOtp, mParams);
        } else {
            showErrorAlert(VerifyOtpActivity.this, getString(R.string.sdk_enter_otp));
        }
    }

    /**
     * submit email otp for verification
     */
    private void submitEmailOtpForVerification() {
        if (mOtp != null && !mOtp.isEmpty()) {
            verifyEmail(mUserName, mOtp, mToken);
        } else {
            showErrorAlert(VerifyOtpActivity.this, getString(R.string.sdk_enter_otp));
        }
    }


    /**
     * start otp waiting time period to activate resend button
     */

    private void statTimer() {
        if (mFirstResend) {
            mFirstResend = false;
            binding.tvResend.setVisibility(View.VISIBLE);
        }

        binding.tvResend.setEnabled(false);
        binding.tvResend.setTextColor(getResources().getColor(R.color.sign_up_pan_text));
        binding.tvResend.setAlpha(0.5f);
        mCounter = 0;

        new CountDownTimer(ALLOWED * 1000L, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int time = ALLOWED - mCounter;
                binding.tvTimer.setText(time + " Seconds");
                mCounter++;
            }

            @Override
            public void onFinish() {
                binding.tvResend.setEnabled(true);
                binding.tvResend.setTextColor(getResources().getColor(R.color.sign_up_button));
                binding.tvResend.setAlpha(1.0f);
                binding.tvTimer.setText(0 + " Seconds");
            }
        }.start();
    }

    /**
     * This method is used to resend mobile otp
     *
     * @param mobileNumber This mobile number has been displayed on the screen
     * @param username     User name used to login
     * @param adminName    Api user name used as admin name
     * @param params       unique params used for each send otp
     */
    private void resendOtp(String mobileNumber, String username, String adminName, String params) {
        showProgressDialog(VerifyOtpActivity.this, getString(R.string.sdk_please_wait));
        MessageData messageData = new MessageData();
        messageData.setMessage1("");
        messageData.setMessage2("");
        messageData.setMessage3("");
        messageData.setMessageOtp("");
        SendOtpRequestModel sendOtpRequestModel = new SendOtpRequestModel();
        sendOtpRequestModel.setUserName(username);
        sendOtpRequestModel.setAdminName(adminName);
        sendOtpRequestModel.setFeatureName(CSCStringConstants.OTP_CAPS);
        sendOtpRequestModel.setOperationPerformed(CSCStringConstants.OTP_CAPS);
        sendOtpRequestModel.setMobileNumber(mobileNumber);
        sendOtpRequestModel.setEmail("");
        sendOtpRequestModel.setWhatsappMobile("");
        sendOtpRequestModel.setParams(params);
        sendOtpRequestModel.setMessageData(messageData);

        OnboardApiService onboardApiService = RetrofitInstance.retrofit().create(OnboardApiService.class);
        Call<PhoneResponseModel> getOtp = onboardApiService.sendOTP(sendOtpRequestModel, sResendOtpUrl);
        getOtp.enqueue(new Callback<PhoneResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<PhoneResponseModel> call, @NonNull Response<PhoneResponseModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    hideProgressDialog();
                    int status = response.body().getStatus();
                    String message = response.body().getMessage();
                    if (status == 1) {
                        statTimer();
                        showToast(VerifyOtpActivity.this, message);
                    } else {
                        showToast(VerifyOtpActivity.this, message);
                    }
                } else {
                    hideProgressDialog();
                    showToast(VerifyOtpActivity.this, getString(R.string.sdk_please_wait));
                }
            }

            @Override
            public void onFailure(@NonNull Call<PhoneResponseModel> call, @NonNull Throwable t) {
                hideProgressDialog();
                showToast(VerifyOtpActivity.this, getString(R.string.sdk_please_wait));
            }
        });

    }

    /**
     * Re-send otp to email api call
     */
    private void resendEmail(String userName, String emailID) {
        showProgressDialog(VerifyOtpActivity.this, getString(R.string.sdk_please_wait));
        EmailRequest emailRequest = new EmailRequest();
        OnboardApiService onboardApiService = RetrofitInstance.retrofit().create(OnboardApiService.class);
        emailRequest.setEmail(emailID);
        emailRequest.setUsername(userName);

        Call<EmailResponseModel> sendEmail = onboardApiService.sendEmail(emailRequest, mToken, sSendEmailUrl);
        sendEmail.enqueue(new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<EmailResponseModel> call, @NonNull Response<EmailResponseModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    hideProgressDialog();
                    int status = response.body().getStatus();
                    String message = response.body().getStatusDesc();
                    if (status == 0) {
                        statTimer();
                        showToast(VerifyOtpActivity.this, getString(R.string.sdk_resend_otp));
                    } else {
                        showToast(VerifyOtpActivity.this, message);
                    }
                } else {
                    hideProgressDialog();
                    handleErrorResponse(VerifyOtpActivity.this,response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<EmailResponseModel> call, @NonNull Throwable t) {
                hideProgressDialog();
                showToast(VerifyOtpActivity.this, getString(R.string.went_wrong));
            }
        });
    }

    /**
     * To verify mobile otp
     */
    private void verifyOtp(String mobileNumber, String otp, String params) {
        showProgressDialog(VerifyOtpActivity.this, getString(R.string.sdk_please_wait));
        VerifyOTPRequestModel requestModel = new VerifyOTPRequestModel();
        requestModel.setMobileNo(mobileNumber);
        requestModel.setOtp(otp);
        requestModel.setParams(params);

        OnboardApiService onboardApiService = RetrofitInstance.retrofit().create(OnboardApiService.class);
        Call<PhoneResponseModel> verify = onboardApiService.verifyOTP(requestModel, sVerifyOtpUrl);
        verify.enqueue(new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<PhoneResponseModel> call, @NonNull Response<PhoneResponseModel> response) {
                hideProgressDialog();
                if (response.isSuccessful() && response.body() != null) {
                    int status = response.body().getStatus();
                    String message = response.body().getMessage();
                    if (status == 1) {
                        ekycDataTable.setType(CSCStringConstants.EXISTING);
                        ekycDataTable.setPhone(mMobileNumber);
                        ekycDataTable.setPhoneOtp(mOtp);
                        ekycDataTable.setSubStatus(SUB_STATUS_1);
                        userEkycViewModel.update(ekycDataTable);
                        // Phone number verified. Go to EmailVerificationActivity
                        Intent intent = OnboardingIntentFactory.returnEmailVerificationActivityIntent(VerifyOtpActivity.this);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    } else {
                        showToast(VerifyOtpActivity.this, message);
                    }
                } else {
                    showToast(VerifyOtpActivity.this, getString(R.string.incorrect_otp));
                }
            }

            @Override
            public void onFailure(@NonNull Call<PhoneResponseModel> call, @NonNull Throwable t) {
                hideProgressDialog();
                showToast(VerifyOtpActivity.this, getString(R.string.went_wrong));
            }
        });
    }

    /**
     * To verify email otp
     */
    private void verifyEmail(String userName, String emailOTP, String token) {
        showProgressDialog(VerifyOtpActivity.this, getString(R.string.sdk_please_wait));
        EmailRequest otpRequest = new EmailRequest();
        otpRequest.setOtp(emailOTP);
        otpRequest.setUsername(userName);
        OnboardApiService onboardApiService = RetrofitInstance.retrofit().create(OnboardApiService.class);
        Call<EmailResponseModel> verifyOtp = onboardApiService.verifyEmail(otpRequest, token, sVerifyEmailUrl);
        verifyOtp.enqueue(new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<EmailResponseModel> call, @NonNull Response<EmailResponseModel> response) {
                hideProgressDialog();
                if (response.isSuccessful() && response.body() != null) {
                    int status = response.body().getStatus();
                    String message = response.body().getStatusDesc();
                    if (status == 0) {
                        ekycDataTable.setEmail(mEmail);
                        ekycDataTable.setEmailOtp(emailOTP);
                        ekycDataTable.setSubStatus(SUB_STATUS_2);
                        userEkycViewModel.update(ekycDataTable);
                        // Email verified. Go to WelcomeActivity
                        Intent intent = OnboardingIntentFactory.returnWelcomeActivityIntent(VerifyOtpActivity.this);
                        startActivity(intent);
                        finish();
                    } else {
                        showToast(VerifyOtpActivity.this, message);
                    }
                } else {
                    handleErrorResponse(VerifyOtpActivity.this, response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<EmailResponseModel> call, @NonNull Throwable t) {
                hideProgressDialog();
                showToast(VerifyOtpActivity.this, getString(R.string.went_wrong));
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mStartForResult = null;
    }

}