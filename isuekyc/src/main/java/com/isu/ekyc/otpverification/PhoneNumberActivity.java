package com.isu.ekyc.otpverification;

import static com.isu.ekyc.utils.EkycSdkConstants.sSendOtpUrl;
import static com.isu.ekyc.utils.Utils.connectionCheck;
import static com.isu.ekyc.utils.Utils.exitTheProcess;
import static com.isu.ekyc.utils.Utils.hideKeyboard;
import static com.isu.ekyc.utils.Utils.hideProgressDialog;
import static com.isu.ekyc.utils.Utils.showErrorAlert;
import static com.isu.ekyc.utils.Utils.showProgressDialog;
import static com.isu.ekyc.utils.Utils.showToast;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.isu.ekyc.R;
import com.isu.ekyc.checkStatus.api.OnboardApiService;
import com.isu.ekyc.checkStatus.api.RetrofitInstance;
import com.isu.ekyc.checkStatus.model.phone.MessageData;
import com.isu.ekyc.checkStatus.model.phone.PhoneResponseModel;
import com.isu.ekyc.checkStatus.model.phone.SendOtpRequestModel;
import com.isu.ekyc.configuration.Validate;
import com.isu.ekyc.ekycDataBase.EkycDataTable;
import com.isu.ekyc.ekycDataBase.EkycViewModelFactory;
import com.isu.ekyc.ekycDataBase.UserEkycRepository;
import com.isu.ekyc.ekycDataBase.viewmodel.UserEkycViewModel;
import com.isu.ekyc.utils.CSCStringConstants;
import com.isu.ekyc.utils.EkycSdkConstants;
import com.isu.ekyc.utils.OnboardingIntentFactory;

import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * <b>PhoneNumberActivity</b> This activity validates mobile number,
 * mobile number is not an editable field.
 * This number is passed from parent application.
 */
public class PhoneNumberActivity extends AppCompatActivity {

    private String mMobile;
    private String mUsername;
    private String mAdminName;
    private UserEkycViewModel userEkycViewModel;
    private EkycDataTable ekycDataTable;
    private String mSaltStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_otp);
        UserEkycRepository userEkycRepository = new UserEkycRepository(getApplication());
        userEkycViewModel = new ViewModelProvider(this, new EkycViewModelFactory(userEkycRepository)).get(UserEkycViewModel.class);
        ekycDataTable = new EkycDataTable();
        ekycDataTable = userEkycViewModel.getAllData();
        init();
        getTransactionID();

        //On back pressed handled
        OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                exitTheProcess(PhoneNumberActivity.this, getString(R.string.sdk_do_you_want_to_exit));
            }
        };
        // Add the callback to the onBackPressedDispatcher
        getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback);
    }

    /**
     * to generate random params for send otp to mobile
     */
    private void getTransactionID() {
        Random random = new Random();
        String randomNumber = String.valueOf(8 + random.nextInt(CSCStringConstants.PARAM_STRING.length() - 8));
        StringBuilder salt = new StringBuilder();
        while (salt.length() < Integer.parseInt(randomNumber)) { // length of the random string.
            int index = (int) (random.nextFloat() * CSCStringConstants.PARAM_STRING.length());
            salt.append(CSCStringConstants.PARAM_STRING.charAt(index));
        }
        mSaltStr = salt.toString();
    }


    public void init() {
        EditText mobileET;
        Button proceed = findViewById(R.id.login_proceed);
        mobileET = findViewById(R.id.login_mobile);
        String mUserMobileNo = EkycSdkConstants.USER_MOBILE_NO;
        mobileET.setText(mUserMobileNo);

        proceed.setOnClickListener(view -> {
            if (Boolean.TRUE.equals(connectionCheck(this))) {
                mMobile = mobileET.getText().toString();
                if (Validate.isValidPhone(mMobile)) {
                    hideKeyboard(PhoneNumberActivity.this,view);
                    mUsername = EkycSdkConstants.USER_NAME;
                    mAdminName = EkycSdkConstants.ADMIN_NAME;
                    mMobile = Validate.getValidPhone(mMobile);
                    sendOTP(mMobile, mUsername, mAdminName, mSaltStr);
                } else {
                    hideKeyboard(PhoneNumberActivity.this,view);
                    showErrorAlert(PhoneNumberActivity.this, getString(R.string.sdk_error_mobile_number));
                }
            }
        });
    }

    /**
     * This method is to send otp to mobile number displayed on the screen
     * @param mobile  This mobile number has been displayed on the screen
     * @param username  User name used to login
     * @param adminName  Api user name used as admin name
     * @param params  unique params used for each send otp
     */
    private void sendOTP(String mobile, String username, String adminName, String params) {
        showProgressDialog(PhoneNumberActivity.this,getString(R.string.sdk_please_wait));
        MessageData messageData = new MessageData();
        messageData.setMessage1("");
        messageData.setMessage2("");
        messageData.setMessage3("");
        messageData.setMessageOtp("");
        SendOtpRequestModel sendOtpRequestModel = new SendOtpRequestModel();
        sendOtpRequestModel.setUserName(username);
        sendOtpRequestModel.setAdminName(adminName);
        sendOtpRequestModel.setFeatureName(CSCStringConstants.OTP_CAPS);
        sendOtpRequestModel.setOperationPerformed(CSCStringConstants.OTP_CAPS);
        sendOtpRequestModel.setMobileNumber(mobile);
        sendOtpRequestModel.setEmail("");
        sendOtpRequestModel.setWhatsappMobile("");
        sendOtpRequestModel.setParams(params);
        sendOtpRequestModel.setMessageData(messageData);

        OnboardApiService onboardApiService = RetrofitInstance.retrofit().create(OnboardApiService.class);
        Call<PhoneResponseModel> getOtp = onboardApiService.sendOTP(sendOtpRequestModel,sSendOtpUrl);
        getOtp.enqueue(new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<PhoneResponseModel> call, @NonNull Response<PhoneResponseModel> response) {
                if (response.isSuccessful() && response.body() != null ) {
                    hideProgressDialog();
                    int status = response.body().getStatus();
                    if (status == 1) {
                        Intent intent = OnboardingIntentFactory.returnVerifyOtpActivityIntent(PhoneNumberActivity.this);
                        intent.putExtra(CSCStringConstants.PARAMS, mSaltStr);
                        intent.putExtra(CSCStringConstants.DATA, mMobile);
                        startActivity(intent);
                        finish();
                    } else {
                        showToast(PhoneNumberActivity.this, getString(R.string.sdk_try_again));
                    }
                } else {
                    hideProgressDialog();
                    showToast(PhoneNumberActivity.this, getString(R.string.sdk_please_try_again));
                }
            }

            @Override
            public void onFailure(@NonNull Call<PhoneResponseModel> call, @NonNull Throwable t) {
                hideProgressDialog();
                showToast(PhoneNumberActivity.this, getString(R.string.sdk_please_try_again));
            }
        });

    }
}