package com.isu.ekyc.otpverification;


import static com.isu.ekyc.utils.Utils.exitTheProcess;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;

import com.isu.ekyc.R;
import com.isu.ekyc.configuration.Validate;
import com.isu.ekyc.posEkyc.EkycAgreementActivity;
import com.isu.ekyc.utils.OnboardingIntentFactory;

/**
 * <b>WelcomeActivity</b> this activity gives a overview of kyc process.
 */
public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        init();
        //On back pressed handled
        OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                exitTheProcess(WelcomeActivity.this, getString(R.string.sdk_do_you_want_to_exit));
            }
        };
        // Add the callback to the onBackPressedDispatcher
        getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback);
    }

    private void init() {
        Button proceed = findViewById(R.id.btn_start_kyc);
        proceed.setOnClickListener(view -> startKycFromLocalDB());
    }

    void startKycFromLocalDB() {
        Intent i = OnboardingIntentFactory.returnEkycAgreementActivityIntent(WelcomeActivity.this);
        startActivity(i);
        finish();
    }

}

