package com.isu.ekyc.emailverfication;

import static com.isu.ekyc.utils.CSCStringConstants.EMAIL_SMALL;
import static com.isu.ekyc.utils.CSCStringConstants.SUB_STATUS_2;
import static com.isu.ekyc.utils.EkycSdkConstants.sSendEmailUrl;
import static com.isu.ekyc.utils.Utils.connectionCheck;
import static com.isu.ekyc.utils.Utils.exitTheProcess;
import static com.isu.ekyc.utils.Utils.handleErrorResponse;
import static com.isu.ekyc.utils.Utils.hideKeyboard;
import static com.isu.ekyc.utils.Utils.hideProgressDialog;
import static com.isu.ekyc.utils.Utils.showErrorAlert;
import static com.isu.ekyc.utils.Utils.showProgressDialog;
import static com.isu.ekyc.utils.Utils.showToast;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.isu.ekyc.R;
import com.isu.ekyc.checkStatus.api.OnboardApiService;
import com.isu.ekyc.checkStatus.api.RetrofitInstance;
import com.isu.ekyc.checkStatus.model.EmailRequest;
import com.isu.ekyc.checkStatus.model.EmailResponseModel;
import com.isu.ekyc.configuration.Validate;
import com.isu.ekyc.ekycDataBase.EkycDataTable;
import com.isu.ekyc.ekycDataBase.EkycViewModelFactory;
import com.isu.ekyc.ekycDataBase.UserEkycRepository;
import com.isu.ekyc.ekycDataBase.viewmodel.UserEkycViewModel;
import com.isu.ekyc.utils.EkycSdkConstants;
import com.isu.ekyc.utils.OnboardingIntentFactory;
import com.isu.ekyc.utils.SessionManager;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * <b>EmailVerificationActivity</b> is a optional step in KYC process,
 * here one can skip the process and send otp to verify it.
 */
public class EmailVerificationActivity extends AppCompatActivity {
    private Button btnProceed;
    private TextView tvSkip;
    private EditText edtEmail;
    private String email;
    private String username = "";
    private EkycDataTable ekycDataTable;
    private UserEkycViewModel userEkycViewModel;
    private String _token = "";
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_verification);
        initializeUI();

        session = new SessionManager(EmailVerificationActivity.this);
        HashMap<String, String> user = session.getUserDetails();

        _token = user.get(SessionManager.KEY_TOKEN);
        ekycDataTable = new EkycDataTable();
        email = EkycSdkConstants.USER_EMAIL;
        edtEmail.setText(email);
        UserEkycRepository userEkycRepository = new UserEkycRepository(getApplication());
        userEkycViewModel = new ViewModelProvider(this, new EkycViewModelFactory(userEkycRepository)).get(UserEkycViewModel.class);
        if (userEkycViewModel.getAllData() != null) {
            ekycDataTable = userEkycViewModel.getAllData();
        }
        username = EkycSdkConstants.USER_NAME;

        tvSkip.setOnClickListener(v -> handleSkipAction());

        btnProceed.setOnClickListener(v -> {
            if (Boolean.TRUE.equals(connectionCheck(this))) {
                email = edtEmail.getText().toString();
                if (Validate.isValidMail(email)) {
                    //send otp method to be change and additional method to be added to presenter for email
                    hideKeyboard(this, v);
                    sendEmail(username, email);
                } else {
                    hideKeyboard(this, v);
                    showErrorAlert(EmailVerificationActivity.this, getString(R.string.sdk_error_email_address));
                }
            }
        });

        //On back pressed handled
        OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                exitTheProcess(EmailVerificationActivity.this, getString(R.string.sdk_do_you_want_to_exit));
            }
        };
        // Add the callback to the onBackPressedDispatcher
        getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback);

    }


    /**
     * As per new requirements user skip email verification.
     * redirect user to welcome screen.
     */
    private void handleSkipAction() {
        ekycDataTable.setSubStatus(SUB_STATUS_2);
        userEkycViewModel.update(ekycDataTable);
        Intent intent = OnboardingIntentFactory.returnWelcomeActivityIntent(EmailVerificationActivity.this);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    /**
     * bind all the view here,
     * we need to bind the view using view binding from core but this is in future scope.
     */
    private void initializeUI() {
        btnProceed = findViewById(R.id.login_proceed);
        tvSkip = findViewById(R.id.btn_skip);
        edtEmail = findViewById(R.id.login_mobile);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    /**
     * Send otp to email api call
     */
    private void sendEmail(String userName, String emailID) {
        showProgressDialog(EmailVerificationActivity.this, getString(R.string.sdk_please_wait));
        EmailRequest emailRequest = new EmailRequest();
        OnboardApiService onboardApiService = RetrofitInstance.retrofit().create(OnboardApiService.class);
        emailRequest.setEmail(emailID);
        emailRequest.setUsername(userName);

        Call<EmailResponseModel> sendEmail = onboardApiService.sendEmail(emailRequest, _token, sSendEmailUrl);
        sendEmail.enqueue(new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<EmailResponseModel> call, @NonNull Response<EmailResponseModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    hideProgressDialog();
                    int status = response.body().getStatus();
                    String message = response.body().getStatusDesc();
                    if (status == 0) {
                        Intent intent = OnboardingIntentFactory.returnVerifyOtpActivityIntent(EmailVerificationActivity.this);
                        intent.putExtra(EMAIL_SMALL, emailID);
                        startActivity(intent);
                        finish();
                    } else {
                        showToast(EmailVerificationActivity.this, message);
                    }
                } else {
                    hideProgressDialog();
                    handleErrorResponse(EmailVerificationActivity.this,response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<EmailResponseModel> call, @NonNull Throwable t) {
                hideProgressDialog();
                showToast(EmailVerificationActivity.this, getString(R.string.went_wrong));
            }
        });
    }


}