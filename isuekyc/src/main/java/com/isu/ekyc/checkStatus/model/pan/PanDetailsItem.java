package com.isu.ekyc.checkStatus.model.pan;

import com.google.gson.annotations.SerializedName;
import com.isu.ekyc.utils.CSCStringConstants;

public class PanDetailsItem{
	@SerializedName(CSCStringConstants.FIRST_NAME)
	private String firstName;
	@SerializedName(CSCStringConstants.MIDDLE_NAME)
	private String middleName;
	@SerializedName(CSCStringConstants.LAST_UPDATE_DATE)
	private String lastUpdateDate;
	@SerializedName(CSCStringConstants.PAN_NUMBER)
	private String pan;
	@SerializedName(CSCStringConstants.LAST_NAME)
	private String lastName;

	public String getFirstName() {
		return firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public String getLastUpdateDate() {
		return lastUpdateDate;
	}

	public String getPan() {
		return pan;
	}

	public String getLastName() {
		return lastName;
	}
}