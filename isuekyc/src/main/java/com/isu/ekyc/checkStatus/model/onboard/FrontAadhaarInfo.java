package com.isu.ekyc.checkStatus.model.onboard;

import com.google.gson.annotations.SerializedName;

public class FrontAadhaarInfo {

	@SerializedName("mother")
	private String mother;

	@SerializedName("pin")
	private String pin;

	@SerializedName("gender")
	private String gender;

	@SerializedName("dob")
	private String dob;

	@SerializedName("father")
	private String father;

	@SerializedName("name")
	private String name;

	@SerializedName("aadhar")
	private String aadhar;

	@SerializedName("husband")
	private String husband;

	@SerializedName("yob")
	private String yob;

	public void setMother(String mother) {
		this.mother = mother;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public void setFather(String father) {
		this.father = father;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAadhar(String aadhar) {
		this.aadhar = aadhar;
	}

	public void setHusband(String husband) {
		this.husband = husband;
	}

	public void setYob(String yob) {
		this.yob = yob;
	}
}