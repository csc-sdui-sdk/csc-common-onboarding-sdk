package com.isu.ekyc.checkStatus.commonOnboarding.onboarding;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("ui_json")
	private UiJson uiJson;

	@SerializedName("version")
	private String version;

	public UiJson getUiJson(){
		return uiJson;
	}

	public String getVersion(){
		return version;
	}
}