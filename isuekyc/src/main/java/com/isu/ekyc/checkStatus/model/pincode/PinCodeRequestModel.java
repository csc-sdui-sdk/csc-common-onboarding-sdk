package com.isu.ekyc.checkStatus.model.pincode;

import com.google.gson.annotations.SerializedName;
import com.isu.ekyc.utils.CSCStringConstants;

public class PinCodeRequestModel{
	@SerializedName(CSCStringConstants.PIN)
	private Integer pin;

	public void setPin(Integer pin) {
		this.pin = pin;
	}
}