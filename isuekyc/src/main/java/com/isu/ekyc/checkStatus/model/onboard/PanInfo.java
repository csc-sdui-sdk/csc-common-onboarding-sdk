package com.isu.ekyc.checkStatus.model.onboard;

import com.google.gson.annotations.SerializedName;

public class PanInfo{

	@SerializedName("date_of_issue")
	private String dateOfIssue;

	@SerializedName("dob")
	private String dob;

	@SerializedName("father")
	private String father;

	@SerializedName("name")
	private String name;

	@SerializedName("pan")
	private String pan;

	public void setDateOfIssue(String dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public void setFather(String father) {
		this.father = father;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}
}