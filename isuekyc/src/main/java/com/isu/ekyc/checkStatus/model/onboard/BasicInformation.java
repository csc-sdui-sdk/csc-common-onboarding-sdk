package com.isu.ekyc.checkStatus.model.onboard;

import com.google.gson.annotations.SerializedName;

public class BasicInformation {

	@SerializedName("income")
	private String income;

	@SerializedName("pincode")
	private String pincode;

	@SerializedName("address")
	private String address;

	@SerializedName("districtCode")
	private String districtCode;

	@SerializedName("city")
	private String city;

	@SerializedName("ShopAddress")
	private String shopAddress;

	@SerializedName("gstNo")
	private String gstNo;

	@SerializedName("shopName")
	private String shopName;

	@SerializedName("ofac")
	private String ofac;

	@SerializedName("long")
	private String longitude;

	@SerializedName("ShopImage")
	private String shopImage;

	@SerializedName("ShopPin")
	private String shopPin;

	@SerializedName("district")
	private String district;

	@SerializedName("shop_type")
	private String shopType;

	@SerializedName("name")
	private String name;

	@SerializedName("stateCode")
	private String stateCode;

	@SerializedName("state")
	private String state;

	@SerializedName("Merchant_type")
	private String merchantType;

	@SerializedName("lat")
	private String latitude;

	public void setIncome(String income) {
		this.income = income;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}

	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public void setOfac(String ofac) {
		this.ofac = ofac;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public void setShopImage(String shopImage) {
		this.shopImage = shopImage;
	}

	public void setShopPin(String shopPin) {
		this.shopPin = shopPin;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public void setShopType(String shopType) {
		this.shopType = shopType;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setMerchantType(String merchantType) {
		this.merchantType = merchantType;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
}