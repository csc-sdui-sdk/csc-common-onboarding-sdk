package com.isu.ekyc.checkStatus.model.onboard;

import com.google.gson.annotations.SerializedName;

public class PanDetails {

	@SerializedName("panImage")
	private String panImage;

	@SerializedName("panInfo")
	private PanInfo panInfo;

	@SerializedName("pan")
	private String pan;

	public void setPanImage(String panImage) {
		this.panImage = panImage;
	}

	public void setPanInfo(PanInfo panInfo) {
		this.panInfo = panInfo;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}
}