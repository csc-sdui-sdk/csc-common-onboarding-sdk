package com.isu.ekyc.checkStatus.model.phone;

import com.google.gson.annotations.SerializedName;
import com.isu.ekyc.utils.CSCStringConstants;

public class MessageData{

	@SerializedName(CSCStringConstants.SUB_STATUS_1)
	private String message1;

	@SerializedName(CSCStringConstants.SUB_STATUS_2)
	private String message2;

	@SerializedName(CSCStringConstants.SUB_STATUS_3)
	private String message3;

	@SerializedName(CSCStringConstants.OTP_SMALL)
	private String messageOtp;

	public void setMessage1(String message1) {
		this.message1 = message1;
	}

	public void setMessage2(String message2) {
		this.message2 = message2;
	}

	public void setMessage3(String message3) {
		this.message3 = message3;
	}

	public void setMessageOtp(String messageOtp) {
		this.messageOtp = messageOtp;
	}
}