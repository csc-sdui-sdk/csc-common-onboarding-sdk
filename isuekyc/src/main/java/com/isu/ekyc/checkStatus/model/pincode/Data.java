package com.isu.ekyc.checkStatus.model.pincode;

import com.google.gson.annotations.SerializedName;
import com.isu.ekyc.utils.CSCStringConstants;

public class Data{

	@SerializedName(CSCStringConstants.STATUS_DESC)
	private String statusDesc;

	@SerializedName(CSCStringConstants.DATA)
	private Data data;

	@SerializedName(CSCStringConstants.STATUS)
	private String status;

	@SerializedName(CSCStringConstants.STATE_NAME)
	private String stateName;

	@SerializedName(CSCStringConstants.CITY)
	private String city;

	public String getStatusDesc() {
		return statusDesc;
	}

	public Data getData() {
		return data;
	}

	public String getStatus() {
		return status;
	}

	public String getStateName() {
		return stateName;
	}

	public String getCity() {
		return city;
	}
}