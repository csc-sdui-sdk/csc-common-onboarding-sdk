package com.isu.ekyc.checkStatus.model.phone;

import com.google.gson.annotations.SerializedName;
import com.isu.ekyc.utils.CSCStringConstants;

public class SendOtpRequestModel {

	@SerializedName(CSCStringConstants.MOBILE)
	private String mobileNumber;

	@SerializedName(CSCStringConstants.EMAIL_REQUEST)
	private String email;

	@SerializedName(CSCStringConstants.FEATURE_NAME)
	private String featureName;

	@SerializedName(CSCStringConstants.USER_NAME)
	private String userName;

	@SerializedName(CSCStringConstants.WHATS_APP_MOBILE)
	private String whatsappMobile;

	@SerializedName(CSCStringConstants.ADMIN_NAME)
	private String adminName;

	@SerializedName(CSCStringConstants.PARAMS)
	private String params;

	@SerializedName(CSCStringConstants.OPERATION_PERFORMED)
	private String operationPerformed;

	@SerializedName(CSCStringConstants.MESSAGE_DATA)
	private MessageData messageData;

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFeatureName(String featureName) {
		this.featureName = featureName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setWhatsappMobile(String whatsappMobile) {
		this.whatsappMobile = whatsappMobile;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public void setParams(String params) {
		this.params = params;
	}

	public void setOperationPerformed(String operationPerformed) {
		this.operationPerformed = operationPerformed;
	}

	public void setMessageData(MessageData messageData) {
		this.messageData = messageData;
	}
}