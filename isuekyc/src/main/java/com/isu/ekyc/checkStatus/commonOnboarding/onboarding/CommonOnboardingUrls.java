package com.isu.ekyc.checkStatus.commonOnboarding.onboarding;

import com.google.gson.annotations.SerializedName;

public class CommonOnboardingUrls{

	@SerializedName("newsignup")
	private String newsignup;

	@SerializedName("existingPhonevalidateAPI")
	private String existingPhonevalidateAPI;

	@SerializedName("existingonboardApi")
	private String existingonboardApi;

	@SerializedName("newvalidatepan")
	private String newvalidatepan;

	@SerializedName("existingverifyMail")
	private String existingverifyMail;

	@SerializedName("newPhonevalidateAPI")
	private String newPhonevalidateAPI;

	@SerializedName("newsendMail")
	private String newsendMail;

	@SerializedName("existingPhonerequestOPTService")
	private String existingPhonerequestOPTService;

	@SerializedName("existingvalidatepan")
	private String existingvalidatepan;

	@SerializedName("newverifyMail")
	private String newverifyMail;

	@SerializedName("newPhonerequestOPTService")
	private String newPhonerequestOPTService;

	@SerializedName("existingsendEmail")
	private String existingsendEmail;

	@SerializedName("existingfetchstatus")
	private String existingfetchstatus;

	@SerializedName("existingPhoneResendOtp")
	private String existingPhoneResendOtp;

	public String getUpdateDetails() {
		return updateDetails;
	}

	@SerializedName("updateDetails")
	private String updateDetails;

	@SerializedName("existingvalidatepincode")
	private String existingvalidatepincode;

	@SerializedName("fetchLocationUrl")
	private String fetchLocationUrl;

	@SerializedName("newvalidatepincode")
	private String newvalidatepincode;

	public String getNewvalidatepincode() {
		return newvalidatepincode;
	}

	public String getExistingvalidatepincode() {
		return existingvalidatepincode;
	}

	public String getExistingPhoneResendOtp() {
		return existingPhoneResendOtp;
	}

	public String getExistingfetchstatus() {
		return existingfetchstatus;
	}

	public String getNewsignup(){
		return newsignup;
	}

	public String getExistingPhonevalidateAPI(){
		return existingPhonevalidateAPI;
	}

	public String getExistingonboardApi(){
		return existingonboardApi;
	}

	public String getNewvalidatepan(){
		return newvalidatepan;
	}

	public String getExistingverifyMail(){
		return existingverifyMail;
	}

	public String getNewPhonevalidateAPI(){
		return newPhonevalidateAPI;
	}

	public String getNewsendMail(){
		return newsendMail;
	}

	public String getFetchLocationUrl() {
		return fetchLocationUrl;
	}

	public String getExistingPhonerequestOPTService(){
		return existingPhonerequestOPTService;
	}

	public String getExistingvalidatepan(){
		return existingvalidatepan;
	}

	public String getNewverifyMail(){
		return newverifyMail;
	}

	public String getNewPhonerequestOPTService(){
		return newPhonerequestOPTService;
	}

	public String getExistingsendEmail(){
		return existingsendEmail;
	}
}