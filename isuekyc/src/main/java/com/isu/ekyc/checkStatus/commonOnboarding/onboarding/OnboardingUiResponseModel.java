package com.isu.ekyc.checkStatus.commonOnboarding.onboarding;

import com.google.gson.annotations.SerializedName;

public class OnboardingUiResponseModel{

	@SerializedName("data")
	private Data data;

	@SerializedName("status")
	private int status;

	public Data getData(){
		return data;
	}

	public int getStatus(){
		return status;
	}

}