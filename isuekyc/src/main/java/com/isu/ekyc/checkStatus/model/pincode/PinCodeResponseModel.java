package com.isu.ekyc.checkStatus.model.pincode;

import com.google.gson.annotations.SerializedName;
import com.isu.ekyc.utils.CSCStringConstants;

public class PinCodeResponseModel{

	@SerializedName(CSCStringConstants.DATA)
	private Data data;

	@SerializedName(CSCStringConstants.STATUS_CODE)
	private Integer statusCode;

	public Data getData() {
		return data;
	}

	public Integer getStatusCode() {
		return statusCode;
	}
}