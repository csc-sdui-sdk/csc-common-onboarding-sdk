package com.isu.ekyc.checkStatus;

import static com.isu.ekyc.utils.EkycSdkConstants.NAME;
import static com.isu.ekyc.utils.EkycSdkConstants.sUpdateDetailsUrl;
import static com.isu.ekyc.utils.Utils.closeApplication;
import static com.isu.ekyc.utils.Utils.connectionCheck;
import static com.isu.ekyc.utils.Utils.exitTheProcess;
import static com.isu.ekyc.utils.Utils.handleErrorResponse;
import static com.isu.ekyc.utils.Utils.hideKeyboard;
import static com.isu.ekyc.utils.Utils.hideProgressDialog;
import static com.isu.ekyc.utils.Utils.intentToSDK;
import static com.isu.ekyc.utils.Utils.showErrorAlert;
import static com.isu.ekyc.utils.Utils.showProgressDialog;
import static com.isu.ekyc.utils.Utils.showToast;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.transition.TransitionManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.isu.awssdk.core.AwsSdkConstants;
import com.isu.awssdk.core.StringConstants;
import com.isu.ekyc.R;
import com.isu.ekyc.checkStatus.api.OnboardApiService;
import com.isu.ekyc.checkStatus.api.RetrofitInstance;
import com.isu.ekyc.configuration.Validate;
import com.isu.ekyc.databinding.ActivityNewAadhaarVerificaitonBinding;
import com.isu.ekyc.masked.viewmodel.MaskViewModel;
import com.isu.ekyc.otpverification.WelcomeActivity;
import com.isu.ekyc.utils.EkycSdkConstants;
import com.isu.ekyc.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewAadhaarVerificationActivity extends AppCompatActivity {
    private int mMatchScore = 0;
    ActivityNewAadhaarVerificaitonBinding newAadhaarVerificationActivity;
    private String mAadhaarName = "";
    private String mAadhaarNumber = "";
    private String mGetToken = "";
    private String mAddress = "";
    private String mSelectedImage = "";
    private SessionManager session;
    private MaskViewModel maskViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        newAadhaarVerificationActivity = ActivityNewAadhaarVerificaitonBinding.inflate(getLayoutInflater());
        setContentView(newAadhaarVerificationActivity.getRoot());

        session = new SessionManager(this);
        HashMap<String, String> user = session.getUserDetails();
        mGetToken = user.get(SessionManager.KEY_TOKEN);
        InputFilter[] aadhaarNumberFilter = new InputFilter[]{
                new InputFilter.LengthFilter(12) // Restrict maximum length
        };
        newAadhaarVerificationActivity.edtAadhaar.setFilters(aadhaarNumberFilter);
        InputFilter[] aadhaarNameFilter = new InputFilter[]{
                new InputFilter.AllCaps(), // Convert input to uppercase
                new InputFilter.LengthFilter(35) // Restrict maximum length
        };
        newAadhaarVerificationActivity.edtAadhaarName.setFilters(aadhaarNameFilter);
        newAadhaarVerificationActivity.editLayoutAadhaar.setEndIconOnClickListener(v -> {
            newAadhaarVerificationActivity.edtAadhaar.setEnabled(true);
            InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInputFromWindow(newAadhaarVerificationActivity.editLayoutAadhaar.getApplicationWindowToken(),
                    InputMethodManager.SHOW_FORCED, 0);
            newAadhaarVerificationActivity.edtAadhaar.requestFocus();
            newAadhaarVerificationActivity.edtAadhaar.setSelection(newAadhaarVerificationActivity.edtAadhaar.getText().length());
        });
        newAadhaarVerificationActivity.editLayoutAadhaarName.setEndIconOnClickListener(v -> {
            newAadhaarVerificationActivity.edtAadhaarName.setEnabled(true);
            InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInputFromWindow(newAadhaarVerificationActivity.editLayoutAadhaarName.getApplicationWindowToken(),
                    InputMethodManager.SHOW_FORCED, 0);
            newAadhaarVerificationActivity.edtAadhaarName.requestFocus();
            newAadhaarVerificationActivity.edtAadhaarName.setSelection(newAadhaarVerificationActivity.edtAadhaarName.getText().length());
        });
        maskViewModel = new MaskViewModel();
        newAadhaarVerificationActivity.edtAadhaar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAadhaarNumber = String.valueOf(s);
                if (mAadhaarNumber.length() == 12) {
                    hideKeyboard(NewAadhaarVerificationActivity.this, newAadhaarVerificationActivity.edtAadhaar);
                    newAadhaarVerificationActivity.edtAadhaar.clearFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        newAadhaarVerificationActivity.edtAadhaarName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAadhaarName = String.valueOf(s);
                if (mAadhaarName.length() == 35) {
                    hideKeyboard(NewAadhaarVerificationActivity.this, newAadhaarVerificationActivity.edtAadhaarName);
                    newAadhaarVerificationActivity.edtAadhaarName.clearFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        newAadhaarVerificationActivity.ivAadharFront.setOnClickListener(v -> {
            mSelectedImage = getString(R.string.aadhaarCardFront);
            if (Boolean.TRUE.equals(connectionCheck(this))) {
                intentToSDK(this, mGetToken, StringConstants.AADHAAR_CARD_FRONT);
            }
        });

        newAadhaarVerificationActivity.ivPlaceHolderFront.setOnClickListener(v -> {
            mSelectedImage = getString(R.string.aadhaarCardFront);
            if (Boolean.TRUE.equals(connectionCheck(NewAadhaarVerificationActivity.this))) {
                intentToSDK(NewAadhaarVerificationActivity.this, mGetToken, StringConstants.AADHAAR_CARD_FRONT);
            }
        });

        newAadhaarVerificationActivity.ivAadharBack.setOnClickListener(v -> {
            if (Boolean.TRUE.equals(connectionCheck(NewAadhaarVerificationActivity.this))) {
                if (newAadhaarVerificationActivity.ivAadharFront.getDrawable() == null || newAadhaarVerificationActivity.frontTapHere.getVisibility() == View.VISIBLE) {
                    showErrorAlert(this, getString(R.string.sdk_error_capture_front_image));
                } else {
                    setUpdatedAadhaarDetails();
                    mSelectedImage = getString(R.string.aadhaarCardBack);
                    intentToSDK(this, mGetToken, StringConstants.AADHAAR_CARD_BACK);
                }
            }
        });

        newAadhaarVerificationActivity.ivPlaceHolderBack.setOnClickListener(v -> {
            if (Boolean.TRUE.equals(connectionCheck(NewAadhaarVerificationActivity.this))) {
                if (newAadhaarVerificationActivity.ivAadharFront.getDrawable() == null || newAadhaarVerificationActivity.frontTapHere.getVisibility() == View.VISIBLE) {
                    showErrorAlert(this, getString(R.string.sdk_error_capture_front_image));
                } else {
                    setUpdatedAadhaarDetails();
                    mSelectedImage = getString(R.string.aadhaarCardBack);
                    intentToSDK(NewAadhaarVerificationActivity.this, mGetToken, StringConstants.AADHAAR_CARD_BACK);
                }
            }
        });

        newAadhaarVerificationActivity.ivSelfieHolder.setOnClickListener(v -> {
            setUpdatedAadhaarDetails();
            if (Boolean.TRUE.equals(connectionCheck(NewAadhaarVerificationActivity.this))) {
                mSelectedImage = getString(R.string.faceLiveliness);
                intentToSDK(this, mGetToken, StringConstants.FACE_LIVELINESS);
            }
        });
        newAadhaarVerificationActivity.ivSefie.setOnClickListener(v -> {
            setUpdatedAadhaarDetails();
            if (Boolean.TRUE.equals(connectionCheck(NewAadhaarVerificationActivity.this))) {
                mSelectedImage = getString(R.string.faceLiveliness);
                intentToSDK(this, mGetToken, StringConstants.FACE_LIVELINESS);
            }
        });

        newAadhaarVerificationActivity.btnSubmit.setOnClickListener(v -> {
            if (Boolean.TRUE.equals(connectionCheck(NewAadhaarVerificationActivity.this))) {
                setUpdatedAadhaarDetails();
                if (newAadhaarVerificationActivity.btnSubmit.getText().toString().equals(getString(R.string.sdk_proceed_btn))) {
                    if (newAadhaarVerificationActivity.ivSefie.getDrawable() == null) {
                        showToast(NewAadhaarVerificationActivity.this, getString(R.string.sdk_error_capture_selfie_image));
                    } else {
                        existingUpdateDetails();
                    }
                } else {
                    if (validate()) {
                        if (!EkycSdkConstants.frontAadhaarImageUrl.isEmpty() && !EkycSdkConstants.backAadhaarImageUrl.isEmpty()) {
                            mSelectedImage = getString(R.string.faceLiveliness);
                            intentToSDK(this, mGetToken, StringConstants.FACE_LIVELINESS);
                        } else {
                            showToast(NewAadhaarVerificationActivity.this, getString(R.string.sdk_aadhar_photo_validation));
                        }


                    }
                }
            }
        });
        //On back pressed handled
        OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                exitTheProcess(NewAadhaarVerificationActivity.this, getString(R.string.sdk_do_you_want_to_exit));
            }
        };
        // Add the callback to the onBackPressedDispatcher
        getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback);
    }

    /**
     * The case is executed when user edited the Aadhaar details field
     * Updated extracted aadhaar details store before another process
     */
    private void setUpdatedAadhaarDetails() {
        mAadhaarNumber = newAadhaarVerificationActivity.edtAadhaar.getText().toString();
        mAadhaarName = newAadhaarVerificationActivity.edtAadhaarName.getText().toString();
        EkycSdkConstants.aadhaarNumber = mAadhaarNumber;
        EkycSdkConstants.aadhaarName = mAadhaarName;
    }

    /**
     * Aadhaar deatails layout will be visible after data extracted from aadhaar
     *
     * @param show   to check if layouts are need to show
     * @param name   extracted aadhaar name
     * @param number extracted aadhaar number
     */
    public void showAadhaar(boolean show, String name, String number) {
        TransitionManager.beginDelayedTransition(newAadhaarVerificationActivity.aadhaarLayout);
        if (show) {

            mAadhaarName = name;
            mAadhaarNumber = number;
            newAadhaarVerificationActivity.edtAadhaarName.setText(name);
            //String maskAadhaar = "**** **** " + num.substring(8);
            newAadhaarVerificationActivity.edtAadhaar.setText(number);

            newAadhaarVerificationActivity.editLayoutAadhaarName.setVisibility(View.VISIBLE);
            newAadhaarVerificationActivity.editLayoutAadhaar.setVisibility(View.VISIBLE);

        } else {
            mAadhaarName = "";
            mAadhaarNumber = "";
            newAadhaarVerificationActivity.edtAadhaar.setText(number);
            newAadhaarVerificationActivity.edtAadhaarName.setText(name);
            newAadhaarVerificationActivity.edtAadhaar.setVisibility(View.GONE);
            newAadhaarVerificationActivity.edtAadhaarName.setVisibility(View.GONE);
        }
    }

    /**
     * validate all fields
     *
     * @return
     */
    private boolean validate() {
        mAadhaarNumber = newAadhaarVerificationActivity.edtAadhaar.getText().toString();
        mAadhaarName = newAadhaarVerificationActivity.edtAadhaarName.getText().toString();
        String namePerDashboard = NAME.toUpperCase();
        String aadhaarNameCheck = mAadhaarName.toUpperCase();
        Double nameMatchScore = Validate.diceCoefficientOptimized(namePerDashboard, aadhaarNameCheck);
        if (newAadhaarVerificationActivity.ivAadharFront.getDrawable() == null && newAadhaarVerificationActivity.ivAadharBack.getDrawable() == null) {
            showErrorAlert(this, getResources().getString(R.string.sdk_aadhar_photo_validation));
            return false;
        } else if (newAadhaarVerificationActivity.ivAadharFront.getDrawable() == null || newAadhaarVerificationActivity.frontTapHere.getVisibility() == View.VISIBLE) {
            showErrorAlert(this, getString(R.string.sdk_error_capture_front_image));
            return false;
        } else if (mAadhaarNumber.isEmpty()) {
            showErrorAlert(this, getString(R.string.sdk_error_aadhaar_number));
            return false;
        } else if (mAadhaarName.isEmpty()) {
            showErrorAlert(this, getString(R.string.sdk_error_aadhaar_name));
            return false;
        } else if (mAadhaarName.length() < 5) {
            showErrorAlert(this, getString(R.string.sdk_error_aadhaar_name_length));
            return false;
        } else if (newAadhaarVerificationActivity.ivAadharBack.getDrawable() == null || newAadhaarVerificationActivity.backTapHere.getVisibility() == View.VISIBLE) {
            showErrorAlert(this, getString(R.string.sdk_error_capture_back_image));
            return false;
        } else if (mAadhaarNumber.length() < 12) {
            showErrorAlert(this, getString(R.string.sdk_error_aadhaar_number_length));
            return false;
        } else if (!Validate.validateAadharNumber(mAadhaarNumber)) {
            showErrorAlert(this, getString(R.string.sdk_invalid_aadhaar_number));
            return false;
        }/*else if (nameMatchScore < 0.7) {
           showErrorAlert(this,getString(R.string.sdk_error_aadhaar_name_match));
           return false;
        }*/ else {
            return true;
        }
    }


    /**
     * update details for existing process
     */
    private void existingUpdateDetails() {
        try {
            showProgressDialog(this, getString(R.string.sdk_please_wait));
            JSONObject userUpdateData = new JSONObject();
            JSONObject frontAadhaarInfo = new JSONObject();
            frontAadhaarInfo.put("mother", "");
            frontAadhaarInfo.put("pin", EkycSdkConstants.pin);
            frontAadhaarInfo.put("gender", EkycSdkConstants.gender);
            frontAadhaarInfo.put("dob", EkycSdkConstants.dob);
            frontAadhaarInfo.put("father", EkycSdkConstants.fathersName);
            frontAadhaarInfo.put("aadhar", mAadhaarNumber);
            frontAadhaarInfo.put("name", EkycSdkConstants.aadhaarName);
            frontAadhaarInfo.put("husband", "");
            frontAadhaarInfo.put("yob", "");

            JSONObject backAadhaarInfo = new JSONObject();
            backAadhaarInfo.put("mother", "");
            backAadhaarInfo.put("pin", EkycSdkConstants.pin);
            backAadhaarInfo.put("gender", EkycSdkConstants.gender);
            backAadhaarInfo.put("dob", EkycSdkConstants.dob);
            backAadhaarInfo.put("father", EkycSdkConstants.fathersName);
            backAadhaarInfo.put("name", EkycSdkConstants.aadhaarName);
            backAadhaarInfo.put("aadhar", mAadhaarNumber);
            backAadhaarInfo.put("husband", "");
            backAadhaarInfo.put("yob", "");

            JSONObject aadhaarUpdate = new JSONObject();
            aadhaarUpdate.put("aadhar", mAadhaarNumber);
            aadhaarUpdate.put("aadhaarBackImage", EkycSdkConstants.backAadhaarImageUrl);
            aadhaarUpdate.put("aadhaarFrontImage", EkycSdkConstants.frontAadhaarImageUrl);
            aadhaarUpdate.put("frontAadharInfo", frontAadhaarInfo);
            aadhaarUpdate.put("backAadharInfo", backAadhaarInfo);
            userUpdateData.put("2", aadhaarUpdate);

            JSONObject selfiUpdate = new JSONObject();
            selfiUpdate.put("match_score", mMatchScore);
            selfiUpdate.put("selfieUrl", EkycSdkConstants.selfieImageUrl);
            userUpdateData.put("4", selfiUpdate);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("username", EkycSdkConstants.USER_NAME);
            jsonObject.put("userUpdateData", userUpdateData);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));
            OnboardApiService onboardApiService = RetrofitInstance.retrofit().create(OnboardApiService.class);
            onboardApiService.updateOnboardData(body, sUpdateDetailsUrl, mGetToken).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        hideProgressDialog();
                        try {
                            String updateResponse = response.body().string();
                            JSONObject updateJson = new JSONObject(updateResponse);
                            String status = updateJson.getString("status");
                            String msg = updateJson.getString("statusDesc");
                            if (status.equals("0")) {
                                closeApplication(NewAadhaarVerificationActivity.this, msg);
                            } else {
                                showToast(NewAadhaarVerificationActivity.this, msg);
                            }

                        } catch (JSONException | IOException e) {
                            showToast(NewAadhaarVerificationActivity.this, getString(R.string.something_went_wrong_please_try_again));
                        }
                    } else {
                        hideProgressDialog();
                        handleErrorResponse(NewAadhaarVerificationActivity.this, response.errorBody());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                    hideProgressDialog();
                    showToast(NewAadhaarVerificationActivity.this, t.getLocalizedMessage());
                }
            });

        } catch (JSONException e) {
            showToast(NewAadhaarVerificationActivity.this, getString(R.string.something_went_wrong_please_try_again));
        }
    }


    /**
     * Handle extracted data from SDK
     */
    @Override
    protected void onResume() {
        super.onResume();

        if (AwsSdkConstants.INSTANCE.getTOKEN_EXPIRED()) {
            closeApplication(this, getString(R.string.session_expired));
            AwsSdkConstants.INSTANCE.setTOKEN_EXPIRED(false);
        } else {
            String imgType = AwsSdkConstants.INSTANCE.getIMAGE_TYPE();
            if (imgType.equalsIgnoreCase(StringConstants.AADHAAR_CARD_FRONT) && Objects.equals(mSelectedImage, getString(R.string.aadhaarCardFront))) {
                EkycSdkConstants.aadhaarName = AwsSdkConstants.INSTANCE.getAadhaarFrontDetails().getAadhaarName();
                EkycSdkConstants.dob = AwsSdkConstants.INSTANCE.getAadhaarFrontDetails().getAadhaarDob();
                EkycSdkConstants.gender = AwsSdkConstants.INSTANCE.getAadhaarFrontDetails().getAadhaarGender();
                if (!AwsSdkConstants.INSTANCE.getAadhaarFrontDetails().getAadhaarFatherName().isEmpty()) {
                    EkycSdkConstants.fathersName = AwsSdkConstants.INSTANCE.getAadhaarBackDetails().getAadhaarFatherName();
                }
                mAadhaarNumber = AwsSdkConstants.INSTANCE.getAadhaarFrontDetails().getAadhaarNumber();
                EkycSdkConstants.frontAadhaarImageUrl = AwsSdkConstants.INSTANCE.getAadhaarFrontDetails().getAadhaarFrontFirebaseUrl();
                newAadhaarVerificationActivity.ivPlaceHolderFront.setVisibility(View.GONE);
                newAadhaarVerificationActivity.frontTapHere.setVisibility(View.GONE);
                newAadhaarVerificationActivity.ivAadharFront.setVisibility(View.VISIBLE);
                removeSelfieDetails();
                Glide.with(this).load(EkycSdkConstants.frontAadhaarImageUrl).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        newAadhaarVerificationActivity.ivPlaceHolderFront.setVisibility(View.VISIBLE);
                        newAadhaarVerificationActivity.frontTapHere.setVisibility(View.VISIBLE);
                        newAadhaarVerificationActivity.ivAadharFront.setVisibility(View.GONE);
                        newAadhaarVerificationActivity.ivAadharFront.setImageDrawable(null);
                        showToast(NewAadhaarVerificationActivity.this, getString(R.string.sdk_error_recapture_image));
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                }).diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter().placeholder(R.drawable.progress_animation).into(newAadhaarVerificationActivity.ivAadharFront);
                mAadhaarName = EkycSdkConstants.aadhaarName;
                EkycSdkConstants.aadhaarNumber = mAadhaarNumber.replaceAll("\\s", "");
                showAadhaar(true, EkycSdkConstants.aadhaarName, EkycSdkConstants.aadhaarNumber);
            } else if (imgType.equalsIgnoreCase(StringConstants.INSTANCE.AADHAAR_CARD_BACK) && Objects.equals(mSelectedImage, getString(R.string.aadhaarCardBack))) {
                mAddress = AwsSdkConstants.INSTANCE.getAadhaarBackDetails().getAadhaarAddress();
                EkycSdkConstants.pin = AwsSdkConstants.INSTANCE.getAadhaarBackDetails().getAadhaarPin();
                EkycSdkConstants.city = AwsSdkConstants.INSTANCE.getAadhaarBackDetails().getAadhaarCity();
                EkycSdkConstants.state = AwsSdkConstants.INSTANCE.getAadhaarBackDetails().getAadhaarState();
                EkycSdkConstants.district = AwsSdkConstants.INSTANCE.getAadhaarBackDetails().getAadhaarDistrict();
                EkycSdkConstants.address = mAddress;
                EkycSdkConstants.aadhaarBackNumber = AwsSdkConstants.INSTANCE.getAadhaarBackDetails().getAadhaarNumber();
                EkycSdkConstants.backAadhaarImageUrl = AwsSdkConstants.INSTANCE.getAadhaarBackDetails().getAadhaarBackFirebaseUrl();
                if (!AwsSdkConstants.INSTANCE.getAadhaarBackDetails().getAadhaarFatherName().isEmpty()) {
                    EkycSdkConstants.fathersName = AwsSdkConstants.INSTANCE.getAadhaarBackDetails().getAadhaarFatherName();
                }
                removeSelfieDetails();
                newAadhaarVerificationActivity.ivPlaceHolderBack.setVisibility(View.GONE);
                newAadhaarVerificationActivity.backTapHere.setVisibility(View.GONE);
                newAadhaarVerificationActivity.ivAadharBack.setVisibility(View.VISIBLE);
                Glide.with(this).load(EkycSdkConstants.backAadhaarImageUrl).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        newAadhaarVerificationActivity.ivPlaceHolderBack.setVisibility(View.VISIBLE);
                        newAadhaarVerificationActivity.backTapHere.setVisibility(View.VISIBLE);
                        newAadhaarVerificationActivity.ivAadharBack.setVisibility(View.GONE);
                        newAadhaarVerificationActivity.ivAadharBack.setImageDrawable(null);
                        showToast(NewAadhaarVerificationActivity.this, getString(R.string.sdk_error_recapture_image));
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                }).diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter().placeholder(R.drawable.progress_animation).into(newAadhaarVerificationActivity.ivAadharBack);

            } else if (imgType.equalsIgnoreCase(StringConstants.FACE_LIVELINESS) && Objects.equals(mSelectedImage, getString(R.string.faceLiveliness))) {
                String encoded = AwsSdkConstants.INSTANCE.getAadhaarFaceMatchDetails().getSelfieFirebaseUrl();
                if (!AwsSdkConstants.INSTANCE.getAadhaarFaceMatchDetails().getMatchScore().isEmpty()) {
                    Double score = Double.parseDouble(AwsSdkConstants.INSTANCE.getAadhaarFaceMatchDetails().getMatchScore());
                    mMatchScore = score.intValue();
                }
                EkycSdkConstants.selfieImageUrl = encoded;
                newAadhaarVerificationActivity.rlSelfie.setVisibility(View.VISIBLE);
                newAadhaarVerificationActivity.tvSelfie.setVisibility(View.VISIBLE);
                newAadhaarVerificationActivity.ivSelfieHolder.setVisibility(View.GONE);
                newAadhaarVerificationActivity.selfieTapHere.setVisibility(View.GONE);
                newAadhaarVerificationActivity.ivSefie.setVisibility(View.VISIBLE);
                Glide.with(this)
                        .load(encoded)
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                newAadhaarVerificationActivity.ivSelfieHolder.setVisibility(View.VISIBLE);
                                newAadhaarVerificationActivity.selfieTapHere.setVisibility(View.VISIBLE);
                                newAadhaarVerificationActivity.ivSefie.setVisibility(View.GONE);
                                newAadhaarVerificationActivity.ivSefie.setImageDrawable(null);
                                showToast(NewAadhaarVerificationActivity.this, getString(R.string.sdk_error_recapture_image));
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                return false;
                            }
                        })
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .fitCenter()
                        .placeholder(R.drawable.progress_animation_potrait)
                        .into(newAadhaarVerificationActivity.ivSefie);
                newAadhaarVerificationActivity.btnSubmit.setText(getString(R.string.sdk_proceed_btn));
            }
            AwsSdkConstants.INSTANCE.setIMAGE_TYPE("");
        }
    }

    /**
     * Remove selfie captured details when aadhaar data is extracted
     */
    private void removeSelfieDetails() {
        newAadhaarVerificationActivity.ivSelfieHolder.setVisibility(View.VISIBLE);
        newAadhaarVerificationActivity.selfieTapHere.setVisibility(View.VISIBLE);
        newAadhaarVerificationActivity.ivSefie.setVisibility(View.GONE);
        newAadhaarVerificationActivity.rlSelfie.setVisibility(View.GONE);
        newAadhaarVerificationActivity.tvSelfie.setVisibility(View.GONE);
        newAadhaarVerificationActivity.btnSubmit.setText(getString(R.string.sdk_proceed_to_next));
        newAadhaarVerificationActivity.ivSefie.setImageDrawable(null);
    }

}