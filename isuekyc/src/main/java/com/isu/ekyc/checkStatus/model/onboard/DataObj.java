package com.isu.ekyc.checkStatus.model.onboard;

import com.google.gson.annotations.SerializedName;

public class DataObj{

	@SerializedName("1")
	private String phone;

	@SerializedName("2")
	private String email;

	@SerializedName("3")
	private PanDetails panDetails;

	@SerializedName("4")
	private AadhaarDetails aadhaarDetails;

	@SerializedName("5")
	private SelfieDetails selfieDetails;

	@SerializedName("6")
	private BasicInformation basicInformation;

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPanDetails(PanDetails panDetails) {
		this.panDetails = panDetails;
	}

	public void setAadhaarDetails(AadhaarDetails aadhaarDetails) {
		this.aadhaarDetails = aadhaarDetails;
	}

	public void setSelfieDeatils(SelfieDetails selfieDetails) {
		this.selfieDetails = selfieDetails;
	}

	public void setBasicInformation(BasicInformation basicInformation) {
		this.basicInformation = basicInformation;
	}
}