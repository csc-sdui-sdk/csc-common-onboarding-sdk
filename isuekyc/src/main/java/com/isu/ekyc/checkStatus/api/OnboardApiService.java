package com.isu.ekyc.checkStatus.api;

import com.isu.ekyc.checkStatus.commonOnboarding.OnboardUiRequestModel;
import com.isu.ekyc.checkStatus.commonOnboarding.onboarding.OnboardingUiResponseModel;
import com.isu.ekyc.checkStatus.model.EmailRequest;
import com.isu.ekyc.checkStatus.model.EmailResponseModel;
import com.isu.ekyc.checkStatus.model.FetchStatusRequest;
import com.isu.ekyc.checkStatus.model.OtpRequest;
import com.isu.ekyc.checkStatus.model.fetchstatedistrict.FetchStateDistrictRequestModel;
import com.isu.ekyc.checkStatus.model.fetchstatedistrict.FetchStateDistrictResponseModel;
import com.isu.ekyc.checkStatus.model.fetchstatus.FetchStatusRequestBody;
import com.isu.ekyc.checkStatus.model.fetchstatus.FetchStatusResponseModel;
import com.isu.ekyc.checkStatus.model.onboard.OnboardRequestModel;
import com.isu.ekyc.checkStatus.model.onboard.OnboardResponseModel;
import com.isu.ekyc.checkStatus.model.pan.PanRequestModel;
import com.isu.ekyc.checkStatus.model.pan.PanResponseModel;
import com.isu.ekyc.checkStatus.model.phone.PhoneResponseModel;
import com.isu.ekyc.checkStatus.model.phone.SendOtpRequestModel;
import com.isu.ekyc.checkStatus.model.phone.VerifyOTPRequestModel;
import com.isu.ekyc.checkStatus.model.pincode.PinCodeRequestModel;
import com.isu.ekyc.checkStatus.model.pincode.PinCodeResponseModel;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface OnboardApiService {
    @POST
    Call<ResponseBody> getFetchStaus(@Url String url, @Body FetchStatusRequest fetchStatusRequest);

    @POST
    Call<OnboardResponseModel> onboard(@Body OnboardRequestModel onboardRequestModel, @Header("Authorization") String token, @Url String url);
    @POST
    Call<EmailResponseModel> sendEmail(@Body EmailRequest emailRequest, @Header("Authorization") String token, @Url String url);

    @POST
    Call<EmailResponseModel> verifyEmail(@Body EmailRequest otpRequest, @Header("Authorization") String token, @Url String url);

    @POST
    Call<ResponseBody> updateOnboardData(@Body RequestBody requestBody, @Url String url, @Header("Authorization") String token);

    @POST("common_onboarding/fetchData")
    Call<OnboardingUiResponseModel> getOnboardUrl(@Body OnboardUiRequestModel onboardUiRequestModel);

    @POST
    Call<FetchStateDistrictResponseModel> getStateDistrict(@Body FetchStateDistrictRequestModel requestModel, @Url String url);

    @POST
    Call<PinCodeResponseModel> pinCode(@Body PinCodeRequestModel requestBody, @Url String url);

    @POST
    Call<PanResponseModel> validatePan(@Body PanRequestModel requestBody, @Url String url);

    @POST
    Call<FetchStatusResponseModel> fetchStatus(@Body FetchStatusRequestBody requestBody, @Url String url, @Header("Authorization") String token);
    @POST
    Call<PhoneResponseModel> sendOTP(@Body SendOtpRequestModel sendOtpRequest, @Url String url);
    @POST
    Call<PhoneResponseModel> verifyOTP(@Body VerifyOTPRequestModel verifyOTPRequest, @Url String url);
}
