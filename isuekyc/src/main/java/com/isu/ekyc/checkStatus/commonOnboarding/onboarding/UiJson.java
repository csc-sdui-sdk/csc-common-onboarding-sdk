package com.isu.ekyc.checkStatus.commonOnboarding.onboarding;

import com.google.gson.annotations.SerializedName;

public class UiJson{

	@SerializedName("APIs")
	private APIResponse commonOnboardingUrls;

	public APIResponse getCommonOnboardingUrls(){
		return commonOnboardingUrls;
	}
}