package com.isu.ekyc.checkStatus.model.phone;

import com.google.gson.annotations.SerializedName;
import com.isu.ekyc.utils.CSCStringConstants;

public class VerifyOTPRequestModel{

	@SerializedName(CSCStringConstants.OTP_SMALL)
	private String otp;

	@SerializedName(CSCStringConstants.VERIFY_OTP_MOBILE_NO)
	private String mobileNo;

	@SerializedName(CSCStringConstants.PARAMS)
	private String params;

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public void setParams(String params) {
		this.params = params;
	}
}