package com.isu.ekyc.checkStatus;

import static com.isu.ekyc.utils.EkycSdkConstants.sSendEmailUrl;
import static com.isu.ekyc.utils.Utils.connectionCheck;
import static com.isu.ekyc.utils.Utils.exitTheProcess;
import static com.isu.ekyc.utils.Utils.handleErrorResponse;
import static com.isu.ekyc.utils.Utils.hideProgressDialog;
import static com.isu.ekyc.utils.Utils.showErrorAlert;
import static com.isu.ekyc.utils.Utils.showProgressDialog;
import static com.isu.ekyc.utils.Utils.showToast;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.isu.ekyc.R;
import com.isu.ekyc.checkStatus.api.OnboardApiService;
import com.isu.ekyc.checkStatus.api.RetrofitInstance;
import com.isu.ekyc.checkStatus.model.EmailRequest;
import com.isu.ekyc.checkStatus.model.EmailResponseModel;
import com.isu.ekyc.configuration.Validate;
import com.isu.ekyc.databinding.ActivityNewEmailVerificationBinding;
import com.isu.ekyc.utils.EkycSdkConstants;
import com.isu.ekyc.utils.LoginConstants;
import com.isu.ekyc.utils.OnboardingIntentFactory;
import com.isu.ekyc.utils.SessionManager;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewEmailVerificationActivity extends AppCompatActivity {
    ActivityNewEmailVerificationBinding activityNewEmailVerificationBinding;
    private String mEmail = "";
    private String mGetToken = "";
    private SessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityNewEmailVerificationBinding = DataBindingUtil.setContentView(this, R.layout.activity_new_email_verification);
        session = new SessionManager(NewEmailVerificationActivity.this);
        HashMap<String, String> user = session.getUserDetails();
        mGetToken = user.get(SessionManager.KEY_TOKEN);
        activityNewEmailVerificationBinding.loginProceed.setOnClickListener(v -> {
            if (Boolean.TRUE.equals(connectionCheck(this))) {
                mEmail = activityNewEmailVerificationBinding.loginMobile.getText().toString().trim();
                LoginConstants.newUserEmail = mEmail;
                if (Validate.isValidMail(mEmail)) {
                    existingSendEmailAPI();
                } else {
                    showErrorAlert(NewEmailVerificationActivity.this, getString(R.string.sdk_error_email_address));
                }
            }
        });

        //On back pressed handled
        OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                exitTheProcess(NewEmailVerificationActivity.this, getString(R.string.sdk_do_you_want_to_exit));
            }
        };
        // Add the callback to the onBackPressedDispatcher
        getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback);
    }


    /**
     * This function is called to send otp to email,when user email rejected and need to re-verify email in existing KYC process
     */
    private void existingSendEmailAPI() {
        showProgressDialog(this, getString(R.string.sdk_please_wait));
        EmailRequest emailRequest = new EmailRequest();
        OnboardApiService onboardApiService = RetrofitInstance.retrofit().create(OnboardApiService.class);
        emailRequest.setEmail(mEmail);
        emailRequest.setUsername(EkycSdkConstants.USER_NAME);

        Call<EmailResponseModel> sendEmail = onboardApiService.sendEmail(emailRequest, mGetToken, sSendEmailUrl);
        sendEmail.enqueue(new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<EmailResponseModel> call, @NonNull Response<EmailResponseModel> response) {
                hideProgressDialog();
                if (response.isSuccessful() && response.body() != null) {
                    int status = response.body().getStatus();
                    String msg = response.body().getStatusDesc();
                    showToast(NewEmailVerificationActivity.this, msg);
                    if (status == 0) {
                        Intent intent = OnboardingIntentFactory.returnNewVerifyActivityIntent(NewEmailVerificationActivity.this);
                        intent.putExtra("email", mEmail);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    handleErrorResponse(NewEmailVerificationActivity.this, response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<EmailResponseModel> call, @NonNull Throwable t) {
                hideProgressDialog();
                showToast(NewEmailVerificationActivity.this, t.getLocalizedMessage());
            }
        });
    }

}