package com.isu.ekyc.checkStatus.model.onboard;

import com.google.gson.annotations.SerializedName;

public class OnboardResponseModel{

	@SerializedName("statusDesc")
	private String statusDesc;

	@SerializedName("status")
	private Integer status;

	public String getStatusDesc() {
		return statusDesc;
	}

	public Integer getStatus() {
		return status;
	}
}