package com.isu.ekyc.checkStatus.model;

import com.google.gson.annotations.SerializedName;

public class EmailRequest{
	@SerializedName("email")
	private String email;
	@SerializedName("otp")
	private String otp;
	@SerializedName("username")
	private String username;

	public void setEmail(String email) {
		this.email = email;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
