package com.isu.ekyc.checkStatus;

import static com.isu.ekyc.utils.EkycSdkConstants.sUpdateDetailsUrl;
import static com.isu.ekyc.utils.Utils.closeApplication;
import static com.isu.ekyc.utils.Utils.exitTheProcess;
import static com.isu.ekyc.utils.Utils.handleErrorResponse;
import static com.isu.ekyc.utils.Utils.hideProgressDialog;
import static com.isu.ekyc.utils.Utils.showProgressDialog;
import static com.isu.ekyc.utils.Utils.showToast;

import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.isu.ekyc.R;
import com.isu.ekyc.checkStatus.api.OnboardApiService;
import com.isu.ekyc.checkStatus.api.RetrofitInstance;
import com.isu.ekyc.configuration.Validate;
import com.isu.ekyc.databinding.ActivityNewGstactivityBinding;
import com.isu.ekyc.utils.EkycSdkConstants;
import com.isu.ekyc.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewGSTActivity extends AppCompatActivity {

    ActivityNewGstactivityBinding gstBinding;
    private String mGstNov;
    private String mToken = "";
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gstBinding = DataBindingUtil.setContentView(this, R.layout.activity_new_gstactivity);
        session = new SessionManager(this);
        HashMap<String, String> user = session.getUserDetails();
        mToken = user.get(SessionManager.KEY_TOKEN);

        gstBinding.ekycFormNext.setOnClickListener(v -> {
            mGstNov = gstBinding.ekycFormGSTNo.getText().toString().trim();
            if (mGstNov.isEmpty() || mGstNov.isEmpty()) {
                showToast(NewGSTActivity.this, getString(R.string.sdk_error_empty_gst));
            } else {
                if (Validate.isValidGSTNo(mGstNov)) {
                    existingUpdateDetails();
                } else {
                    showToast(NewGSTActivity.this, getString(R.string.sdk_error_valid_gst));
                }

            }
        });

        //On back pressed handled
        OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                exitTheProcess(NewGSTActivity.this, getString(R.string.sdk_do_you_want_to_exit));
            }
        };
        // Add the callback to the onBackPressedDispatcher
        getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback);
    }

    /**
     * update GST details in existing KYC process
     */
    private void existingUpdateDetails() {
        try {
            showProgressDialog(this,getString(R.string.sdk_please_wait));
            JSONObject userUpdateData = new JSONObject();

            userUpdateData.put("5", mGstNov);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("username", EkycSdkConstants.USER_NAME);
            jsonObject.put("userUpdateData", userUpdateData);

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));
            OnboardApiService onboardApiService = RetrofitInstance.retrofit().create(OnboardApiService.class);
            onboardApiService.updateOnboardData(body, sUpdateDetailsUrl, mToken).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                    hideProgressDialog();
                    if (response.isSuccessful() && response.body() != null) {
                        try {
                            String updateResponse = response.body().string();
                            JSONObject updateJson = new JSONObject(updateResponse);
                            int status = updateJson.optInt("status");
                            String msg = updateJson.optString("statusDesc");
                            if (status == 0) {
                                closeApplication(NewGSTActivity.this, msg);
                            } else {
                                showToast(NewGSTActivity.this, msg);
                            }

                        } catch (JSONException | IOException e) {
                            showToast(NewGSTActivity.this, getString(R.string.something_went_wrong_please_try_again));
                        }
                    } else {
                        handleErrorResponse(NewGSTActivity.this, response.errorBody());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                    hideProgressDialog();
                    showToast(NewGSTActivity.this, t.getLocalizedMessage());
                }
            });

        } catch (JSONException e) {
            showToast(NewGSTActivity.this, getString(R.string.something_went_wrong_please_try_again));
        }


    }
}