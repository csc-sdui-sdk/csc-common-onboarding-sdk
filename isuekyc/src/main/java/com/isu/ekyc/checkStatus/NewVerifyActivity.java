package com.isu.ekyc.checkStatus;

import static android.content.ContentValues.TAG;
import static com.isu.ekyc.utils.EkycSdkConstants.sSendEmailUrl;
import static com.isu.ekyc.utils.EkycSdkConstants.sUpdateDetailsUrl;
import static com.isu.ekyc.utils.EkycSdkConstants.sVerifyEmailUrl;
import static com.isu.ekyc.utils.Utils.closeApplication;
import static com.isu.ekyc.utils.Utils.connectionCheck;
import static com.isu.ekyc.utils.Utils.exitTheProcess;
import static com.isu.ekyc.utils.Utils.handleErrorResponse;
import static com.isu.ekyc.utils.Utils.hideKeyboard;
import static com.isu.ekyc.utils.Utils.hideProgressDialog;
import static com.isu.ekyc.utils.Utils.showErrorAlert;
import static com.isu.ekyc.utils.Utils.showProgressDialog;
import static com.isu.ekyc.utils.Utils.showToast;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.isu.ekyc.R;
import com.isu.ekyc.checkStatus.api.OnboardApiService;
import com.isu.ekyc.checkStatus.api.RetrofitInstance;
import com.isu.ekyc.checkStatus.model.EmailRequest;
import com.isu.ekyc.checkStatus.model.EmailResponseModel;
import com.isu.ekyc.checkStatus.model.OtpRequest;
import com.isu.ekyc.configuration.Validate;
import com.isu.ekyc.databinding.ActivityNewVerifyBinding;
import com.isu.ekyc.emailverfication.EmailVerificationActivity;
import com.isu.ekyc.utils.EkycSdkConstants;
import com.isu.ekyc.utils.LoginConstants;
import com.isu.ekyc.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewVerifyActivity extends AppCompatActivity {

    ActivityNewVerifyBinding activityNewVerifyBinding;
    private String getToken = "";
    private String mOtp;
    private SessionManager session;
    private int mCounter = 30;
    private final int allowed = 30;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityNewVerifyBinding = DataBindingUtil.setContentView(this, R.layout.activity_new_verify);
        session = new SessionManager(this);
        HashMap<String, String> user = session.getUserDetails();
        getToken = user.get(SessionManager.KEY_TOKEN);
        if (getIntent() != null && getIntent().hasExtra("email")) {
            LoginConstants.newUserEmail = getIntent().getStringExtra("email");
            activityNewVerifyBinding.tvOtpMessage.setText(R.string.sdk_email_otp_message);
            activityNewVerifyBinding.tvMobile.setText(LoginConstants.newUserEmail);
        }
        statTimer();
        activityNewVerifyBinding.loginProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Boolean.TRUE.equals(connectionCheck(NewVerifyActivity.this))) {
                    mOtp = activityNewVerifyBinding.edtOtp.getText().toString();
                    hideKeyboard(NewVerifyActivity.this, v);
                    if (mOtp != null && !mOtp.isEmpty()) {
                        showProgressDialog(NewVerifyActivity.this, getString(R.string.sdk_please_wait));
                        existingVerifyAPI(sVerifyEmailUrl, getToken);
                    } else {
                        showErrorAlert(NewVerifyActivity.this, getString(R.string.sdk_enter_otp));
                    }
                }
            }
        });

        activityNewVerifyBinding.tvResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Boolean.TRUE.equals(connectionCheck(NewVerifyActivity.this))) {
                    activityNewVerifyBinding.edtOtp.setText("");
                    showProgressDialog(NewVerifyActivity.this, getString(R.string.sdk_please_wait));
                    existingResendEmail();
                }
            }
        });
        //On back pressed handled
        OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                exitTheProcess(NewVerifyActivity.this, getString(R.string.sdk_do_you_want_to_exit));
            }
        };
        // Add the callback to the onBackPressedDispatcher
        getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback);
    }


    /**
     * This function is called to re-send otp to email in existing KYC process
     */
    private void existingResendEmail() {
        EmailRequest emailRequest = new EmailRequest();

        OnboardApiService onboardApiService = RetrofitInstance.retrofit().create(OnboardApiService.class);
        emailRequest.setEmail(LoginConstants.newUserEmail);
        emailRequest.setUsername(EkycSdkConstants.USER_NAME);

        Call<EmailResponseModel> sendEmail = onboardApiService.sendEmail(emailRequest, getToken, sSendEmailUrl);
        sendEmail.enqueue(new Callback<EmailResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<EmailResponseModel> call, @NonNull Response<EmailResponseModel> response) {
                hideProgressDialog();
                if (response.isSuccessful() && response.body() != null) {
                    int status = response.body().getStatus();
                    String msg = response.body().getStatusDesc();
                    if (status == 0) {
                        showToast(NewVerifyActivity.this, getString(R.string.sdk_resend_otp));
                        statTimer();
                    } else {
                        showToast(NewVerifyActivity.this, msg);
                    }
                } else {
                    handleErrorResponse(NewVerifyActivity.this, response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<EmailResponseModel> call, @NonNull Throwable t) {
                hideProgressDialog();
                showToast(NewVerifyActivity.this, t.getLocalizedMessage());
            }
        });
    }


    /**
     * This function will verify email in existing process
     *
     * @param url
     * @param token
     */
    private void existingVerifyAPI(String url, String token) {
        EmailRequest otpRequest = new EmailRequest();
        otpRequest.setOtp(mOtp);

        otpRequest.setUsername(EkycSdkConstants.USER_NAME);
        OnboardApiService onboardApiService = RetrofitInstance.retrofit().create(OnboardApiService.class);
        Call<EmailResponseModel> verifyOtp = onboardApiService.verifyEmail(otpRequest, token, url);
        verifyOtp.enqueue(new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<EmailResponseModel> call, @NonNull Response<EmailResponseModel> response) {
                hideProgressDialog();
                if (response.isSuccessful() && response.body() != null) {
                    int status = response.body().getStatus();
                    String msg = response.body().getStatusDesc();
                    showToast(NewVerifyActivity.this, msg);
                    if (status == 0) {
                        existingUpdateDetails(mOtp);
                    } else {
                        activityNewVerifyBinding.edtOtp.setText("");
                    }
                } else {
                    handleErrorResponse(NewVerifyActivity.this, response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<EmailResponseModel> call, @NonNull Throwable t) {
                hideProgressDialog();
                showToast(NewVerifyActivity.this, t.getLocalizedMessage());
            }
        });
    }


    /**
     * This function is called to update email details in existing process
     *
     * @param otp
     */
    private void existingUpdateDetails(String otp) {
        try {
            showProgressDialog(this, getString(R.string.sdk_please_wait));
            JSONObject userUpdateData = new JSONObject();
            JSONObject userUpdateKey = new JSONObject();
            userUpdateKey.put("email", LoginConstants.newUserEmail);
            userUpdateKey.put("otp", otp);
            userUpdateData.put("3", userUpdateKey);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("username", EkycSdkConstants.USER_NAME);
            jsonObject.put("userUpdateData", userUpdateData);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));
            OnboardApiService onboardApiService = RetrofitInstance.retrofit().create(OnboardApiService.class);
            onboardApiService.updateOnboardData(body, sUpdateDetailsUrl, getToken).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                    hideProgressDialog();
                    if (response.isSuccessful() && response.body() != null) {
                        try {
                            String updateResponse = response.body().string();
                            JSONObject updateJson = new JSONObject(updateResponse);
                            int status = updateJson.optInt("status", 0);
                            Log.e(TAG, "onResponse: status " + status);
                            String msg = updateJson.optString("statusDesc", "");
                            if (status == 0) {
                                closeApplication(NewVerifyActivity.this, msg);
                            } else {
                                showToast(NewVerifyActivity.this, msg);
                            }
                        } catch (JSONException | IOException e) {
                            showToast(NewVerifyActivity.this, getString(R.string.something_went_wrong_please_try_again));
                        }
                    } else {
                        handleErrorResponse(NewVerifyActivity.this, response.errorBody());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                    hideProgressDialog();
                    showToast(NewVerifyActivity.this, t.getLocalizedMessage());
                }
            });

        } catch (JSONException e) {
            showToast(NewVerifyActivity.this, getString(R.string.something_went_wrong_please_try_again));
        }


    }

    /**
     * start otp waiting time period to activate resend button
     */

    private void statTimer() {
        activityNewVerifyBinding.tvResend.setEnabled(false);
        activityNewVerifyBinding.tvResend.setTextColor(getResources().getColor(R.color.sign_up_pan_text));
        activityNewVerifyBinding.tvResend.setAlpha(0.5f);
        mCounter = 0;

        new CountDownTimer(allowed * 1000L, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int time = allowed - mCounter;
                activityNewVerifyBinding.tvTimer.setText(time + " Seconds");
                mCounter++;
            }

            @Override
            public void onFinish() {
                activityNewVerifyBinding.tvResend.setEnabled(true);
                activityNewVerifyBinding.tvResend.setTextColor(getResources().getColor(R.color.sign_up_button));
                activityNewVerifyBinding.tvResend.setAlpha(1.0f);
                activityNewVerifyBinding.tvTimer.setText(0 + " Seconds");
            }
        }.start();
    }

}