package com.isu.ekyc.checkStatus.model.fetchstatedistrict;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("village_code")
	private String villageCode;

	@SerializedName("name")
	private String name;

	@SerializedName("dist_code")
	private String distCode;

	@SerializedName("state_code")
	private String stateCode;

	@SerializedName("sub_dist_code")
	private String subDistCode;

	public String getVillageCode() {
		return villageCode;
	}

	public String getName() {
		return name;
	}

	public String getDistCode() {
		return distCode;
	}

	public String getStateCode() {
		return stateCode;
	}

	public String getSubDistCode() {
		return subDistCode;
	}
}