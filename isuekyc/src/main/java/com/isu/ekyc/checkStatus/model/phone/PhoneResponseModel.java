package com.isu.ekyc.checkStatus.model.phone;

import com.google.gson.annotations.SerializedName;
import com.isu.ekyc.utils.CSCStringConstants;

public class PhoneResponseModel{

	@SerializedName(CSCStringConstants.MESSAGE_STRING)
	private String message;

	@SerializedName(CSCStringConstants.STATUS)
	private Integer status;

	public String getMessage() {
		return message;
	}

	public Integer getStatus() {
		return status;
	}
}