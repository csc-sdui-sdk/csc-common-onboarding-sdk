package com.isu.ekyc.checkStatus.model.pan;

import com.google.gson.annotations.SerializedName;
import com.isu.ekyc.utils.CSCStringConstants;

public class PanResponseModel{

	@SerializedName(CSCStringConstants.DATA)
	private Data data;

	@SerializedName(CSCStringConstants.MESSAGE_STRING)
	private String message;

	@SerializedName(CSCStringConstants.STATUS)
	private Integer status;

	public Data getData() {
		return data;
	}

	public String getMessage() {
		return message;
	}

	public Integer getStatus() {
		return status;
	}
}