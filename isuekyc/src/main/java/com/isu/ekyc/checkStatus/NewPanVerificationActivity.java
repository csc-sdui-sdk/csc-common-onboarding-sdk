package com.isu.ekyc.checkStatus;

import static com.isu.ekyc.utils.EkycSdkConstants.NAME;
import static com.isu.ekyc.utils.EkycSdkConstants.sUpdateDetailsUrl;
import static com.isu.ekyc.utils.EkycSdkConstants.sValidatePanUrl;
import static com.isu.ekyc.utils.Utils.closeApplication;
import static com.isu.ekyc.utils.Utils.connectionCheck;
import static com.isu.ekyc.utils.Utils.exitTheProcess;
import static com.isu.ekyc.utils.Utils.handleErrorResponse;
import static com.isu.ekyc.utils.Utils.hideProgressDialog;
import static com.isu.ekyc.utils.Utils.intentToSDK;
import static com.isu.ekyc.utils.Utils.showErrorAlert;
import static com.isu.ekyc.utils.Utils.showProgressDialog;
import static com.isu.ekyc.utils.Utils.showToast;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.transition.TransitionManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.isu.awssdk.core.AwsSdkConstants;
import com.isu.awssdk.core.StringConstants;
import com.isu.awssdk.ui.AwsSdkActivity;
import com.isu.ekyc.R;
import com.isu.ekyc.checkStatus.api.OnboardApiService;
import com.isu.ekyc.checkStatus.api.RetrofitInstance;
import com.isu.ekyc.checkStatus.model.pan.Data;
import com.isu.ekyc.checkStatus.model.pan.PanRequestModel;
import com.isu.ekyc.checkStatus.model.pan.PanResponseModel;
import com.isu.ekyc.configuration.Validate;
import com.isu.ekyc.databinding.ActivityNewPanVerificationBinding;
import com.isu.ekyc.otpverification.WelcomeActivity;
import com.isu.ekyc.posEkyc.AadhaarVerificationActivity;
import com.isu.ekyc.posEkyc.PanVerificationActivity;
import com.isu.ekyc.utils.ConnectionDetector;
import com.isu.ekyc.utils.EkycSdkConstants;
import com.isu.ekyc.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewPanVerificationActivity extends AppCompatActivity {

    ActivityNewPanVerificationBinding activityNewPanVerificationBinding;
    private ProgressDialog progressDialog;
    private String mGetToken = "";
    private String mPanImageUrl = "";
    private String mPanNumber = "";
    private String mPanName = "";
    private String mFatherName = "";
    private String mDOB = "";
    private SessionManager session;
    private TextInputLayout editLayoutPan;
    private TextInputLayout editLayoutPanName;
    private TextInputEditText etPanNumber;
    private TextInputEditText etPanName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityNewPanVerificationBinding = ActivityNewPanVerificationBinding.inflate(getLayoutInflater());
        setContentView(activityNewPanVerificationBinding.getRoot());
        progressDialog = new ProgressDialog(NewPanVerificationActivity.this);

        editLayoutPan = findViewById(R.id.edit_layout_pan);
        editLayoutPanName = findViewById(R.id.edit_layout_panName);
        etPanNumber = findViewById(R.id.pan_verify);
        etPanName = findViewById(R.id.pan_verify_Name);

        session = new SessionManager(this);
        HashMap<String, String> user = session.getUserDetails();
        mGetToken = user.get(SessionManager.KEY_TOKEN);
        InputFilter[] panNumberFilters = new InputFilter[]{
                new InputFilter.AllCaps(), // Convert input to uppercase
                new InputFilter.LengthFilter(10) // Restrict maximum length
        };
        etPanNumber.setFilters(panNumberFilters);
        InputFilter[] panNameFilter = new InputFilter[]{
                new InputFilter.AllCaps(), // Convert input to uppercase
                new InputFilter.LengthFilter(35) // Restrict maximum length
        };
        etPanName.setFilters(panNameFilter);
        editLayoutPan.setEndIconOnClickListener(v -> {
            etPanNumber.setEnabled(true);
            InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInputFromWindow(editLayoutPan.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
            etPanNumber.requestFocus();
            etPanNumber.setSelection(etPanNumber.getText().length());
        });
        editLayoutPanName.setEndIconOnClickListener(v -> {
            etPanName.setEnabled(true);
            InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInputFromWindow(editLayoutPanName.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
            etPanName.requestFocus();
            etPanName.setSelection(etPanName.getText().length());
        });
        etPanNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mPanNumber = String.valueOf(s);
                if (mPanNumber.length() == 10) {
                    hideKeyboard();
                    etPanNumber.clearFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etPanName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mPanName = String.valueOf(s);
                if (mPanName.length() == 35) {
                    hideKeyboard();
                    etPanName.clearFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        activityNewPanVerificationBinding.panVerifyNext.setOnClickListener(v -> {
            if (Boolean.TRUE.equals(connectionCheck(NewPanVerificationActivity.this))) {
                mPanNumber = etPanNumber.getText().toString();
                mPanName = etPanName.getText().toString();
                activityNewPanVerificationBinding.panVerifyPrev.setVisibility(View.GONE);
                if (activityNewPanVerificationBinding.panImage.getDrawable() == null) {
                    showErrorAlert(NewPanVerificationActivity.this, getString(R.string.sdk_error_pan_image));
                } else if (mPanNumber.isEmpty()) {
                    showErrorAlert(NewPanVerificationActivity.this, getString(R.string.sdk_error_pan_number));
                } else if (mPanName.isEmpty()) {
                    showErrorAlert(NewPanVerificationActivity.this, getString(R.string.sdk_error_empty_pan_name));
                } else if (mPanName.length() < 5) {
                    showErrorAlert(NewPanVerificationActivity.this, getString(R.string.sdk_error_pan_name_length));
                } else {
                    String namePerDashboard = NAME.toUpperCase();
                    String PanNameCheck = mPanName.toUpperCase();
                    Double nameMatchScore = Validate.diceCoefficientOptimized(namePerDashboard, PanNameCheck);
                    if (nameMatchScore >= 0.7) {
                        nameMatchScore = nameMatchScore * 100;
                        validatePan(mPanNumber);
                    } else {
                        showErrorAlert(NewPanVerificationActivity.this, getString(R.string.sdk_error_pan_name));
                    }
                }
            }
        });

        activityNewPanVerificationBinding.panImage.setOnClickListener(v -> {
            if (Boolean.TRUE.equals(connectionCheck(NewPanVerificationActivity.this))) {
                intentToSDK(this, mGetToken, StringConstants.PAN_CARD);
            }
        });
        activityNewPanVerificationBinding.panImageSelect.setOnClickListener(v -> {
            if (Boolean.TRUE.equals(connectionCheck(NewPanVerificationActivity.this))) {
                intentToSDK(this, mGetToken, StringConstants.PAN_CARD);
            }
        });
        //On back pressed handled
        OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                exitTheProcess(NewPanVerificationActivity.this, getString(R.string.sdk_do_you_want_to_exit));
            }
        };
        // Add the callback to the onBackPressedDispatcher
        getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback);
    }


    /**
     * show pan input layouts
     *
     * @param value
     * @param pan_name
     */
    public void showPan(String value, String pan_name) {
        TransitionManager.beginDelayedTransition(activityNewPanVerificationBinding.panLayout);
        mPanNumber = value;
        editLayoutPan.setVisibility(View.VISIBLE);
        editLayoutPanName.setVisibility(View.VISIBLE);
        activityNewPanVerificationBinding.panNote.setVisibility(View.VISIBLE);
        activityNewPanVerificationBinding.panImageEdit.setVisibility(View.VISIBLE);
        etPanNumber.setText(mPanNumber);
        etPanName.setText(pan_name);
    }

    /**
     * This api call is to validate pan number
     *
     * @param panNo pan number
     */
    private void validatePan(String panNo) {
        showProgressDialog(NewPanVerificationActivity.this, getString(R.string.sdk_pan_verification_loader_message));
        PanRequestModel panRequest = new PanRequestModel();
        panRequest.setPanNumber(panNo);
        OnboardApiService onboardApiService = RetrofitInstance.retrofit().create(OnboardApiService.class);
        Call<PanResponseModel> validPan = onboardApiService.validatePan(panRequest, sValidatePanUrl);
        validPan.enqueue(new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<PanResponseModel> call, @NonNull Response<PanResponseModel> response) {
                hideProgressDialog();
                if (response.isSuccessful() && response.body() != null) {
                    int status = response.body().getStatus();
                    String message = response.body().getMessage();
                    if (status == 0) {
                        Data data = response.body().getData();
                        Data.PanInquiryResponse panInquiryResponse = data.getPanInquiryResponse();
                        String issueDate = "";
                        if (panInquiryResponse.getBody().getPanDetails().get(0).getLastUpdateDate() != null) {
                            issueDate = panInquiryResponse.getBody().getPanDetails().get(0).getLastUpdateDate();
                        }
                        existingUpdateDetails(issueDate);
                    } else {
                        showErrorAlert(NewPanVerificationActivity.this, message);
                    }
                } else {
                    etPanNumber.setText("");
                    showErrorAlert(NewPanVerificationActivity.this, getString(R.string.input_a_valid_pan_number_retake));
                }
            }

            @Override
            public void onFailure(@NonNull Call<PanResponseModel> call, @NonNull Throwable t) {
                hideProgressDialog();
                etPanNumber.setText("");
                showErrorAlert(NewPanVerificationActivity.this, getString(R.string.input_a_valid_pan_number_retake));
            }
        });
    }

    /**
     * update details API call for existing KYC
     */
    private void existingUpdateDetails(String issueDate) {
        try {
            showProgressDialog(this, getString(R.string.sdk_please_wait));
            JSONObject userUpdateData = new JSONObject();
            JSONObject panInfo = new JSONObject();
            panInfo.put("date_of_issue", issueDate);
            panInfo.put("dob", mDOB);
            panInfo.put("father", mFatherName);
            panInfo.put("name", mPanName);
            panInfo.put("pan", mPanNumber);

            JSONObject panupdate = new JSONObject();
            panupdate.put("pan", mPanNumber);
            panupdate.put("panImage", mPanImageUrl);
            panupdate.put("panInfo", panInfo);
            userUpdateData.put("1", panupdate);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("username", EkycSdkConstants.USER_NAME);
            jsonObject.put("userUpdateData", userUpdateData);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));
            OnboardApiService onboardApiService = RetrofitInstance.retrofit().create(OnboardApiService.class);
            onboardApiService.updateOnboardData(body, sUpdateDetailsUrl, mGetToken).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                    hideProgressDialog();
                    if (response.isSuccessful() && response.body() != null) {
                        try {
                            String updateResponse = response.body().string();
                            JSONObject updateJson = new JSONObject(updateResponse);
                            String status = updateJson.getString("status");
                            String msg = updateJson.getString("statusDesc");
                            if (status.equals("0")) {
                                closeApplication(NewPanVerificationActivity.this, msg);
                            } else {
                                showToast(NewPanVerificationActivity.this, msg);
                            }

                        } catch (JSONException | IOException e) {
                            showToast(NewPanVerificationActivity.this, getString(R.string.something_went_wrong_please_try_again));
                        }
                    } else {
                        handleErrorResponse(NewPanVerificationActivity.this, response.errorBody());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                    hideProgressDialog();
                    showToast(NewPanVerificationActivity.this, t.getLocalizedMessage());
                }
            });

        } catch (JSONException e) {
            showToast(NewPanVerificationActivity.this, getString(R.string.something_went_wrong_please_try_again));
        }
    }

    /**
     * Here extracted data get from AWS SDK
     */

    protected void onResume() {
        super.onResume();

        if (AwsSdkConstants.INSTANCE.getTOKEN_EXPIRED()){
            closeApplication(this, getString(R.string.session_expired));
            AwsSdkConstants.INSTANCE.setTOKEN_EXPIRED(false);
        }else {
            String imgType = AwsSdkConstants.INSTANCE.getIMAGE_TYPE();
            if (imgType.equalsIgnoreCase(StringConstants.PAN_CARD)) {
                mPanName = AwsSdkConstants.INSTANCE.getPanDetails().getPanName();
                if (!AwsSdkConstants.INSTANCE.getPanDetails().getPanDob().isEmpty()) {
                    mDOB = AwsSdkConstants.INSTANCE.getPanDetails().getPanDob();
                }
                if (!AwsSdkConstants.INSTANCE.getPanDetails().getPanFatherName().isEmpty()) {
                    mFatherName = AwsSdkConstants.INSTANCE.getPanDetails().getPanFatherName();
                }
                mPanNumber = AwsSdkConstants.INSTANCE.getPanDetails().getPanNumber();
                String panImageUrl = AwsSdkConstants.INSTANCE.getPanDetails().getPanFirebaseUrl();
                activityNewPanVerificationBinding.panImage.setVisibility(View.VISIBLE);
                activityNewPanVerificationBinding.textView4.setVisibility(View.GONE);
                activityNewPanVerificationBinding.panImageSelect.setVisibility(View.GONE);
                Glide.with(this).load(panImageUrl)
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                activityNewPanVerificationBinding.panImage.setVisibility(View.GONE);
                                activityNewPanVerificationBinding.textView4.setVisibility(View.VISIBLE);
                                activityNewPanVerificationBinding.panImageSelect.setVisibility(View.VISIBLE);
                                activityNewPanVerificationBinding.panImage.setImageDrawable(null);
                                showToast(NewPanVerificationActivity.this, getString(R.string.sdk_error_recapture_image));
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                return false;
                            }
                        }).diskCacheStrategy(DiskCacheStrategy.ALL).
                        fitCenter().placeholder(R.drawable.progress_animation).into(activityNewPanVerificationBinding.panImage);
                mPanImageUrl = panImageUrl;
                showPan(mPanNumber, mPanName);

            }
            AwsSdkConstants.INSTANCE.setIMAGE_TYPE("");
        }
    }


    /**
     * hide keyboard pop-up
     */
    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}