package com.isu.ekyc.checkStatus.model.pan;

import com.google.gson.annotations.SerializedName;
import com.isu.ekyc.utils.CSCStringConstants;

public class PanRequestModel{
	@SerializedName(CSCStringConstants.PAN_1)
	private String panNumber;

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}
}