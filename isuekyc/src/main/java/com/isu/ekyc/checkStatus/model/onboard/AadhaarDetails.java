package com.isu.ekyc.checkStatus.model.onboard;

import com.google.gson.annotations.SerializedName;

public class AadhaarDetails {

	@SerializedName("aadhaarFrontImage")
	private String aadhaarFrontImage;

	@SerializedName("aadhar")
	private String aadhar;

	@SerializedName("aadhaarBackImage")
	private String aadhaarBackImage;

	@SerializedName("frontAadharInfo")
	private FrontAadhaarInfo frontAadhaarInfo;

	@SerializedName("backAadharInfo")
	private BackAadhaarInfo backAadhaarInfo;

	public void setAadhaarFrontImage(String aadhaarFrontImage) {
		this.aadhaarFrontImage = aadhaarFrontImage;
	}

	public void setAadhar(String aadhar) {
		this.aadhar = aadhar;
	}

	public void setAadhaarBackImage(String aadhaarBackImage) {
		this.aadhaarBackImage = aadhaarBackImage;
	}

	public void setFrontAadharInfo(FrontAadhaarInfo frontAadhaarInfo) {
		this.frontAadhaarInfo = frontAadhaarInfo;
	}

	public void setBackAadharInfo(BackAadhaarInfo backAadhaarInfo) {
		this.backAadhaarInfo = backAadhaarInfo;
	}
}