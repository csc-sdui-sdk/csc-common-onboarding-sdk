package com.isu.ekyc.checkStatus.commonOnboarding.onboarding;

import com.google.gson.annotations.SerializedName;

public class APIResponse {

	@SerializedName("pincode")
	private String pincode;

	@SerializedName("existingverifyMail")
	private String existingverifyMail;


	@SerializedName("existingvalidatepan")
	private String existingvalidatepan;

	@SerializedName("newverifyMail")
	private String newverifyMail;

	@SerializedName("locationList")
	private String locationList;

	@SerializedName("existingPhonevalidateAPI")
	private String existingPhonevalidateAPI;

	@SerializedName("existingonboardApi")
	private String existingonboardApi;
	@SerializedName("updateDetails")
	private String updateDetails;
	@SerializedName("existingPhonerequestOPTService")
	private String existingPhonerequestOPTService;

	@SerializedName("existingfetchstatus")
	private String existingfetchstatus;

	@SerializedName("existingPhoneResendOtp")
	private String existingPhoneResendOtp;

	@SerializedName("existingsendEmail")
	private String existingsendEmail;

	public String getPincode() {
		return pincode;
	}

	public String getExistingverifyMail() {
		return existingverifyMail;
	}

	public String getExistingvalidatepan() {
		return existingvalidatepan;
	}

	public String getLocationList() {
		return locationList;
	}

	public String getExistingPhonevalidateAPI() {
		return existingPhonevalidateAPI;
	}

	public String getExistingonboardApi() {
		return existingonboardApi;
	}

	public String getUpdateDetails() {
		return updateDetails;
	}

	public String getExistingPhonerequestOPTService() {
		return existingPhonerequestOPTService;
	}

	public String getExistingfetchstatus() {
		return existingfetchstatus;
	}

	public String getExistingPhoneResendOtp() {
		return existingPhoneResendOtp;
	}

	public String getExistingsendEmail() {
		return existingsendEmail;
	}
}