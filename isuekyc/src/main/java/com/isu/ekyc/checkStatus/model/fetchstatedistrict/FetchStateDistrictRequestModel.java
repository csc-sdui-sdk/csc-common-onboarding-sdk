package com.isu.ekyc.checkStatus.model.fetchstatedistrict;

import com.google.gson.annotations.SerializedName;

public class FetchStateDistrictRequestModel{

	@SerializedName("village_code")
	private String villageCode;

	@SerializedName("subdistrict_code")
	private String subdistrictCode;

	@SerializedName("district_code")
	private String districtCode;

	@SerializedName("response_type")
	private String responseType;

	@SerializedName("state_code")
	private String stateCode;

	@SerializedName("location_type")
	private String locationType;

	public void setVillageCode(String villageCode) {
		this.villageCode = villageCode;
	}

	public void setSubdistrictCode(String subdistrictCode) {
		this.subdistrictCode = subdistrictCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}
}