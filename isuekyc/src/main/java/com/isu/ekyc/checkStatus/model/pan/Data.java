package com.isu.ekyc.checkStatus.model.pan;

import com.google.gson.annotations.SerializedName;
import com.isu.ekyc.utils.CSCStringConstants;

import java.util.List;

public class Data{
	@SerializedName(CSCStringConstants.PAN_INQUIRY_RESPONSE)
	private PanInquiryResponse panInquiryResponse;

	public PanInquiryResponse getPanInquiryResponse() {
		return panInquiryResponse;
	}

	public static class PanInquiryResponse{
		@SerializedName(CSCStringConstants.BODY)
		private Body body;

		public Body getBody() {
			return body;
		}

	}
	public static class Body{
		@SerializedName(CSCStringConstants.PAN_DETAILS)
		private List<PanDetailsItem> panDetails;

		public List<PanDetailsItem> getPanDetails() {
			return panDetails;
		}
	}
}