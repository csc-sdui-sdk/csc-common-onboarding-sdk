package com.isu.ekyc.checkStatus.model;

import com.google.gson.annotations.SerializedName;

public class EmailResponseModel{
	@SerializedName("statusDesc")
	private String statusDesc;

	@SerializedName("status")
	private Integer status;

	public String getStatusDesc() {
		return statusDesc;
	}

	public Integer getStatus() {
		return status;
	}
}