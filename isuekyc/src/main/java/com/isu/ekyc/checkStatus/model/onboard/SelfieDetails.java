package com.isu.ekyc.checkStatus.model.onboard;

import com.google.gson.annotations.SerializedName;

public class SelfieDetails {

	@SerializedName("match_score")
	private String matchScore;

	@SerializedName("selfieUrl")
	private String selfieUrl;

	public void setMatchScore(String matchScore) {
		this.matchScore = matchScore;
	}

	public void setSelfieUrl(String selfieUrl) {
		this.selfieUrl = selfieUrl;
	}
}