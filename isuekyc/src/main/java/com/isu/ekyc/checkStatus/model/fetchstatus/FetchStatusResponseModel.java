package com.isu.ekyc.checkStatus.model.fetchstatus;

import com.google.gson.annotations.SerializedName;
import com.isu.ekyc.utils.CSCStringConstants;

public class FetchStatusResponseModel{

	@SerializedName(CSCStringConstants.COMMON_ONBOARD_STATUS)
	private String commonOnboardStatus;

	@SerializedName(CSCStringConstants.STATUS_DESC)
	private String statusDesc;

	@SerializedName(CSCStringConstants.STEPS)
	private Steps steps;

	@SerializedName(CSCStringConstants.REMARKS)
	private Remarks remarks;

	@SerializedName(CSCStringConstants.STATUS)
	private Integer status;

	@SerializedName(CSCStringConstants.COMMON_ONBOARDING)
	private Boolean commonOnboarding;

	public String getCommonOnboardStatus() {
		return commonOnboardStatus;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public Steps getSteps() {
		return steps;
	}

	public Remarks getRemarks() {
		return remarks;
	}

	public Integer getStatus() {
		return status;
	}

	public Boolean getCommonOnboarding() {
		return commonOnboarding;
	}

	public static class Steps{

		@SerializedName(CSCStringConstants.SUB_STATUS_1)
		private Integer step1;

		@SerializedName(CSCStringConstants.SUB_STATUS_2)
		private Integer step2;

		@SerializedName(CSCStringConstants.SUB_STATUS_3)
		private Integer step3;

		@SerializedName(CSCStringConstants.SUB_STATUS_4)
		private Integer step4;

		@SerializedName(CSCStringConstants.SUB_STATUS_5)
		private Integer step5;

		@SerializedName(CSCStringConstants.SUB_STATUS_6)
		private Integer step6;
	}

	public static class Remarks{
		@SerializedName(CSCStringConstants.DESCRIPTION)
		private String description;

		@SerializedName(CSCStringConstants.COMMENTS)
		private String comments;


		public String getDescription() {
			return description;
		}

		public String getComments() {
			return comments;
		}
	}

}