package com.isu.ekyc.checkStatus.model.fetchstatedistrict;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FetchStateDistrictResponseModel {

	@SerializedName("data")
	private ArrayList<DataItem> districtAndStateList;

	@SerializedName("count")
	private Integer count;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private Integer status;

	public ArrayList<DataItem> getData() {
		return districtAndStateList;
	}

	public Integer getCount() {
		return count;
	}

	public String getMessage() {
		return message;
	}

	public Integer getStatus() {
		return status;
	}
}