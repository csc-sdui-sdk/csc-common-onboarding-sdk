package com.isu.ekyc.checkStatus.commonOnboarding

import com.google.gson.annotations.SerializedName

data class OnboardUiRequestModel(
    @field:SerializedName("user_name")
    val username: String? = "",

    @field:SerializedName("bank_code")
    val bankcode: String? = ""
)
