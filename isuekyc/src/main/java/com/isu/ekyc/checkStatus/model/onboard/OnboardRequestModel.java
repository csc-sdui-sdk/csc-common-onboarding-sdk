package com.isu.ekyc.checkStatus.model.onboard;

import com.google.gson.annotations.SerializedName;

public class OnboardRequestModel{

	@SerializedName("dataObj")
	private DataObj dataObj;

	@SerializedName("type")
	private String type;

	@SerializedName("username")
	private String username;

	public void setDataObj(DataObj dataObj) {
		this.dataObj = dataObj;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}