package com.isu.ekyc.checkStatus.commonOnboarding

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.isu.ekyc.R
import com.isu.ekyc.checkStatus.NewAadhaarVerificationActivity
import com.isu.ekyc.checkStatus.NewEmailVerificationActivity
import com.isu.ekyc.checkStatus.NewGSTActivity
import com.isu.ekyc.checkStatus.NewPanVerificationActivity
import com.isu.ekyc.checkStatus.api.OnboardApiService
import com.isu.ekyc.checkStatus.api.RetrofitInstance
import com.isu.ekyc.checkStatus.commonOnboarding.onboarding.OnboardingUiResponseModel
import com.isu.ekyc.checkStatus.model.fetchstatus.FetchStatusRequestBody
import com.isu.ekyc.checkStatus.model.fetchstatus.FetchStatusResponseModel
import com.isu.ekyc.databinding.ActivityOnBoardingEntryBinding
import com.isu.ekyc.ekycDataBase.EkycDataTable
import com.isu.ekyc.ekycDataBase.EkycViewModelFactory
import com.isu.ekyc.ekycDataBase.UserEkycRepository
import com.isu.ekyc.ekycDataBase.viewmodel.UserEkycViewModel
import com.isu.ekyc.emailverfication.EmailVerificationActivity
import com.isu.ekyc.otpverification.PhoneNumberActivity
import com.isu.ekyc.otpverification.WelcomeActivity
import com.isu.ekyc.posEkyc.AadhaarVerificationActivity
import com.isu.ekyc.posEkyc.EkycBasicInfoActivity
import com.isu.ekyc.utils.CSCStringConstants.APPROVED
import com.isu.ekyc.utils.CSCStringConstants.KYC_IS
import com.isu.ekyc.utils.CSCStringConstants.NULL_CHECK
import com.isu.ekyc.utils.CSCStringConstants.PENDING
import com.isu.ekyc.utils.CSCStringConstants.REJECTED
import com.isu.ekyc.utils.CSCStringConstants.REUPLOAD
import com.isu.ekyc.utils.CSCStringConstants.SUB_STATUS_0
import com.isu.ekyc.utils.CSCStringConstants.SUB_STATUS_1
import com.isu.ekyc.utils.CSCStringConstants.SUB_STATUS_2
import com.isu.ekyc.utils.CSCStringConstants.SUB_STATUS_3
import com.isu.ekyc.utils.CSCStringConstants.SUB_STATUS_4
import com.isu.ekyc.utils.CSCStringConstants.SUB_STATUS_5
import com.isu.ekyc.utils.CSCStringConstants.SUB_STATUS_6
import com.isu.ekyc.utils.ConnectionDetector
import com.isu.ekyc.utils.EkycSdkConstants
import com.isu.ekyc.utils.EkycSdkConstants.EkycStatusCode
import com.isu.ekyc.utils.EkycSdkConstants.TYPE
import com.isu.ekyc.utils.EkycSdkConstants.sFetchStateLocationUrl
import com.isu.ekyc.utils.EkycSdkConstants.sFetchStatusUrl
import com.isu.ekyc.utils.EkycSdkConstants.sOnboardUrl
import com.isu.ekyc.utils.EkycSdkConstants.sPinValidateUrl
import com.isu.ekyc.utils.EkycSdkConstants.sResendOtpUrl
import com.isu.ekyc.utils.EkycSdkConstants.sSendEmailUrl
import com.isu.ekyc.utils.EkycSdkConstants.sSendOtpUrl
import com.isu.ekyc.utils.EkycSdkConstants.sUpdateDetailsUrl
import com.isu.ekyc.utils.EkycSdkConstants.sValidatePanUrl
import com.isu.ekyc.utils.EkycSdkConstants.sVerifyEmailUrl
import com.isu.ekyc.utils.EkycSdkConstants.sVerifyOtpUrl
import com.isu.ekyc.utils.SessionManager
import com.isu.ekyc.utils.Utils.closeApplication
import com.isu.ekyc.utils.Utils.hideProgressDialog
import com.isu.ekyc.utils.Utils.showProgressDialog
import com.isu.ekyc.utils.Utils.showToast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 *  `OnBoardingEntry` checks status of user kyc and proceed to next step.
 *  Before proceeding to next step is the user has already stated kyc process,
 *  then checks sub status from database and proceed further.
 */
class OnBoardingEntry : AppCompatActivity() {
    private lateinit var userKycViewModel: UserEkycViewModel
    private lateinit var allUserData: EkycDataTable
    private var subStatus: Int = 0
    private var databaseUserName = ""
    private var _token = ""
    private lateinit var session: SessionManager
    private lateinit var binding: ActivityOnBoardingEntryBinding
    private val permissionList = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )
    private var count = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_on_boarding_entry)
        session = SessionManager(this)
        val user: HashMap<String, String> = session.userDetails
        _token = user[SessionManager.KEY_TOKEN]!!

        val userKycRepository = UserEkycRepository(application)
        userKycViewModel = ViewModelProvider(
            this, EkycViewModelFactory(userKycRepository)
        )[UserEkycViewModel::class.java]


        allUserData = EkycDataTable()
        if (userKycViewModel.allData != null) {
            allUserData = userKycViewModel.allData
            if (allUserData.username != EkycSdkConstants.USER_NAME) {
                userKycViewModel.delete()
                allUserData = EkycDataTable()
                allUserData.username = EkycSdkConstants.USER_NAME
                userKycViewModel.insert(allUserData)
            } else {
                databaseUserName = allUserData.username
            }
        }
        permissionRequest()
    }

    private fun permissionRequest() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_MEDIA_IMAGES
            ) == PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ) == PackageManager.PERMISSION_GRANTED) && ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.BLUETOOTH_CONNECT
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            if (connectionCheck(this)) {
                callOnboardSdiApiCall()
            }
        } else {
            permissionRequest.launch(permissionList)
        }
    }


    private val permissionRequest: ActivityResultLauncher<Array<String>> = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { result: Map<String, Boolean> ->
        val mutableResult = result.toMutableMap()
        if (mutableResult["android.permission.CAMERA"] == true &&
            mutableResult["android.permission.ACCESS_COARSE_LOCATION"] == true &&
            mutableResult["android.permission.ACCESS_FINE_LOCATION"] == true
        ) {
            if (connectionCheck(this)) {
                callOnboardSdiApiCall()
            }
        } else {
            if (count < 2) {
                permissionRequest()
                count++
            } else {
                closeApplication(
                    this@OnBoardingEntry,
                    getString(R.string.without_permission_feature_msg)
                )
            }
        }
    }


    /**
     * Fetch onboard dynamic APIs
     */
    private fun callOnboardSdiApiCall() {
        showProgressDialog(this@OnBoardingEntry, getString(R.string.sdk_please_wait))
        val onboardUiRequestModel = OnboardUiRequestModel(EkycSdkConstants.ADMIN_NAME, "csc")
        val apiService: OnboardApiService =
            RetrofitInstance.dynamicUiInstanceMaker().create(OnboardApiService::class.java)
        val rechargeUiResponseModelCall: Call<OnboardingUiResponseModel> =
            apiService.getOnboardUrl(onboardUiRequestModel)
        rechargeUiResponseModelCall.enqueue(object : Callback<OnboardingUiResponseModel?> {
            override fun onResponse(
                call: Call<OnboardingUiResponseModel?>,
                response: Response<OnboardingUiResponseModel?>,
            ) {
                hideProgressDialog()
                if (response.isSuccessful && response.body() != null) {
                    sSendOtpUrl = response.body()!!.data.uiJson.commonOnboardingUrls.existingPhonerequestOPTService
                    sVerifyOtpUrl = response.body()!!.data.uiJson.commonOnboardingUrls.existingPhonevalidateAPI
                    sSendEmailUrl = response.body()!!.data.uiJson.commonOnboardingUrls.existingsendEmail
                    sVerifyEmailUrl = response.body()!!.data.uiJson.commonOnboardingUrls.existingverifyMail
                    sValidatePanUrl = response.body()!!.data.uiJson.commonOnboardingUrls.existingvalidatepan
                    sOnboardUrl = response.body()!!.data.uiJson.commonOnboardingUrls.existingonboardApi
                    sFetchStatusUrl = response.body()!!.data.uiJson.commonOnboardingUrls.existingfetchstatus
                    sResendOtpUrl = response.body()!!.data.uiJson.commonOnboardingUrls.existingPhoneResendOtp
                    sUpdateDetailsUrl = response.body()!!.data.uiJson.commonOnboardingUrls.updateDetails
                    sPinValidateUrl = response.body()!!.data.uiJson.commonOnboardingUrls.pincode
                    sFetchStateLocationUrl = response.body()!!.data.uiJson.commonOnboardingUrls.locationList

                    if (TYPE == "KycStatus") {
                        fetchMerchantOnboardStatus(EkycSdkConstants.USER_NAME)
                    } else {
                        if (EkycStatusCode == "5") {
                            fetchMerchantOnboardStatus(EkycSdkConstants.USER_NAME)
                        } else {
                            startKycFromLocalDB()
                        }
                    }

                } else {
                    showToast(this@OnBoardingEntry, response.message())
                }
            }

            override fun onFailure(call: Call<OnboardingUiResponseModel?>, t: Throwable) {
                hideProgressDialog()
                showToast(this@OnBoardingEntry, t.message)
            }
        })
    }

    /**
     * Fetch onboard kyc status calling fetch status API
     */
    private fun fetchMerchantOnboardStatus(onBoardUser: String) {
        showProgressDialog(this@OnBoardingEntry, getString(R.string.sdk_please_wait))
        val fetchStatusRequest = FetchStatusRequestBody()
        val onboardApiService = RetrofitInstance.retrofit().create(OnboardApiService::class.java)
        fetchStatusRequest.setUsername(onBoardUser)
        val fetchStatus = onboardApiService.fetchStatus(
            fetchStatusRequest,
            sFetchStatusUrl,
            _token
        )

        fetchStatus.enqueue(object : Callback<FetchStatusResponseModel> {
            override fun onResponse(
                call: Call<FetchStatusResponseModel>,
                response: Response<FetchStatusResponseModel>
            ) {
                hideProgressDialog()
                if (response.isSuccessful && response.body() != null) {
                    val fetchStatusResponse = response.body()
                    fetchStatusResponse?.let {
                        val status = it.status.toString()
                        val commonBoarding = it.commonOnboardStatus
                        when {
                            commonBoarding.equals(REJECTED, ignoreCase = true) || commonBoarding.equals(
                                REUPLOAD,
                                ignoreCase = true
                            ) -> {
                                val remarks = it.remarks
                                val description = remarks.description
                                val comments = remarks.comments
                                kycStatusDialog("$KYC_IS $commonBoarding!$description", comments)
                            }

                            else -> {
                                if (status.equals(SUB_STATUS_0, ignoreCase = true)) {
                                    if (commonBoarding.equals(
                                            APPROVED,
                                            ignoreCase = true
                                        ) || commonBoarding.equals(PENDING, ignoreCase = true)
                                    ) {
                                        closeApplication(this@OnBoardingEntry, "$KYC_IS $commonBoarding !")
                                    } else {
                                        startKycFromLocalDB()
                                    }
                                } else {
                                    startKycFromLocalDB()
                                }
                            }
                        }
                    } ?: closeApplication(this@OnBoardingEntry, getString(R.string.unable_to_fetch_data))
                } else {
                    closeApplication(this@OnBoardingEntry, getString(R.string.unable_to_fetch_data))
                }
            }

            override fun onFailure(call: Call<FetchStatusResponseModel>, t: Throwable) {
                hideProgressDialog()
                closeApplication(this@OnBoardingEntry, getString(R.string.unable_to_fetch_data))
            }

        })
    }

    /**
     * This function is used to check KYC status from database before proceeding
     * By default KYC process starts with phone number verification
     * **Status 1** - Phone number verification completed, need to complete email verification
     * **Status 2** - Email verification completed, need to complete PAN verification followed by welcome screen and user consent agreement
     * **Status 3,4** - PAN verification completed, need to complete Aadhaar verification and face match
     * **Status 5,6** - Aadhaar verification completed, need to completed last process of KYC i.e basic information
     */
    fun startKycFromLocalDB() {
        subStatus = if (allUserData.subStatus != null) {
            allUserData.subStatus.toInt()
        } else {
            0
        }
        try {
            if (!databaseUserName.equals(EkycSdkConstants.USER_NAME, ignoreCase = true)) {
                userKycViewModel.deleteAllData()
                allUserData.username = EkycSdkConstants.USER_NAME
                userKycViewModel.insert(allUserData)
                startNewActivity(PhoneNumberActivity::class.java)
            } else {
                when (subStatus) {
                    1 -> {//status == 1, send to email verification , phone number verified.
                        startNewActivity(EmailVerificationActivity::class.java)
                    }

                    2 -> {// status == 2 send to PanVerificationActivity, passing through kyc agreement.
                        startNewActivity(WelcomeActivity::class.java)
                    }

                    3, 4 -> {//status == 3,4, pan has been added, send to aadhaar verification with pan card name
                        startNewActivity(AadhaarVerificationActivity::class.java)
                    }

                    5, 6 -> {// status == 5,6 means aadhaar has been verified with pan name, send to basic info with aadhaar details.
                        startNewActivity(EkycBasicInfoActivity::class.java)
                    }

                    else -> {
                        startNewActivity(PhoneNumberActivity::class.java)
                    }
                }
            }
        } catch (e: java.lang.Exception) {
            userKycViewModel.deleteAllData()
            allUserData.username = EkycSdkConstants.USER_NAME
            userKycViewModel.insert(allUserData)
            startNewActivity(PhoneNumberActivity::class.java)
        }
    }

    /**
     * Show dialog before proceeding to rejection process
     * @param message reason why kyc rejected
     * @param code to check which kyc process is rejected
     * @param comments reason need to show if condition not satisfy
     */
    private fun kycStatusDialog(message: String, comments: String) {
        val builder = AlertDialog.Builder(this@OnBoardingEntry, R.style.AlertDialogTheme)
        builder.setMessage(message)
        builder.setTitle("")
        builder.setCancelable(false)
        builder.setPositiveButton(getString(R.string.sdk_ok_btn)) { dialog: DialogInterface, _: Int ->
            dialog.dismiss()
            rejectionProcess()
        }
        builder.show()
    }

    /**
     * To check and proceed to required kyc rejected process
     * @param code to check which kyc process is rejected
     * @param comments reason need to show if condition not satisfy
     */
    private fun rejectionProcess() {
        startKycFromLocalDB()
        // As there is no module wise rejection in CSC for now. Below code is commented until
        // further instruction.

       /* when {
            code.equals(SUB_STATUS_0, ignoreCase = true) || code.equals(
                NULL_CHECK, ignoreCase = true
            ) || code.equals(
                "", ignoreCase = true
            ) -> {
                startKycFromLocalDB()
            }

            code.equals(SUB_STATUS_1, ignoreCase = true) -> {
                startNewActivity(NewPanVerificationActivity::class.java)
            }

            code.equals(SUB_STATUS_2, ignoreCase = true) || code.equals(SUB_STATUS_4, ignoreCase = true) -> {
                startNewActivity(NewAadhaarVerificationActivity::class.java)
            }

            code.equals(SUB_STATUS_3, ignoreCase = true) -> {
                startNewActivity(NewEmailVerificationActivity::class.java)
            }

            code.equals(SUB_STATUS_5, ignoreCase = true) -> {
                startNewActivity(NewGSTActivity::class.java)
            }

            code.equals(SUB_STATUS_6, ignoreCase = true) -> {
                startKycFromLocalDB()
            }

            else -> {
                showToast(this@OnBoardingEntry, comments)
            }
        }*/
    }

    /**
     * Starts a new activity based on the destination activity class.
     * @param destinationActivity destination class provided
     */
    private fun startNewActivity(destinationActivity: Class<*>) {
        val intent = Intent(this@OnBoardingEntry, destinationActivity)
        startActivity(intent)
        finish()
    }

    /**
     * Check internet connection and dismiss dialog
     */
    private fun connectionCheck(context: Context?): Boolean {
        val connectionDetector = ConnectionDetector(context)
        return if (connectionDetector.isConnectingToInternet) {
            true
        } else {
            if (context != null) {
                AlertDialog.Builder(context, R.style.AlertDialogTheme)
                    .setTitle(context.getString(R.string.sdk_alert))
                    .setCancelable(false)
                    .setMessage(context.getString(R.string.sdk_no_internet_connection))
                    .setPositiveButton(context.getString(R.string.retry)) { dialogInterface: DialogInterface, _: Int ->
                        if (connectionCheck(context)) {
                            dialogInterface.dismiss()
                            callOnboardSdiApiCall()
                        }
                    }
                    .setNegativeButton(context.getString(R.string.sdk_exit)) { dialogInterface: DialogInterface, _: Int ->
                        dialogInterface.dismiss()
                        (context as Activity).finishAffinity()
                    }
                    .show()
            }
            false
        }
    }
}