package com.isu.ekyc.checkStatus.model;

public class FetchStatusRequest{
	private String role;
	private String mobile;

	public String getAdmin_name() {
		return admin_name;
	}

	public void setAdmin_name(String admin_name) {
		this.admin_name = admin_name;
	}

	private String admin_name;

	public String getRole(){
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}



	public String getMobile(){
		return mobile;
	}


}
