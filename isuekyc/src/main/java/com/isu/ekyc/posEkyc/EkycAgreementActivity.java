package com.isu.ekyc.posEkyc;

import static com.isu.ekyc.utils.Utils.exitTheProcess;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.lifecycle.ViewModelProvider;

import com.isu.ekyc.R;
import com.isu.ekyc.configuration.Validate;
import com.isu.ekyc.ekycDataBase.EkycDataTable;
import com.isu.ekyc.ekycDataBase.EkycViewModelFactory;
import com.isu.ekyc.ekycDataBase.UserEkycRepository;
import com.isu.ekyc.ekycDataBase.viewmodel.UserEkycViewModel;
import com.isu.ekyc.otpverification.WelcomeActivity;
import com.isu.ekyc.utils.OnboardingIntentFactory;

/**
 * <b>EkycAgreementActivity</b> here you need to agree for agreement displayed on screen
 */
public class EkycAgreementActivity extends AppCompatActivity {
    private Button proceed;
    private SwitchCompat residenceSwitch;
    private SwitchCompat presentSwitch;
    private SwitchCompat ageSwitch;
    boolean mResidence = false;
    boolean mPresent = false;
    boolean mAge = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ekyc_agreement);
        residenceSwitch = findViewById(R.id.ekyc_agreement_residence);
        presentSwitch = findViewById(R.id.ekyc_agreement_present);
        ageSwitch = findViewById(R.id.ekyc_agreement_age);
        proceed = findViewById(R.id.ekyc_agreement_proceed);

        proceed.setOnClickListener(view -> {
            Intent i = OnboardingIntentFactory.returnPanVerificationActivityIntent(EkycAgreementActivity.this);
            i.putExtra("data", "0");
            startActivity(i);
            finish();
        });

        residenceSwitch.setChecked(mResidence);
        presentSwitch.setChecked(mPresent);
        ageSwitch.setChecked(mAge);

        residenceSwitch.setOnClickListener(view -> activeProceed());
        presentSwitch.setOnClickListener(view -> activeProceed());
        ageSwitch.setOnClickListener(view -> activeProceed());
        //On back pressed handled
        OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                exitTheProcess(EkycAgreementActivity.this, getString(R.string.sdk_do_you_want_to_exit));
            }
        };
        // Add the callback to the onBackPressedDispatcher
        getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        activeProceed();
    }

    public void activeProceed() {
        mResidence = residenceSwitch.isChecked();
        mPresent = presentSwitch.isChecked();
        mAge = ageSwitch.isChecked();
        if (mResidence && mPresent && mAge) {
            proceed.setEnabled(true);
            proceed.setAlpha(1.0f);
        } else {
            proceed.setEnabled(false);
            proceed.setAlpha(0.5f);
        }
    }

}
