package com.isu.ekyc.posEkyc;

import static com.isu.ekyc.configuration.Validate.isValidPanCard;
import static com.isu.ekyc.utils.EkycSdkConstants.NAME;
import static com.isu.ekyc.utils.EkycSdkConstants.sValidatePanUrl;
import static com.isu.ekyc.utils.Utils.closeApplication;
import static com.isu.ekyc.utils.Utils.connectionCheck;
import static com.isu.ekyc.utils.Utils.exitTheProcess;
import static com.isu.ekyc.utils.Utils.hideKeyboard;
import static com.isu.ekyc.utils.Utils.hideProgressDialog;
import static com.isu.ekyc.utils.Utils.intentToSDK;
import static com.isu.ekyc.utils.Utils.showErrorAlert;
import static com.isu.ekyc.utils.Utils.showProgressDialog;
import static com.isu.ekyc.utils.Utils.showToast;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.isu.awssdk.core.AwsSdkConstants;
import com.isu.awssdk.core.StringConstants;
import com.isu.ekyc.R;
import com.isu.ekyc.checkStatus.api.OnboardApiService;
import com.isu.ekyc.checkStatus.api.RetrofitInstance;
import com.isu.ekyc.checkStatus.model.pan.Data;
import com.isu.ekyc.checkStatus.model.pan.PanRequestModel;
import com.isu.ekyc.checkStatus.model.pan.PanResponseModel;
import com.isu.ekyc.configuration.OnBoardSharedPreference;
import com.isu.ekyc.configuration.Validate;
import com.isu.ekyc.databinding.ActivityPanVerificationBinding;
import com.isu.ekyc.ekycDataBase.EkycDataTable;
import com.isu.ekyc.ekycDataBase.EkycViewModelFactory;
import com.isu.ekyc.ekycDataBase.UserEkycRepository;
import com.isu.ekyc.ekycDataBase.viewmodel.UserEkycViewModel;
import com.isu.ekyc.utils.EkycSdkConstants;
import com.isu.ekyc.utils.OnboardingIntentFactory;
import com.isu.ekyc.utils.SessionManager;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * <b>PanVerificationActivity</b> this activity captures pan card,
 * and validates extracted details, edit the extracted details is allowed.
 * Name extracted from pan card should match with user profile name
 */
public class PanVerificationActivity extends AppCompatActivity {
    private static final String TAG = PanVerificationActivity.class.getSimpleName();
    OnBoardSharedPreference preference;
    JSONObject docObject;
    private TextInputEditText panET, panNameET;
    private TextInputLayout edit_layout_pan;
    private TextView ntPAN;
    private ImageView panImage, panSelect;
    private String mPanNumber;
    private String mPanName;
    private String mPanDob;
    private String mToken;
    private LinearLayout panLayout;
    private Button next, prev;
    private ActivityPanVerificationBinding binding;
    private EkycDataTable ekycDataTable;
    private UserEkycViewModel userEkycViewModel;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPanVerificationBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        init();
        //On back pressed handled
        OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                exitTheProcess(PanVerificationActivity.this, getString(R.string.sdk_do_you_want_to_exit));
            }
        };
        // Add the callback to the onBackPressedDispatcher
        getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback);
    }

    @SuppressLint("SetTextI18n")
    private void init() {
        session = new SessionManager(this);
        HashMap<String, String> user = session.getUserDetails();
        mToken = user.get(SessionManager.KEY_TOKEN);
        UserEkycRepository userEkycRepository = new UserEkycRepository(getApplication());
        userEkycViewModel = new ViewModelProvider(this, new EkycViewModelFactory(userEkycRepository)).get(UserEkycViewModel.class);
        ekycDataTable = new EkycDataTable();
        ekycDataTable = userEkycViewModel.getAllData();
        preference = new OnBoardSharedPreference(this);
        Log.e(TAG, "onCreate: docObject" + docObject);

        panSelect = findViewById(R.id.pan_image_select);
        panImage = findViewById(R.id.pan_image);

        binding.panVerifyDOB.setOnClickListener(v -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                datePicker();
            }
        });

        panImage.setOnClickListener(v -> {
            if (Boolean.TRUE.equals(connectionCheck(this))) {
                intentToSDK(this, mToken, StringConstants.PAN_CARD);
            }
        });

        panSelect.setOnClickListener(v -> {
            if (Boolean.TRUE.equals(connectionCheck(this))) {
                intentToSDK(this, mToken, StringConstants.PAN_CARD);
            }
        });

        panET = findViewById(R.id.pan_verify);
        panNameET = findViewById(R.id.pan_verify_Name);
        ntPAN = findViewById(R.id.pan_note);
        edit_layout_pan = findViewById(R.id.edit_layout_pan);
        //edit_layout_panName = findViewById(R.id.edit_layout_panName);

        InputFilter[] panNumberFilter = new InputFilter[]{
                new InputFilter.AllCaps(), // Convert input to uppercase
                new InputFilter.LengthFilter(10) // Restrict maximum length
        };
        panET.setFilters(panNumberFilter);
        InputFilter[] panNameFilter = new InputFilter[]{
                new InputFilter.AllCaps(), // Convert input to uppercase
                new InputFilter.LengthFilter(35) // Restrict maximum length
        };
        panNameET.setFilters(panNameFilter);
        edit_layout_pan.setEndIconOnClickListener(v -> {
            panET.setEnabled(true);
            InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInputFromWindow(edit_layout_pan.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
            panET.requestFocus();
            panET.setSelection(panET.getText().length());
        });
        binding.editLayoutPanName.setEndIconOnClickListener(v -> {
            panNameET.setEnabled(true);
            InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInputFromWindow(binding.editLayoutPanName.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
            panNameET.requestFocus();
            panNameET.setSelection(panNameET.getText().length());
        });
        //skip feature
        panLayout = findViewById(R.id.pan_layout);
        next = findViewById(R.id.pan_verify_next);
        prev = findViewById(R.id.pan_verify_prev);
        panET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Not yet implemented
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mPanNumber = String.valueOf(s);
                if (mPanNumber.length() == 10) {
                    hideKeyboard(PanVerificationActivity.this, panET);
                    panET.clearFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Not yet implemented
            }
        });
        panNameET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Not yet implemented
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mPanName = String.valueOf(s);
                if (mPanName.length() == 35) {
                    hideKeyboard(PanVerificationActivity.this, panNameET);
                    panNameET.clearFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Not yet implemented
            }
        });

        next.setOnClickListener(view -> {
            if (Boolean.TRUE.equals(connectionCheck(this))) {
                prev.setVisibility(View.GONE);
                mPanNumber = panET.getText().toString();
                //mPanDob = panDOBET.getText().toString();
                if (panImage.getDrawable() == null || binding.panTapHere.getVisibility() == View.VISIBLE) {
                    showErrorAlert(PanVerificationActivity.this, getString(R.string.sdk_error_pan_image));
                } else if (mPanNumber.trim().isEmpty() || !isValidPanCard(mPanNumber)) {
                    showErrorAlert(PanVerificationActivity.this, getString(R.string.sdk_error_pan_number));
                } else if (mPanName.trim().isEmpty()) {
                    showErrorAlert(PanVerificationActivity.this, getString(R.string.sdk_error_empty_pan_name));
                } else if (panNameET.getText().toString().length() < 5) {
                    showErrorAlert(this, getString(R.string.sdk_error_pan_name_length));
                } /*else if (mPanDob.equals("")) {
                    showErrorAlert(PanVerificationActivity.this, getString(R.string.please_enter_date_of_birth));
                } */ else {
                    showProgressDialog(this, getString(R.string.sdk_pan_verification_loader_message));
                    String namePerDashboard = NAME.toUpperCase();
                    String PanNameCheck = panNameET.getText().toString().toUpperCase();
                    Double nameMatchScore = Validate.diceCoefficientOptimized(namePerDashboard, PanNameCheck);
                    Log.e(TAG, "matchScore " + nameMatchScore);
                    if (nameMatchScore >= 0.7) {
                        nameMatchScore = nameMatchScore * 100;
                        ekycDataTable.setNameMatchScore(nameMatchScore.intValue());
                        userEkycViewModel.update(ekycDataTable);
                        validatePan(mPanNumber, sValidatePanUrl);
                    } else {
                        showErrorAlert(PanVerificationActivity.this,getString(R.string.sdk_error_pan_name));
                        hideProgressDialog();
                        userEkycViewModel.update(ekycDataTable);
                    }
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        ekycDataTable = new EkycDataTable();
        ekycDataTable = userEkycViewModel.getAllData();

        if (AwsSdkConstants.INSTANCE.getTOKEN_EXPIRED()){
            closeApplication(this, getString(R.string.session_expired));
            AwsSdkConstants.INSTANCE.setTOKEN_EXPIRED(false);
        }else {
            String imgType = AwsSdkConstants.INSTANCE.getIMAGE_TYPE();
            if (imgType.equals(StringConstants.PAN_CARD)) {
                String pan_name = AwsSdkConstants.INSTANCE.getPanDetails().getPanName();
                mPanDob = AwsSdkConstants.INSTANCE.getPanDetails().getPanDob();
                EkycSdkConstants.fathersName = AwsSdkConstants.INSTANCE.getPanDetails().getPanFatherName();
                String panNo = AwsSdkConstants.INSTANCE.getPanDetails().getPanNumber();
                String panImageUrl = AwsSdkConstants.INSTANCE.getPanDetails().getPanFirebaseUrl();

                Log.e(TAG, "pan url :" + panImageUrl);
                mPanName = pan_name;
                EkycSdkConstants.panName = pan_name;

                mPanNumber = panNo;
                String namePerDashboard = NAME.toUpperCase();
                String PanNameCheck = mPanName.toUpperCase();
                Double nameMatchScore = Validate.diceCoefficientOptimized(namePerDashboard, PanNameCheck);
                Log.e(TAG, "matchScore " + nameMatchScore);
                binding.panImage.setVisibility(View.VISIBLE);
                binding.panImageSelect.setVisibility(View.GONE);
                binding.panTapHere.setVisibility(View.GONE);
                Glide.with(this).load(panImageUrl).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        binding.panImage.setVisibility(View.GONE);
                        binding.panImageSelect.setVisibility(View.VISIBLE);
                        binding.panTapHere.setVisibility(View.VISIBLE);
                        panImage.setImageDrawable(null);
                        showToast(PanVerificationActivity.this, getString(R.string.sdk_error_recapture_image));
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                }).diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter().placeholder(R.drawable.progress_animation).into(panImage);
                showPan(mPanNumber, mPanName, mPanDob);
                ekycDataTable.setPanImage(panImageUrl);
                ekycDataTable.setPanNumber(mPanNumber);
                ekycDataTable.setPanName(mPanName);
                ekycDataTable.setPanFathersName(EkycSdkConstants.fathersName);
                userEkycViewModel.update(ekycDataTable);

            }
            AwsSdkConstants.INSTANCE.setIMAGE_TYPE("");
        }

    }

    /**
     * This api call is to validate pan number
     *
     * @param panNo pan number
     */
    private void validatePan(String panNo, String url) {
        showProgressDialog(PanVerificationActivity.this, getString(R.string.sdk_pan_verification_loader_message));
        PanRequestModel panRequest = new PanRequestModel();
        panRequest.setPanNumber(panNo);
        OnboardApiService onboardApiService = RetrofitInstance.retrofit().create(OnboardApiService.class);
        Call<PanResponseModel> validPan = onboardApiService.validatePan(panRequest, url);
        validPan.enqueue(new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<PanResponseModel> call, @NonNull Response<PanResponseModel> response) {
                hideProgressDialog();
                if (response.isSuccessful() && response.body() != null) {
                    int status = response.body().getStatus();
                    String message = response.body().getMessage();
                    if (status == 0) {
                        Data data = response.body().getData();
                        Data.PanInquiryResponse panInquiryResponse = data.getPanInquiryResponse();
                        String firstName = "";
                        String issueDate = "";
                        String lastName = "";
                        if (panInquiryResponse.getBody().getPanDetails().get(0).getFirstName() != null) {
                            firstName = panInquiryResponse.getBody().getPanDetails().get(0).getFirstName();
                        }
                        if (panInquiryResponse.getBody().getPanDetails().get(0).getLastName() != null) {
                            lastName = panInquiryResponse.getBody().getPanDetails().get(0).getLastName();
                        }
                        if (panInquiryResponse.getBody().getPanDetails().get(0).getLastUpdateDate() != null) {
                            issueDate = panInquiryResponse.getBody().getPanDetails().get(0).getLastUpdateDate();
                        }
                        if (panNameET.getText().toString().isEmpty()) {
                            if (panInquiryResponse.getBody().getPanDetails().get(0).getMiddleName() == null) {
                                mPanName = firstName + " " + lastName;
                            } else {
                                mPanName = firstName + " " + panInquiryResponse.getBody().getPanDetails().get(0).getMiddleName() + " " + lastName;
                            }
                            panNameET.setText(mPanName);
                            EkycSdkConstants.panName = mPanName;
                        } else {
                            mPanName = panNameET.getText().toString();
                            ekycDataTable.setSubStatus("3");
                            ekycDataTable.setPanNumber(mPanNumber);
                            ekycDataTable.setPanName(mPanName);
                            ekycDataTable.setDoi(issueDate);
                            ekycDataTable.setPanDateOfBirth(mPanDob);
                            userEkycViewModel.update(ekycDataTable);

                            Intent intent = OnboardingIntentFactory.returnAadhaarVerificationActivityIntent(PanVerificationActivity.this);
                            startActivity(intent);
                            finish();
                        }
                    } else {
                        showErrorAlert(PanVerificationActivity.this, message);
                    }
                } else {
                    panET.setText("");
                    showErrorAlert(PanVerificationActivity.this, getString(R.string.input_a_valid_pan_number_retake));
                }
            }

            @Override
            public void onFailure(@NonNull Call<PanResponseModel> call, @NonNull Throwable t) {
                hideProgressDialog();
                panET.setText("");
                showErrorAlert(PanVerificationActivity.this, getString(R.string.input_a_valid_pan_number_retake));
            }
        });
    }

    /**
     * Show PAN details layout after data extracted from card
     *
     * @param panNumber
     * @param panName
     */
    public void showPan(String panNumber, String panName, String dob) {
        TransitionManager.beginDelayedTransition(panLayout);
        panNameET.setText(panName);
        panET.setText(panNumber);
        //panDOBET.setText(dob);   //todo for date of birth
        binding.panNote.setVisibility(View.VISIBLE);
        binding.editLayoutPan.setVisibility(View.VISIBLE);
        binding.editLayoutPanName.setVisibility(View.VISIBLE);
        //binding.editLayoutPanDOB.setVisibility(View.VISIBLE); //todo for date of birth
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void datePicker() {
        // Get the current date
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        // Create a DatePickerDialog
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                PanVerificationActivity.this,
                (view, selectedYear, selectedMonth, selectedDay) -> {
                    // Handle the selected date and format it as "dd/MM/yyyy"
                    Calendar selectedCalendar = Calendar.getInstance();
                    selectedCalendar.set(selectedYear, selectedMonth, selectedDay);
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                    String formattedDate = sdf.format(selectedCalendar.getTime());
                    binding.panVerifyDOB.setText(formattedDate);
                }, year, month, day);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        // Show the date picker dialog
        datePickerDialog.show();
    }

}


//https://stackoverflow.com/questions/63293043/android-using-fileprovider-to-generate-uri-for-file