package com.isu.ekyc.posEkyc;

import static com.isu.ekyc.configuration.Validate.isValidGSTNo;
import static com.isu.ekyc.utils.CSCStringConstants.EXISTING;
import static com.isu.ekyc.utils.EkycSdkConstants.sFetchStateLocationUrl;
import static com.isu.ekyc.utils.EkycSdkConstants.sOnboardUrl;
import static com.isu.ekyc.utils.EkycSdkConstants.sPinValidateUrl;
import static com.isu.ekyc.utils.Utils.closeApplication;
import static com.isu.ekyc.utils.Utils.connectionCheck;
import static com.isu.ekyc.utils.Utils.exitTheProcess;
import static com.isu.ekyc.utils.Utils.handleErrorResponse;
import static com.isu.ekyc.utils.Utils.hideKeyboard;
import static com.isu.ekyc.utils.Utils.hideProgressDialog;
import static com.isu.ekyc.utils.Utils.showErrorAlert;
import static com.isu.ekyc.utils.Utils.showProgressDialog;
import static com.isu.ekyc.utils.Utils.showToast;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.exifinterface.media.ExifInterface;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.isu.ekyc.R;
import com.isu.ekyc.checkStatus.api.OnboardApiService;
import com.isu.ekyc.checkStatus.api.RetrofitInstance;
import com.isu.ekyc.checkStatus.model.fetchstatedistrict.DataItem;
import com.isu.ekyc.checkStatus.model.fetchstatedistrict.FetchStateDistrictRequestModel;
import com.isu.ekyc.checkStatus.model.fetchstatedistrict.FetchStateDistrictResponseModel;
import com.isu.ekyc.checkStatus.model.onboard.AadhaarDetails;
import com.isu.ekyc.checkStatus.model.onboard.BackAadhaarInfo;
import com.isu.ekyc.checkStatus.model.onboard.BasicInformation;
import com.isu.ekyc.checkStatus.model.onboard.DataObj;
import com.isu.ekyc.checkStatus.model.onboard.FrontAadhaarInfo;
import com.isu.ekyc.checkStatus.model.onboard.OnboardRequestModel;
import com.isu.ekyc.checkStatus.model.onboard.OnboardResponseModel;
import com.isu.ekyc.checkStatus.model.onboard.PanDetails;
import com.isu.ekyc.checkStatus.model.onboard.PanInfo;
import com.isu.ekyc.checkStatus.model.onboard.SelfieDetails;
import com.isu.ekyc.checkStatus.model.pincode.PinCodeRequestModel;
import com.isu.ekyc.checkStatus.model.pincode.PinCodeResponseModel;
import com.isu.ekyc.configuration.Validate;
import com.isu.ekyc.ekycDataBase.EkycDataTable;
import com.isu.ekyc.ekycDataBase.EkycViewModelFactory;
import com.isu.ekyc.ekycDataBase.UserEkycRepository;
import com.isu.ekyc.ekycDataBase.viewmodel.UserEkycViewModel;
import com.isu.ekyc.utils.EkycSdkConstants;
import com.isu.ekyc.utils.GpsTracker;
import com.isu.ekyc.utils.OnboardingIntentFactory;
import com.isu.ekyc.utils.SessionManager;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * <b>EkycBasicInfoActivity</b> here you need to enter mandatory fields,
 * upload shop image and onboards the user kyc.
 */
public class EkycBasicInfoActivity extends AppCompatActivity {

    private static final String TAG = EkycBasicInfoActivity.class.getSimpleName();
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    @RequiresApi(api = Build.VERSION_CODES.TIRAMISU)
    private static final String[] STORAGE_PERMISSIONS_33 = {Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_MEDIA_AUDIO, Manifest.permission.READ_MEDIA_VIDEO};
    private static final String[] STORAGE_PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    private EditText etName;
    private EditText etShop;
    private EditText etAddress;
    private EditText etState;
    private EditText etCity;
    private EditText etShopPin;
    private EditText etShopAddress;
    private EditText etPin;
    private EditText edtCode;
    private EditText etGstNumber;
    private EditText etShopCity;
    private String mName = "";
    private String mShop = "";
    private String mAddress = "";
    private String mState = "";
    private String mCity = "";
    private String mPin = "";
    private String mRefCode = "";
    private String mDOB = "";
    private String mMail = "";
    private String mPanNumber = "";
    private String mPhone = "";
    private String mAadhaarNumber = "";
    private String mUsername = "";
    private String mGender = "";
    private String mFatherName = "";
    private String mDateOfIssue = "";
    private String mGstNo = "";
    private String mAnnualIncome = "";
    private String mShopAddress = "";
    private String mSelectedStateName = "";
    private String mStateCode = "";
    private String mDistrictName = "";
    private String mShopPin = "";
    private String mShopCity = "";
    private String mShopCheckbox = "";
    private String mAdminName;
    private String mToken = "";
    private String mPanImageURL = "";
    private String mFrontAadhaarImageURL = "";
    private String mBackAadhaarImageURL = "";
    private String mSelfieImageURL = "";
    private String mGeoTaggedImageURL = "";
    private CheckBox ekyc_checkbox;
    private ImageView geotag_image, iv_place_holder_image;
    private ProgressBar geotag_progress;
    private Uri geoTaggedImageUri;
    private AppCompatSpinner annual_income;
    private AppCompatSpinner spinnerStateName;
    private AppCompatSpinner spinnerDistrict;
    private ArrayList<String> mIncomeType = new ArrayList<>();
    private ArrayList<String> mStateNameDetails = new ArrayList<>();
    private ArrayList<String> mStateCodeDetails = new ArrayList<>();
    private ArrayList<String> mDistrict = new ArrayList<>();
    private ArrayList<String> mDistrictCodeArray = new ArrayList<>();
    private String mDistrictCode = "";
    LocationManager locationManager;
    boolean mGotLocation = false;
    SessionManager session;
    Bitmap bitmapImage = null;
    private GpsTracker gpsTracker;
    private EkycDataTable ekycDataTable;
    double mLongitude = 0L;
    double mLatitude = 0L;
    Uri imageUri;
    private UserEkycViewModel userEkycViewModel;
    LocationListener mLocationListener = new LocationListener() {

        public void onLocationChanged(Location location) {

            if (validLatLng(location.getLatitude(), location.getLongitude())) {
                locationManager.removeUpdates(mLocationListener);
                mLongitude = location.getLongitude();
                mLatitude = location.getLatitude();

                geoTag(geoTaggedImageUri, mLatitude, mLongitude);

            }
        }

        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        public void onProviderEnabled(String s) {
        }

        public void onProviderDisabled(String s) {
        }

    };
    private String matchScore;
    private FirebaseStorage storage;
    private StorageReference storageReference;

    private static String queryName(ContentResolver resolver, Uri uri) {
        Cursor returnCursor = resolver.query(uri, null, null, null, null);
        assert returnCursor != null;
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        returnCursor.moveToFirst();
        String name = returnCursor.getString(nameIndex);
        returnCursor.close();
        return name;
    }

    public static String[] permissions() {
        String[] permissionList;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            permissionList = STORAGE_PERMISSIONS_33;
        } else {
            permissionList = STORAGE_PERMISSIONS;
        }
        return permissionList;
    }

    public static Bitmap compressImage(Bitmap bitmap, int newWidth, int newHeight) {
        Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);
        float scaleX = newWidth / (float) bitmap.getWidth();
        float scaleY = newHeight / (float) bitmap.getHeight();
        float pivotX = 0;
        float pivotY = 0;
        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(scaleX, scaleY, pivotX, pivotY);
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, 0, 0, new Paint(Paint.FILTER_BITMAP_FLAG));
        return scaledBitmap;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pos_ekycbasicinfo);
        init();
        //On back pressed handled
        OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                exitTheProcess(EkycBasicInfoActivity.this, getString(R.string.sdk_do_you_want_to_exit));
            }
        };
        // Add the callback to the onBackPressedDispatcher
        getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void init() {
        etShop = findViewById(R.id.ekyc_form_shop);
        etName = findViewById(R.id.ekyc_form_name);
        etAddress = findViewById(R.id.ekyc_form_address);
        etState = findViewById(R.id.ekyc_form_state);
        etCity = findViewById(R.id.ekyc_form_city);
        etPin = findViewById(R.id.ekyc_form_pin);
        edtCode = findViewById(R.id.edtCode);
        etGstNumber = findViewById(R.id.ekyc_form_GST_no);
        annual_income = findViewById(R.id.ekyc_form_Annual_income);
        spinnerStateName = findViewById(R.id.spinner_stateName);
        spinnerDistrict = findViewById(R.id.spinner_District);
        etShopPin = findViewById(R.id.ekyc_shop_pin);
        etShopAddress = findViewById(R.id.ekyc_shop_address);
        etShopCity = findViewById(R.id.ekyc_shop_city);
        ekyc_checkbox = findViewById(R.id.ekyc_checkbox);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        storage = FirebaseStorage.getInstance("gs://iserveu_storage");
        storageReference = storage.getReference();
        UserEkycRepository userEkycRepository = new UserEkycRepository(getApplication());
        userEkycViewModel = new ViewModelProvider(this, new EkycViewModelFactory(userEkycRepository)).get(UserEkycViewModel.class);
        ekycDataTable = new EkycDataTable();
        ekycDataTable = userEkycViewModel.getAllData();
        mIncomeType.add(getString(R.string.sdk_Annual_Income));
        mIncomeType.add(getString(R.string.greater_10_lakh));
        mIncomeType.add(getString(R.string.lesser_10_lakh));
        if (ActivityCompat.checkSelfPermission(EkycBasicInfoActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(EkycBasicInfoActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(EkycBasicInfoActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }
        InputFilter[] gstNumberFilter = new InputFilter[]{
                new InputFilter.AllCaps(), // Convert input to uppercase
                new InputFilter.LengthFilter(15) // Restrict maximum length
        };
        etGstNumber.setFilters(gstNumberFilter);
        etGstNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Not yet implemented
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String gstNumber = String.valueOf(s);
                if (gstNumber.length() == 15) {
                    hideKeyboard(EkycBasicInfoActivity.this, etGstNumber);
                    etGstNumber.clearFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Not yet implemented
            }
        });
        session = new SessionManager(getApplicationContext());

        HashMap<String, String> user = session.getUserDetails();
        mToken = user.get(SessionManager.KEY_TOKEN);
        mAdminName = EkycSdkConstants.ADMIN_NAME;
        mUsername = EkycSdkConstants.USER_NAME;

        geotag_image = findViewById(R.id.iv_geotagged_image);
        iv_place_holder_image = findViewById(R.id.iv_place_holder_image);

        geotag_progress = findViewById(R.id.geotag_progress);

        if (Boolean.TRUE.equals(connectionCheck(this))) {
            fetchStateDistrict(getString(R.string.fetch_location_state_key), getString(R.string.fetch_location_response_type), "");
        }
        setDataToSpinner();
        // Get data from database
        mName = ekycDataTable.getAadhaarName();
        mPhone = ekycDataTable.getPhone();
        matchScore = ekycDataTable.getSelfieMatchScore();
        mDOB = ekycDataTable.getAadhaarDateOfBirth();
        mPanImageURL = ekycDataTable.getPanImage();
        mFrontAadhaarImageURL = ekycDataTable.getAadhaarFrontImage();
        mBackAadhaarImageURL = ekycDataTable.getAadhaarBackImage();
        mSelfieImageURL = ekycDataTable.getSelfieImage();
        mDOB = ekycDataTable.getAadhaarDateOfBirth();
        mMail = ekycDataTable.getEmail();
        mAadhaarNumber = ekycDataTable.getAadhaarNumber();
        mPanNumber = ekycDataTable.getPanNumber();
        matchScore = ekycDataTable.getSelfieMatchScore();
        mGender = ekycDataTable.getGender();
        mDateOfIssue = ekycDataTable.getDoi();
        mFatherName = ekycDataTable.getPanFathersName();
        etName.setText(mName);
        if (ekycDataTable.getAadhaarCity() != null) {
            mCity = ekycDataTable.getAadhaarCity();
            etCity.setText(mCity);
        } else {
            etCity.setText("");
        }

        if (ekycDataTable.getAadhaarState() != null) {
            mState = ekycDataTable.getAadhaarState();
            etState.setText(mState);
        } else {
            etState.setText("");
        }
        if (ekycDataTable.getAadhaarPinCode() != null) {
            mPin = ekycDataTable.getAadhaarPinCode().trim();
            if (Boolean.TRUE.equals(connectionCheck(this)) && !mPin.isEmpty()) {
                fetchPinCode(Integer.parseInt(mPin));
            }
            etPin.setText(mPin);
        } else {
            etPin.setText("");
        }
        if (ekycDataTable.getAadhaarAddress() != null) {
            mAddress = ekycDataTable.getAadhaarAddress();
            etAddress.setText(mAddress);
        } else {
            etAddress.setText("");
        }
        if (ekycDataTable.getShopName() != null) {
            mShop = ekycDataTable.getShopName();
            etShop.setText(mShop);
        }

        if (ekycDataTable.getReferralCode() != null) {
            mRefCode = ekycDataTable.getReferralCode();
            edtCode.setText(mRefCode);
            edtCode.setEnabled(false);
        }


        etPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //Not yet implemented
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mPin = String.valueOf(charSequence);

                if (mPin.length() == 6) {
                    hideKeyboard(EkycBasicInfoActivity.this, etPin);
                    if (mPin.equalsIgnoreCase("000000")) {
                        showDismissPinAlert(getString(R.string.sdk_valid_pin));
                    } else {
                        int pincode = Integer.parseInt(mPin);
                        fetchPinCode(pincode);
                    }
                } else {
                    etCity.setText("");
                    etState.setText("");
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //Not yet implemented
            }
        });
        annual_income.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mAnnualIncome = annual_income.getSelectedItem().toString();
                if (position == 0) {
                    //Do Nothing
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //Not yet implemented

            }
        });
        spinnerStateName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSelectedStateName = spinnerStateName.getSelectedItem().toString();
                mStateCode = mStateCodeDetails.get(position);
                if (position != 0) {
                    fetchStateDistrict(getString(R.string.fetch_location_district_key), getString(R.string.fetch_location_response_type), mStateCode);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //Not yet implemented
            }
        });
        spinnerDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mDistrictName = spinnerDistrict.getSelectedItem().toString();
                mDistrictCode = mDistrictCodeArray.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //Not yet implemented
            }
        });

        ekyc_checkbox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                etShopAddress.setText(etAddress.getText().toString());
                etShopPin.setText(etPin.getText().toString());
                etShopCity.setText(etCity.getText().toString());
                mShopAddress = etShopAddress.getText().toString();
                mShopPin = etShopPin.getText().toString();
                mShopCity = etShopCity.getText().toString();
            } else {
                etShopAddress.setText("");
                etShopPin.setText("");
                etShopCity.setText("");
            }

        });

        iv_place_holder_image.setOnClickListener(v -> {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                boolean isAndroid13Permission = (ContextCompat.checkSelfPermission(EkycBasicInfoActivity.this, Manifest.permission.READ_MEDIA_IMAGES) == PackageManager.PERMISSION_GRANTED);
                if (!isAndroid13Permission) {
                    ActivityCompat.requestPermissions(EkycBasicInfoActivity.this, permissions(), 3);
                } else {
                    takeCameraImage();
                }
            } else {
                boolean isPermission = (ContextCompat.checkSelfPermission(EkycBasicInfoActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
                if (!isPermission) {
                    ActivityCompat.requestPermissions(EkycBasicInfoActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,}, 2);
                } else {
                    takeCameraImage();
                }
            }


        });

        findViewById(R.id.ekyc_form_next).setOnClickListener(view -> {
            hideKeyboard(EkycBasicInfoActivity.this, view);
            if (mAdminName.isEmpty() || mUsername.isEmpty()) {
                closeApplication(EkycBasicInfoActivity.this,getString(R.string.we_are_unable_to_fetch_your_data_please_close_your_app_and_open_again));
            }
            if (Boolean.TRUE.equals(connectionCheck(this))) {
                mName = etName.getText().toString();
                mAddress = etAddress.getText().toString().trim();
                mPin = etPin.getText().toString();
                mShop = etShop.getText().toString().trim();
                mGstNo = etGstNumber.getText().toString().trim();
                mShopAddress = etShopAddress.getText().toString().trim();
                mShopPin = etShopPin.getText().toString();
                mShopCheckbox = ekyc_checkbox.getText().toString();
                mCity = etCity.getText().toString().trim();
                mState = etState.getText().toString().trim();
                mShopCity = etShopCity.getText().toString().trim();

                if (bitmapImage == null) {
                    showErrorAlert(EkycBasicInfoActivity.this, getString(R.string.sdk_basic_shop_image));
                } else if (mShop.isEmpty()) {
                    showErrorAlert(EkycBasicInfoActivity.this, getString(R.string.sdk_basic_shop_name));
                } else if (!Validate.isValidName(mName)) {
                    showErrorAlert(EkycBasicInfoActivity.this, getString(R.string.sdk_basic_name));
                } else if (mShopAddress.isEmpty()) {
                    showErrorAlert(EkycBasicInfoActivity.this, getString(R.string.sdk_basic_shop_address));
                } else if (mShopCity.isEmpty()) {
                    showErrorAlert(EkycBasicInfoActivity.this, getString(R.string.sdk_basic_city));
                } else if (mShopPin.isEmpty()) {
                    showErrorAlert(EkycBasicInfoActivity.this, getString(R.string.sdk_basic_pinCode));
                } else if (mShopPin.length() < 6) {
                    showErrorAlert(EkycBasicInfoActivity.this, getString(R.string.sdk_basic_shop_pinCode));
                } else if (mSelectedStateName.equalsIgnoreCase(getString(R.string.select_state))) {
                    showErrorAlert(EkycBasicInfoActivity.this, getString(R.string.please_select_state));
                } else if (mDistrictName.equalsIgnoreCase(getString(R.string.select_district))) {
                    showErrorAlert(EkycBasicInfoActivity.this, getString(R.string.please_select_district));
                } else if (!mGstNo.isEmpty() && !isValidGSTNo(mGstNo)) {
                    showErrorAlert(EkycBasicInfoActivity.this, getString(R.string.enter_a_valid_gstno));
                } /*else if (mAnnualIncome.contains(getString(R.string.annual_income))) { // May be in later annual income validation will be added
                    showErrorAlert(EkycBasicInfoActivity.this, getString(R.string.enter_annual_income));
                }*/ else if (mAddress.isEmpty()) {
                    showErrorAlert(EkycBasicInfoActivity.this, getString(R.string.sdk_basic_address));
                } else if (mPin.length() != 6) {
                    showErrorAlert(EkycBasicInfoActivity.this, getString(R.string.sdk_basic_pinCode));
                } else if (mCity.isEmpty()) {
                    showErrorAlert(EkycBasicInfoActivity.this, getString(R.string.sdk_basic_pinCode_of_city));
                } else if (mState.isEmpty()) {
                    showErrorAlert(EkycBasicInfoActivity.this, getString(R.string.sdk_basic_pinCode_of_state));
                } else {
                    mState = etState.getText().toString();
                    mCity = etCity.getText().toString();
                    mRefCode = edtCode.getText().toString();

                    ekycDataTable.setEmail(mMail);
                    ekycDataTable.setAadhaarCity(mCity);
                    ekycDataTable.setAadhaarState(mState);
                    ekycDataTable.setAadhaarAddress(mAddress);
                    ekycDataTable.setPhone(mPhone);
                    ekycDataTable.setAadhaarPinCode(mPin);
                    ekycDataTable.setAadhaarName(mName);
                    ekycDataTable.setAadhaarDateOfBirth(mDOB);
                    ekycDataTable.setUsername(mUsername);
                    ekycDataTable.setReferralCode(mRefCode);
                    ekycDataTable.setShopName(mShop);
                    ekycDataTable.setGstNumber(mGstNo);
                    ekycDataTable.setIncome(mAnnualIncome);
                    ekycDataTable.setSubStatus("5");
                    ekycDataTable.setShopPin(mShopPin);
                    ekycDataTable.setShopAddress(mShopAddress);
                    userEkycViewModel.update(ekycDataTable);

                    uploadFileInFirebase(geoTaggedImageUri, "Shop", "GeoTaggedShop");
                }
            }
        });
    }

    public boolean validLatLng(double lat, double lng) {
        if (lat != 0.0 && lng != 0.0) {
            this.mGotLocation = true;
            return true;
        } else return false;
    }

    private void takeCameraImage() {
        ContentValues values = new ContentValues();
        imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        mStartForResult.launch(intent);
    }
    ActivityResultLauncher<Intent> mStartForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    try {
                        Bitmap thumbnail = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                        geoTaggedImageUri = imageUri;
                        bitmapImage = thumbnail;
                        handleUCropResult();
                    } catch (Exception e) {
                        showToast(EkycBasicInfoActivity.this, getString(R.string.sdk_try_again));
                    }
                } else {
                    setResultCancelled();
                }
            });

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case UCrop.REQUEST_CROP:
                if (resultCode == RESULT_OK) {
                    handleUCropResult();
                } else {
                    setResultCancelled();
                }
                break;
            case UCrop.RESULT_ERROR:
                final Throwable cropError = UCrop.getError(data);
                Log.e(TAG, "Crop error: " + cropError);
                setResultCancelled();
                break;
            default:
                setResultCancelled();
        }


    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void handleUCropResult() {
        geotag_progress.setVisibility(View.VISIBLE);
        iv_place_holder_image.setVisibility(View.GONE);
        gpsTracker = new GpsTracker(EkycBasicInfoActivity.this);
        if (gpsTracker.canGetLocation()) {
            mLatitude = gpsTracker.getLatitude();
            mLongitude = gpsTracker.getLongitude();
            geoTag(geoTaggedImageUri, mLatitude, mLongitude);
        } else {
            gpsTracker.showSettingsAlert();
            iv_place_holder_image.setVisibility(View.VISIBLE);
            geotag_progress.setVisibility(View.GONE);
        }

    }

    private void setResultCancelled() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_CANCELED, intent);
    }


    public static File getOutputDirectory(Context context) {
        File mediaDir = null;
        if (context.getExternalMediaDirs().length > 0) {
            mediaDir = new File(context.getExternalMediaDirs()[0], context.getString(R.string.app_name));
            mediaDir.mkdir();
        }

        if (mediaDir != null && mediaDir.exists()) {
            return mediaDir;
        } else {
            return context.getFilesDir();
        }
    }

    private void geoTag(Uri imageUri, double latitude, double longitude) {
        try {
            File outputDir = getOutputDirectory(this);

            // Use a unique file name for the saved image
            String fileName = "SHOP_IMAGE_" + System.currentTimeMillis() + ".jpg"; // Adjust the extension as needed
            File outputFile = new File(outputDir, fileName);

            // Open an input stream for the imageUri
            InputStream inputStream = getContentResolver().openInputStream(imageUri);
            if (inputStream != null) {
                // Create a FileOutputStream to write the image to the outputFile
                FileOutputStream outputStream = new FileOutputStream(outputFile);

                // Read from the input stream and write to the output stream

                byte[] buffer = new byte[4096];
                int bytesRead;
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                // Close the streams
                inputStream.close();
                outputStream.close();

                // Now, you can work with the outputFile
                String filePath = outputFile.getAbsolutePath();
                androidx.exifinterface.media.ExifInterface exif = new androidx.exifinterface.media.ExifInterface(filePath);

                // Perform your Exif operations here
                exif.setAttribute(androidx.exifinterface.media.ExifInterface.TAG_GPS_LATITUDE, GPS.convert(latitude));
                exif.setAttribute(androidx.exifinterface.media.ExifInterface.TAG_GPS_LATITUDE_REF, GPS.latitudeRef(latitude));
                exif.setAttribute(androidx.exifinterface.media.ExifInterface.TAG_GPS_LONGITUDE, GPS.convert(longitude));
                exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, GPS.longitudeRef(longitude));
                exif.saveAttributes();

                // Load and manipulate the image
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inScaled = false;
                options.inDither = false;
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;

                Bitmap bitmapImage = BitmapFactory.decodeFile(filePath);

                Matrix rotationMatrix = new Matrix();
                if (bitmapImage.getWidth() >= bitmapImage.getHeight()) {
                    rotationMatrix.setRotate(90);
                } else {
                    rotationMatrix.setRotate(0);
                }

                Bitmap rotatedBitmap = Bitmap.createBitmap(bitmapImage, 0, 0, bitmapImage.getWidth(), bitmapImage.getHeight(), rotationMatrix, true);

                // Update UI with the manipulated image
                geotag_image.setVisibility(View.VISIBLE);
                iv_place_holder_image.setAlpha(0);
                iv_place_holder_image.setVisibility(View.VISIBLE);
                geotag_progress.setVisibility(View.GONE);
                geotag_image.setImageBitmap(rotatedBitmap);
                geotag_progress.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            Log.e("PictureActivity", e.getLocalizedMessage());
            geotag_progress.setVisibility(View.GONE);
            iv_place_holder_image.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(EkycBasicInfoActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                    try {
                        String provider = LocationManager.NETWORK_PROVIDER;

                        locationManager.requestLocationUpdates(provider, 1000, 0.0f, mLocationListener);

                    } catch (Exception e) {
                        Log.e("Exception______", e.getMessage());
                    }
                }
            } else {
                showToast(this, getString(R.string.sdk_permission_denied));
            }
        }
    }

    private void setDataToSpinner() {

        if (mIncomeType != null) {

            final ArrayAdapter<String> martialAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, mIncomeType) {
                @Override
                public boolean isEnabled(int position) {
                    return position != 0;
                }

                @Override
                public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    if (position == 0) {
                        // Set the disable item text color
                        tv.setTextColor(Color.GRAY);
                    } else {
                        tv.setTextColor(Color.BLACK);
                    }
                    return view;
                }

                @Override
                public int getCount() {
                    return mIncomeType != null ? mIncomeType.size() : 0;
                }

                @Override
                public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View rowView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
                    TextView item = rowView.findViewById(android.R.id.text1);
                    item.setText(mIncomeType.get(position));
                    if (position == 0) {
                        // 'SELECT' to be showed in gray colour
                        item.setTextColor(Color.GRAY);
                    } else {
                        item.setTextColor(Color.BLACK);
                    }
                    return rowView;
                }
            };
            annual_income.setAdapter(martialAdapter);
        }
    }

    private void setStateNameToSpinner() {

        if (mStateNameDetails != null) {
            ArrayAdapter<String> martialAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, mStateNameDetails) {
                @Override
                public boolean isEnabled(int position) {
                    return position != 0;
                }

                @Override
                public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    if (position == 0) {
                        // Set the disable item text color
                        tv.setTextColor(Color.GRAY);
                    } else {
                        tv.setTextColor(Color.BLACK);
                    }
                    return view;
                }

                @Override
                public int getCount() {
                    return mStateNameDetails != null ? mStateNameDetails.size() : 0;
                }

                @Override
                public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View rowView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
                    TextView item = rowView.findViewById(android.R.id.text1);
                    item.setText(mStateNameDetails.get(position));
                    if (position == 0) {
                        // 'SELECT' to be showed in gray colour
                        item.setTextColor(Color.GRAY);
                    } else {
                        item.setTextColor(Color.BLACK);
                    }
                    return rowView;
                }
            };
            spinnerStateName.setAdapter(martialAdapter);
        }
    }

    private void setDistrictToSpinner() {
        if (mDistrict != null) {
            ArrayAdapter<String> martialAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, mDistrict) {
                @Override
                public boolean isEnabled(int position) {
                    return position != 0;
                }

                @Override
                public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    if (position == 0) {
                        // Set the disable item text color
                        tv.setTextColor(Color.GRAY);
                    } else {
                        tv.setTextColor(Color.BLACK);
                    }
                    return view;
                }

                @Override
                public int getCount() {
                    return mDistrict != null ? mDistrict.size() : 0;
                }

                @Override
                public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View rowView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
                    TextView item = rowView.findViewById(android.R.id.text1);
                    item.setText(mDistrict.get(position));
                    if (position == 0) {
                        // 'SELECT' to be showed in gray colour
                        item.setTextColor(Color.GRAY);
                    } else {
                        item.setTextColor(Color.BLACK);
                    }
                    return rowView;
                }
            };
            spinnerDistrict.setAdapter(martialAdapter);
        }
    }

    /**
     * Upload files in firebase
     *
     * @param urlFilePath file path
     * @param image       image to be upload
     * @param filename    file to be upload in particular name
     */
    private void uploadFileInFirebase(Uri urlFilePath, String image, String filename) {
        ProgressDialog progressD = new ProgressDialog(EkycBasicInfoActivity.this);
        progressD.setTitle(image + " Image Uploading...");
        progressD.setCancelable(false);
        Log.e(TAG, image + "URI : " + urlFilePath);
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance("gs://iserveu_storage");
        StorageReference reference = firebaseStorage.getReference();
        String fileName = "";
        StorageReference filepath;

        fileName = EkycSdkConstants.USER_NAME + "_" + EkycSdkConstants.USER_MOBILE_NO + "_" + filename + ".png";
        filepath = reference.child("KYC_NEW/" + EkycSdkConstants.ADMIN_NAME + "/" + EkycSdkConstants.USER_NAME + "/" + fileName);

        UploadTask uploadTask = filepath.putFile(urlFilePath);
        progressD.show();
        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                int progress = (int) ((100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount());
                progressD.setMessage("Uploaded " + progress + "%");
                Log.d(TAG, "Upload is " + progress + "% done");
            }
        }).addOnPausedListener(new OnPausedListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onPaused(UploadTask.TaskSnapshot taskSnapshot) {
                Log.d(TAG, "Upload is paused");
            }
        });

        filepath.putFile(urlFilePath).continueWithTask(new Continuation() {
            @Override
            public Object then(Task task) throws Exception {
                if (task.isSuccessful()) {
                    return filepath.getDownloadUrl();
                }
                throw Objects.requireNonNull(task.getException());
            }
        }).addOnCompleteListener((OnCompleteListener<Uri>) task -> {
            if (task.isSuccessful()) {
                progressD.dismiss();
                Uri uri = task.getResult();
                String getUrl = uri.toString();
                mGeoTaggedImageURL = getUrl;
                onboardApiCall(mUsername, mPhone, mMail, mPanNumber, mPanImageURL,
                        mDateOfIssue, mDOB, mFatherName, mName, mAadhaarNumber, mFrontAadhaarImageURL, mBackAadhaarImageURL,
                        mPin, mGender, mSelfieImageURL, matchScore, mShop, mCity, mAddress,
                        mGstNo, mAnnualIncome, mShopAddress, mShopPin, mGeoTaggedImageURL, String.valueOf(mLatitude),
                        String.valueOf(mLongitude), mState, mSelectedStateName, mStateCode, mDistrictName, mDistrictCode
                );
            } else {
                progressD.dismiss();
                showToast(this, "Uploaded Failed : " + image);
            }
        });

    }

    /**
     * show alert for invalid pin code
     *
     * @param statusDesc
     */
    public void showDismissPinAlert(String statusDesc) {
        androidx.appcompat.app.AlertDialog.Builder alertBuilder = new androidx.appcompat.app.AlertDialog.Builder(EkycBasicInfoActivity.this);
        alertBuilder.setTitle(getString(R.string.sdk_alert));
        alertBuilder.setMessage(statusDesc);
        alertBuilder.setCancelable(true);
        alertBuilder.setPositiveButton(getString(R.string.sdk_ok_btn), (dialog, id) -> {
            etPin.setText("");
            dialog.dismiss();
        });
        AlertDialog alertDialog = alertBuilder.create();
        alertDialog.show();
    }

    /**
     * This method is for validate pin code and fetch city and state
     *
     * @param pinCode
     */
    private void fetchPinCode(int pinCode) {
        showProgressDialog(EkycBasicInfoActivity.this, getString(R.string.sdk_loading));
        PinCodeRequestModel pinCodeRequest = new PinCodeRequestModel();
        pinCodeRequest.setPin(pinCode);
        OnboardApiService onboardApiService = RetrofitInstance.retrofit().create(OnboardApiService.class);
        Call<PinCodeResponseModel> fetchPinCode = onboardApiService.pinCode(pinCodeRequest, sPinValidateUrl);
        fetchPinCode.enqueue(new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<PinCodeResponseModel> call, @NonNull Response<PinCodeResponseModel> response) {
                hideProgressDialog();
                if (response.isSuccessful() && response.body() != null) {
                    int statusCode = response.body().getStatusCode();
                    String statusDesc = response.body().getData().getStatusDesc();
                    if (statusCode == 0) {
                        String status = response.body().getData().getStatus();
                        if (status.equalsIgnoreCase("Success")) {
                            mState = response.body().getData().getData().getStateName();
                            mCity = response.body().getData().getData().getCity();
                            etState.setText(mState);
                            etCity.setText(mCity);
                        } else {
                            showToast(EkycBasicInfoActivity.this, statusDesc);
                        }
                    } else {
                        showToast(EkycBasicInfoActivity.this, statusDesc);
                    }
                } else {
                    showToast(EkycBasicInfoActivity.this, getString(R.string.pincode_not_found));
                }
            }

            @Override
            public void onFailure(@NonNull Call<PinCodeResponseModel> call, @NonNull Throwable t) {
                hideProgressDialog();
                showToast(EkycBasicInfoActivity.this, getString(R.string.went_wrong));
            }
        });
    }

    /**
     * Set state names in spinner. When user select state then fetch district name.
     *
     * @param locationType
     * @param responseType
     * @param stateCode
     */
    private void fetchStateDistrict(String locationType, String responseType, String stateCode) {
        showProgressDialog(this, getString(R.string.sdk_please_wait));
        FetchStateDistrictRequestModel fetchStateDistrictRequestModel = new FetchStateDistrictRequestModel();
        fetchStateDistrictRequestModel.setLocationType(locationType);
        fetchStateDistrictRequestModel.setResponseType(responseType);
        fetchStateDistrictRequestModel.setStateCode(stateCode);
        fetchStateDistrictRequestModel.setDistrictCode("");
        fetchStateDistrictRequestModel.setSubdistrictCode("");
        fetchStateDistrictRequestModel.setVillageCode("");


        OnboardApiService onboardApiService = RetrofitInstance.retrofit().create(OnboardApiService.class);
        Call<FetchStateDistrictResponseModel> fetchLocation = onboardApiService.getStateDistrict(fetchStateDistrictRequestModel, sFetchStateLocationUrl);
        fetchLocation.enqueue(new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<FetchStateDistrictResponseModel> call, @NonNull Response<FetchStateDistrictResponseModel> response) {
                hideProgressDialog();
                if (response.isSuccessful() && response.body() != null) {
                    int status = response.body().getStatus();
                    String message = response.body().getMessage();
                    if (status == 1) {
                        mDistrictCodeArray.clear();
                        mDistrict.clear();
                        mDistrict.add(getString(R.string.select_district));
                        mDistrictCodeArray.add("");
                        if (locationType.equals(getString(R.string.fetch_location_state_key))) {
                            mStateNameDetails.clear();
                            mStateCodeDetails.clear();
                            ArrayList<DataItem> stateData = response.body().getData();
                            if (stateData != null && stateData.size() > 0) {
                                mStateNameDetails.add(getString(R.string.select_state));
                                mStateCodeDetails.add("");
                                for (int i = 0; i < stateData.size(); i++) {
                                    mStateNameDetails.add(stateData.get(i).getName());
                                    mStateCodeDetails.add(stateData.get(i).getStateCode());
                                }
                                setStateNameToSpinner();
                                setDistrictToSpinner();
                            }
                        } else if (locationType.equals(getString(R.string.fetch_location_district_key))) {
                            ArrayList<DataItem> stateData = response.body().getData();
                            if (stateData != null && stateData.size() > 0) {
                                for (int i = 0; i < stateData.size(); i++) {
                                    mDistrict.add(stateData.get(i).getName());
                                    mDistrictCodeArray.add(stateData.get(i).getDistCode());
                                }
                                setDistrictToSpinner();
                            }
                        } else {
                            showToast(EkycBasicInfoActivity.this, message);
                        }
                    } else {
                        showToast(EkycBasicInfoActivity.this, message);
                    }
                } else {
                    showToast(EkycBasicInfoActivity.this, getString(R.string.sdk_please_try_again));
                }
            }

            @Override
            public void onFailure(@NonNull Call<FetchStateDistrictResponseModel> call, @NonNull Throwable t) {
                hideProgressDialog();
                showToast(EkycBasicInfoActivity.this, t.getLocalizedMessage());
            }
        });
    }

    private void onboardApiCall(String username, String mobile, String email, String panNumber, String panImage, String dateOfIssue,
                                String dateOfBirth, String fatherName, String name, String aadhaarNumber, String frontImage, String backImage,
                                String aadhaarPinCode, String gender, String selfieImage, String faceMatchScore,
                                String shopName, String city, String address, String gstNumber, String income, String shopAddress, String shopPinCode,
                                String shopImage, String latitude, String longitude, String state, String selectedState,
                                String stateCode, String district, String districtCode) {
        showProgressDialog(this, getString(R.string.sdk_please_wait));

        PanInfo panInfo = new PanInfo(); // pan info object details
        panInfo.setPan(panNumber);
        panInfo.setDob(dateOfBirth);
        panInfo.setDateOfIssue(dateOfIssue);
        panInfo.setFather(fatherName);
        panInfo.setName(name);

        PanDetails panDetails = new PanDetails(); // pan details object for onboard and update details
        panDetails.setPan(panNumber);
        panDetails.setPanImage(panImage);
        panDetails.setPanInfo(panInfo);

        FrontAadhaarInfo frontAadhaarInfo = new FrontAadhaarInfo(); // aadhaar front info object details
        frontAadhaarInfo.setMother("");
        frontAadhaarInfo.setPin(aadhaarPinCode);
        frontAadhaarInfo.setGender(gender);
        frontAadhaarInfo.setDob(dateOfBirth);
        frontAadhaarInfo.setFather(fatherName);
        frontAadhaarInfo.setAadhar(aadhaarNumber);
        frontAadhaarInfo.setName(name);
        frontAadhaarInfo.setHusband("");
        frontAadhaarInfo.setYob("");

        BackAadhaarInfo backAadhaarInfo = new BackAadhaarInfo(); // aadhaar back info object details
        backAadhaarInfo.setMother("");
        backAadhaarInfo.setPin(aadhaarPinCode);
        backAadhaarInfo.setGender(gender);
        backAadhaarInfo.setDob(dateOfBirth);
        backAadhaarInfo.setFather(fatherName);
        backAadhaarInfo.setAadhar(aadhaarNumber);
        backAadhaarInfo.setName(name);
        backAadhaarInfo.setHusband("");
        backAadhaarInfo.setYob("");

        AadhaarDetails aadhaarDetails = new AadhaarDetails(); // aadhaar details object for onboard and update details
        aadhaarDetails.setAadhar(aadhaarNumber);
        aadhaarDetails.setAadhaarFrontImage(frontImage);
        aadhaarDetails.setAadhaarBackImage(backImage);
        aadhaarDetails.setFrontAadharInfo(frontAadhaarInfo);
        aadhaarDetails.setBackAadharInfo(backAadhaarInfo);

        SelfieDetails selfieDetails = new SelfieDetails(); // selfie details object for onboard and update details
        selfieDetails.setMatchScore(faceMatchScore);
        selfieDetails.setSelfieUrl(selfieImage);

        BasicInformation basicInformation = new BasicInformation(); // basic information details object for onboard details
        basicInformation.setName(name);
        basicInformation.setPincode(aadhaarPinCode);
        basicInformation.setShopName(shopName);
        basicInformation.setCity(city);
        basicInformation.setAddress(address);
        basicInformation.setOfac("");
        basicInformation.setGstNo(gstNumber);
        basicInformation.setIncome(income);
        basicInformation.setShopAddress(shopAddress);
        basicInformation.setShopPin(shopPinCode);
        basicInformation.setShopImage(shopImage);
        basicInformation.setLatitude(latitude);
        basicInformation.setLongitude(longitude);
        basicInformation.setShopType("");
        basicInformation.setMerchantType("");
        basicInformation.setState(selectedState);
        basicInformation.setStateCode(stateCode);
        basicInformation.setDistrict(district);
        basicInformation.setDistrictCode(districtCode);

        DataObj dataObj = new DataObj(); // data object details
        dataObj.setPhone(mobile);
        dataObj.setEmail(email);
        dataObj.setPanDetails(panDetails);
        dataObj.setAadhaarDetails(aadhaarDetails);
        dataObj.setSelfieDeatils(selfieDetails);
        dataObj.setBasicInformation(basicInformation);

        OnboardRequestModel onboardRequestModel = new OnboardRequestModel(); // Onboard details object
        onboardRequestModel.setUsername(username);
        onboardRequestModel.setType(EXISTING);
        onboardRequestModel.setDataObj(dataObj);


        OnboardApiService onboardApiService = RetrofitInstance.retrofit().create(OnboardApiService.class);
        Call<OnboardResponseModel> onboard = onboardApiService.onboard(onboardRequestModel, mToken, sOnboardUrl);
        onboard.enqueue(new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<OnboardResponseModel> call, @NonNull Response<OnboardResponseModel> response) {
                hideProgressDialog();
                if (response.isSuccessful() && response.body() != null) {
                    int status = response.body().getStatus();
                    String message = response.body().getStatusDesc();
                    if (status == 0) {
                        Intent intent = OnboardingIntentFactory.returnCongratulationActivityIntent(EkycBasicInfoActivity.this);
                        startActivity(intent);
                        finish();
                    } else {
                        showToast(EkycBasicInfoActivity.this, message);
                    }
                } else {
                    handleErrorResponse(EkycBasicInfoActivity.this, response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<OnboardResponseModel> call, @NonNull Throwable t) {
                hideProgressDialog();
                showToast(EkycBasicInfoActivity.this, getString(R.string.sdk_please_try_again));
            }
        });
    }

}
