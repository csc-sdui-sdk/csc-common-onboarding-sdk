package com.isu.ekyc.posEkyc;

import static com.isu.ekyc.utils.Utils.closeApplication;
import static com.isu.ekyc.utils.Utils.connectionCheck;
import static com.isu.ekyc.utils.Utils.exitTheProcess;
import static com.isu.ekyc.utils.Utils.hideKeyboard;
import static com.isu.ekyc.utils.Utils.intentToSDK;
import static com.isu.ekyc.utils.Utils.showErrorAlert;
import static com.isu.ekyc.utils.Utils.showToast;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.isu.awssdk.core.AwsSdkConstants;
import com.isu.awssdk.core.StringConstants;
import com.isu.ekyc.R;
import com.isu.ekyc.configuration.Validate;
import com.isu.ekyc.ekycDataBase.EkycDataTable;
import com.isu.ekyc.ekycDataBase.EkycViewModelFactory;
import com.isu.ekyc.ekycDataBase.UserEkycRepository;
import com.isu.ekyc.ekycDataBase.viewmodel.UserEkycViewModel;
import com.isu.ekyc.masked.viewmodel.MaskViewModel;
import com.isu.ekyc.otpverification.PhoneNumberActivity;
import com.isu.ekyc.utils.EkycSdkConstants;
import com.isu.ekyc.utils.OnboardingIntentFactory;
import com.isu.ekyc.utils.SessionManager;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

/**
 * <b>AadhaarVerificationActivity</b> , here you have to upload by capturing aadhaar front and aadhaar back document.
 * Extracted data are editable, name extracted from aadhaar should match with name extracted from pan
 * after validating basic details you will proceed for face match with aadhaar image
 * If face matches with aadhaar then this activities purpose fulfills.
 */
public class AadhaarVerificationActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = AadhaarVerificationActivity.class.getSimpleName();
    private String mMatchScore = "";
    private String mToken = "";
    private LinearLayout aadhaarLayout;
    private TextView panCheckLayout;
    private TextView frontTapHere;
    private TextView backTapHere;
    private TextView selfieTapHere;
    private ImageView ivFront;
    private ImageView ivBack;
    private ImageView ivSelfie;
    private TextInputEditText edtAadhaar;
    private TextInputEditText edtAadhaarName;
    private TextInputEditText edtAadhaarDob;
    private TextInputLayout editLayoutAadhaar;
    private TextInputLayout editLayoutAadhaarName;
    private TextInputLayout editLayoutAadhaarDob;
    private TextView ntAADHAAR;
    private TextView tvSelfie;
    private ImageView ivFrontPlaceHolder;
    private ImageView ivSelfiePlaceHolder;
    private ImageView ivBackPlaceHolder;
    private FrameLayout placeHolderFront;
    private Button btnSubmit;
    private String mDateOfBirth = "";
    private String mAadhaarName = "";
    private String mAadhaarNumber = "";
    private RelativeLayout rlSelfie;
    private int mYear;
    private int mMonth;
    private int mDay;
    private EkycDataTable ekycDataTable;
    private UserEkycViewModel userEkycViewModel;
    private String mSelectedImage = "";
    private ProgressBar ivImgProgress, ivBackImgProgress;
    private MaskViewModel maskViewModel;
    private SessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aadhaar_verification);
        initializeUIControl();
        edtAadhaarName.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        //On back pressed handled
        OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                exitTheProcess(AadhaarVerificationActivity.this, getString(R.string.sdk_do_you_want_to_exit));
            }
        };
        // Add the callback to the onBackPressedDispatcher
        getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback);
    }

    /**
     * init ui components
     */
    private void initializeUIControl() {
        session = new SessionManager(this);
        HashMap<String, String> user = session.getUserDetails();
        mToken = user.get(SessionManager.KEY_TOKEN);
        maskViewModel = new MaskViewModel();
        UserEkycRepository userEkycRepository = new UserEkycRepository(getApplication());
        userEkycViewModel = new ViewModelProvider(this, new EkycViewModelFactory(userEkycRepository)).get(UserEkycViewModel.class);
        ekycDataTable = new EkycDataTable();
        ekycDataTable = userEkycViewModel.getAllData();

        aadhaarLayout = findViewById(R.id.aadhaar_layout);
        edtAadhaar = findViewById(R.id.edt_aadhaar);
        edtAadhaarName = findViewById(R.id.edt_aadhaar_name);
        edtAadhaarDob = findViewById(R.id.edt_aadhaar_dob);
        ntAADHAAR = findViewById(R.id.aadhaar_note);
        editLayoutAadhaar = findViewById(R.id.edit_layout_aadhaar);
        ivImgProgress = findViewById(R.id.iv_img_progress);
        ivBackImgProgress = findViewById(R.id.iv_img_progressback);
        editLayoutAadhaar.setEndIconOnClickListener(v -> {
            edtAadhaar.setEnabled(true);
            InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInputFromWindow(editLayoutAadhaar.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
            edtAadhaar.requestFocus();
            edtAadhaar.setSelection(Objects.requireNonNull(edtAadhaar.getText()).length());
        });
        editLayoutAadhaarName = findViewById(R.id.edit_layout_aadhaar_name);
        editLayoutAadhaarName.setEndIconOnClickListener(v -> {
            edtAadhaarName.setEnabled(true);
            InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInputFromWindow(editLayoutAadhaarName.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
            edtAadhaarName.requestFocus();
            edtAadhaarName.setSelection(Objects.requireNonNull(edtAadhaarName.getText()).length());
        });
        edtAadhaar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAadhaarNumber = String.valueOf(s);
                if (mAadhaarNumber.length() == 12) {
                    hideKeyboard(AadhaarVerificationActivity.this, edtAadhaar);
                    edtAadhaar.clearFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtAadhaarName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAadhaarName = String.valueOf(s);
                if (mAadhaarName.length() == 35) {
                    hideKeyboard(AadhaarVerificationActivity.this, edtAadhaarName);
                    edtAadhaarName.clearFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editLayoutAadhaarDob = findViewById(R.id.edit_layout_aadhaar_dob);
        edtAadhaarDob.setOnClickListener(v -> {   //todo aadhaar dob
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                datePicker();
            }
        });
        ivFront = findViewById(R.id.iv_aadhar_front);
        ivFront.setOnClickListener(this);
        ivBack = findViewById(R.id.iv_aadhar_back);
        ivSelfie = findViewById(R.id.iv_sefie);
        ivBack.setOnClickListener(this);
        ivSelfie.setOnClickListener(this);
        ivFrontPlaceHolder = findViewById(R.id.iv_place_holder_front);
        placeHolderFront = findViewById(R.id.place_holder_front);
        ivFrontPlaceHolder.setOnClickListener(this);
        ivBackPlaceHolder = findViewById(R.id.iv_place_holder_back);
        ivBackPlaceHolder.setOnClickListener(this);
        btnSubmit = findViewById(R.id.btn_submit);
        btnSubmit.setOnClickListener(this);
        rlSelfie = findViewById(R.id.rl_selfie);
        ivSelfiePlaceHolder = findViewById(R.id.iv_selfie_holder);
        ivSelfiePlaceHolder.setOnClickListener(this);
        tvSelfie = findViewById(R.id.tv_selfie);
        frontTapHere = findViewById(R.id.front_tap_here);
        backTapHere = findViewById(R.id.back_tap_here);
        selfieTapHere = findViewById(R.id.selfie_tap_here);
        panCheckLayout = findViewById(R.id.pan_check_layout);
    }


    @Override
    protected void onResume() {
        super.onResume();

        panCheckLayout.setVisibility(View.VISIBLE);
        if (AwsSdkConstants.INSTANCE.getTOKEN_EXPIRED()) {
            closeApplication(this, getString(R.string.session_expired));
            AwsSdkConstants.INSTANCE.setTOKEN_EXPIRED(false);
        } else {
            String imgType = AwsSdkConstants.INSTANCE.getIMAGE_TYPE();
            if (imgType.equalsIgnoreCase(StringConstants.INSTANCE.AADHAAR_CARD_FRONT) && Objects.equals(mSelectedImage, getString(R.string.aadhaarCardFront))) {
                EkycSdkConstants.aadhaarName = AwsSdkConstants.INSTANCE.getAadhaarFrontDetails().getAadhaarName();
                EkycSdkConstants.dob = AwsSdkConstants.INSTANCE.getAadhaarFrontDetails().getAadhaarDob();
                EkycSdkConstants.gender = AwsSdkConstants.INSTANCE.getAadhaarFrontDetails().getAadhaarGender();
                if (!AwsSdkConstants.INSTANCE.getAadhaarFrontDetails().getAadhaarFatherName().isEmpty()) {
                    EkycSdkConstants.fathersName = AwsSdkConstants.INSTANCE.getAadhaarFrontDetails().getAadhaarFatherName();
                }
                mAadhaarNumber = AwsSdkConstants.INSTANCE.getAadhaarFrontDetails().getAadhaarNumber();
                EkycSdkConstants.frontAadhaarImageUrl = AwsSdkConstants.INSTANCE.getAadhaarFrontDetails().getAadhaarFrontFirebaseUrl();
                EkycSdkConstants.aadhaarFaceImage = AwsSdkConstants.INSTANCE.getAadhaarFrontDetails().getAadhaarFrontImageUrl();
                ivFrontPlaceHolder.setVisibility(View.GONE);
                frontTapHere.setVisibility(View.GONE);
                ivFront.setVisibility(View.VISIBLE);
                removeSelfieDetails();
                Glide.with(this).load(EkycSdkConstants.frontAadhaarImageUrl).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        ivFrontPlaceHolder.setVisibility(View.VISIBLE);
                        frontTapHere.setVisibility(View.VISIBLE);
                        ivFront.setVisibility(View.GONE);
                        ivFront.setImageDrawable(null);
                        showToast(AadhaarVerificationActivity.this, getString(R.string.sdk_error_recapture_image));
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                }).diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter().placeholder(R.drawable.progress_animation).into(ivFront);

                ekycDataTable.setAadhaarFrontImage(EkycSdkConstants.frontAadhaarImageUrl);
                ekycDataTable.setAadhaarDateOfBirth(EkycSdkConstants.dob);
                ekycDataTable.setGender(EkycSdkConstants.gender);
                userEkycViewModel.update(ekycDataTable);

                mAadhaarName = EkycSdkConstants.aadhaarName;
                EkycSdkConstants.aadhaarNumber = mAadhaarNumber.replaceAll("\\s", "");
                showAadhaar(EkycSdkConstants.aadhaarName, EkycSdkConstants.aadhaarNumber, EkycSdkConstants.dob);
            } else if (imgType.equalsIgnoreCase(StringConstants.AADHAAR_CARD_BACK) && Objects.equals(mSelectedImage, getString(R.string.aadhaarCardBack))) {
                EkycSdkConstants.pin = AwsSdkConstants.INSTANCE.getAadhaarBackDetails().getAadhaarPin();
                EkycSdkConstants.city = AwsSdkConstants.INSTANCE.getAadhaarBackDetails().getAadhaarCity();
                EkycSdkConstants.state = AwsSdkConstants.INSTANCE.getAadhaarBackDetails().getAadhaarState();
                EkycSdkConstants.district = AwsSdkConstants.INSTANCE.getAadhaarBackDetails().getAadhaarDistrict();
                EkycSdkConstants.address = AwsSdkConstants.INSTANCE.getAadhaarBackDetails().getAadhaarAddress();
                EkycSdkConstants.aadhaarBackNumber = AwsSdkConstants.INSTANCE.getAadhaarBackDetails().getAadhaarNumber();
                EkycSdkConstants.backAadhaarImageUrl = AwsSdkConstants.INSTANCE.getAadhaarBackDetails().getAadhaarBackFirebaseUrl();
                if (!AwsSdkConstants.INSTANCE.getAadhaarBackDetails().getAadhaarFatherName().isEmpty()) {
                    EkycSdkConstants.fathersName = AwsSdkConstants.INSTANCE.getAadhaarBackDetails().getAadhaarFatherName();
                }
                removeSelfieDetails();
                ivBackPlaceHolder.setVisibility(View.GONE);
                backTapHere.setVisibility(View.GONE);
                ivBack.setVisibility(View.VISIBLE);
                Glide.with(this).load(EkycSdkConstants.backAadhaarImageUrl).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        ivBackPlaceHolder.setVisibility(View.VISIBLE);
                        backTapHere.setVisibility(View.VISIBLE);
                        ivBack.setVisibility(View.GONE);
                        ivBack.setImageDrawable(null);
                        showToast(AadhaarVerificationActivity.this, getString(R.string.sdk_error_recapture_image));
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                }).diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter().placeholder(R.drawable.progress_animation_potrait).into(ivBack);

            } else if (imgType.equalsIgnoreCase(StringConstants.FACE_LIVELINESS) && Objects.equals(mSelectedImage, getString(R.string.faceLiveliness))) {
                String encoded = AwsSdkConstants.INSTANCE.getAadhaarFaceMatchDetails().getSelfieFirebaseUrl();
                if (!AwsSdkConstants.INSTANCE.getAadhaarFaceMatchDetails().getMatchScore().isEmpty()) {
                    mMatchScore = AwsSdkConstants.INSTANCE.getAadhaarFaceMatchDetails().getMatchScore();
                }
                EkycSdkConstants.selfieImageUrl = encoded;
                rlSelfie.setVisibility(View.VISIBLE);
                tvSelfie.setVisibility(View.VISIBLE);
                ivSelfiePlaceHolder.setVisibility(View.GONE);
                selfieTapHere.setVisibility(View.GONE);
                ivSelfie.setVisibility(View.VISIBLE);
                Glide.with(this).load(encoded).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        ivSelfiePlaceHolder.setVisibility(View.VISIBLE);
                        selfieTapHere.setVisibility(View.VISIBLE);
                        ivSelfie.setVisibility(View.GONE);
                        ivSelfie.setImageDrawable(null);
                        showToast(AadhaarVerificationActivity.this, getString(R.string.sdk_error_recapture_image));
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                }).diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter().placeholder(R.drawable.progress_animation_potrait).into(ivSelfie);
            }
            AwsSdkConstants.INSTANCE.setIMAGE_TYPE("");
        }
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();/*to get clicked view id**/
        if (id == R.id.iv_selfie_holder || id == R.id.iv_sefie) {
            mAadhaarNumber = edtAadhaar.getText().toString();
            EkycSdkConstants.aadhaarNumber = mAadhaarNumber;
            mAadhaarName = edtAadhaarName.getText().toString();
            EkycSdkConstants.aadhaarName = mAadhaarName;
            rlSelfie.setVisibility(View.GONE);
            tvSelfie.setVisibility(View.GONE);
            mSelectedImage = getString(R.string.faceLiveliness);
            if (Boolean.TRUE.equals(connectionCheck(this))) {
                intentToSDK(this, mToken, StringConstants.FACE_LIVELINESS);
            }
        } else if (id == R.id.iv_place_holder_front || id == R.id.iv_aadhar_front) {
            EkycSdkConstants.shown = false;
            mSelectedImage = getString(R.string.aadhaarCardFront);
            if (Boolean.TRUE.equals(connectionCheck(this))) {
                intentToSDK(this, mToken, StringConstants.AADHAAR_CARD_FRONT);
            }
        } else if (id == R.id.iv_place_holder_back || id == R.id.iv_aadhar_back) {
            mAadhaarNumber = edtAadhaar.getText().toString();
            EkycSdkConstants.aadhaarNumber = mAadhaarNumber;
            mAadhaarName = edtAadhaarName.getText().toString();
            EkycSdkConstants.aadhaarName = mAadhaarName;
            if (ivFront.getDrawable() == null || frontTapHere.getVisibility() == View.VISIBLE) {
                showErrorAlert(this, getString(R.string.sdk_error_capture_front_image));
            } else {
                mSelectedImage = getString(R.string.aadhaarCardBack);
                if (Boolean.TRUE.equals(connectionCheck(this))) {
                    intentToSDK(this, mToken, StringConstants.AADHAAR_CARD_BACK);
                }
            }
        } else if (id == R.id.btn_submit) {
            mAadhaarNumber = edtAadhaar.getText().toString();
            EkycSdkConstants.aadhaarNumber = mAadhaarNumber;
            mAadhaarName = edtAadhaarName.getText().toString();
            EkycSdkConstants.aadhaarName = mAadhaarName;
            //EkycSdkConstants.dob = edtAadhaarDob.getText().toString();
            if (Boolean.TRUE.equals(connectionCheck(this)) && validate()) {
                String aadhaarNameCheck = mAadhaarName.toUpperCase();
                String PanNameCheck = "";
                PanNameCheck = ekycDataTable.getPanName().toUpperCase();
                Double nameMatchScore = Validate.diceCoefficientOptimized(aadhaarNameCheck, PanNameCheck);
                if (nameMatchScore < 0.7) {
                    showErrorAlert(this, getString(R.string.sdk_error_pan_aadhaar_match));
                    userEkycViewModel.update(ekycDataTable);
                } else if (rlSelfie.getVisibility() == View.VISIBLE) {
                    if (ivSelfie.getDrawable() == null || selfieTapHere.getVisibility() == View.VISIBLE) {
                        showToast(AadhaarVerificationActivity.this, getString(R.string.sdk_error_capture_selfie_image));
                    } else {
                        ekycDataTable.setNameMatchScore(nameMatchScore.intValue());
                        ekycDataTable.setAadhaarNumber(EkycSdkConstants.aadhaarNumber);
                        userEkycViewModel.update(ekycDataTable);
                        validateAndSaveImageUri();
                    }
                } else {
                    ekycDataTable.setAadhaarBackImage(EkycSdkConstants.backAadhaarImageUrl);
                    ekycDataTable.setAadhaarPinCode(EkycSdkConstants.pin);
                    ekycDataTable.setAadhaarAddress(EkycSdkConstants.address);
                    ekycDataTable.setAadhaarCity(EkycSdkConstants.city);
                    ekycDataTable.setAadhaarDistrict(EkycSdkConstants.district);
                    ekycDataTable.setAadhaarName(EkycSdkConstants.aadhaarName);
                    ekycDataTable.setAadhaarNumber(mAadhaarNumber);
                    ekycDataTable.setGender(EkycSdkConstants.gender);
                    ekycDataTable.setSubStatus("4");
                    ekycDataTable.setAadhaarState(EkycSdkConstants.state);
                    ekycDataTable.setNameMatchScore(nameMatchScore.intValue());
                    userEkycViewModel.update(ekycDataTable);
                    mSelectedImage = getString(R.string.faceLiveliness);
                    intentToSDK(this, mToken, StringConstants.FACE_LIVELINESS);
                }
            }
        }
    }

    /**
     * Remove selfie captured details when aadhaar data is extracted
     */
    private void removeSelfieDetails() {
        ivSelfiePlaceHolder.setVisibility(View.VISIBLE);
        selfieTapHere.setVisibility(View.VISIBLE);
        ivSelfie.setVisibility(View.GONE);
        tvSelfie.setVisibility(View.GONE);
        rlSelfie.setVisibility(View.GONE);
        ivSelfie.setImageDrawable(null);
    }


    /**
     * validate input data and start upload to server
     */
    private void validateAndSaveImageUri() {
        ekycDataTable.setSelfieImage(EkycSdkConstants.selfieImageUrl);
        ekycDataTable.setSelfieMatchScore(mMatchScore);
        ekycDataTable.setAadhaarName(EkycSdkConstants.aadhaarName);
        ekycDataTable.setAadhaarNumber(EkycSdkConstants.aadhaarNumber);
        if (EkycSdkConstants.fathersName.isEmpty()) {
            ekycDataTable.setPanFathersName(EkycSdkConstants.fathersName);
        }
        ekycDataTable.setAadhaarDateOfBirth(EkycSdkConstants.dob);
        ekycDataTable.setSubStatus("5");
        userEkycViewModel.update(ekycDataTable);

        Intent intent = OnboardingIntentFactory.returnEkycBasicInfoActivityIntent(AadhaarVerificationActivity.this);
        startActivity(intent);
        finish();
    }

    /**
     * Validate all fields before proceeding
     *
     * @return
     */

    private boolean validate() {
        if (ivFront.getDrawable() == null && ivBack.getDrawable() == null) {
            showErrorAlert(this, getResources().getString(R.string.sdk_aadhar_photo_validation));
            return false;
        } else if (ivFront.getDrawable() == null || frontTapHere.getVisibility() == View.VISIBLE) {
            showErrorAlert(this, getString(R.string.sdk_error_capture_front_image));
            return false;
        } else if (mAadhaarNumber.trim().isEmpty()) {
            showErrorAlert(this, getString(R.string.sdk_error_aadhaar_number));
            return false;
        } else if (mAadhaarName.trim().isEmpty()) {
            showErrorAlert(this, getString(R.string.sdk_error_aadhaar_name));
            return false;
        } else if (mAadhaarName.length() < 5) {
            showErrorAlert(this, getString(R.string.sdk_error_aadhaar_name_length));
            return false;
        } else if (ivBack.getDrawable() == null || backTapHere.getVisibility() == View.VISIBLE) {
            showErrorAlert(this, getString(R.string.sdk_error_capture_back_image));
            return false;
        } else if (mAadhaarNumber.length() < 12) {
            showErrorAlert(this, getString(R.string.sdk_error_aadhaar_number_length));
            return false;
        } else if (!Validate.validateAadharNumber(mAadhaarNumber)) {
            showErrorAlert(this, getString(R.string.sdk_invalid_aadhaar_number));
            return false;
        } else {
            return true;
        }
    }


    /**
     * Show aadhaar details layouts
     */
    public void showAadhaar(String name, String num, String dob) {
        TransitionManager.beginDelayedTransition(aadhaarLayout);
        mAadhaarName = name;
        mAadhaarNumber = num;
        edtAadhaarName.setText(name);
        edtAadhaar.setText(num);
        //edtAadhaarDob.setText(dob);
        editLayoutAadhaar.setVisibility(View.VISIBLE);
        editLayoutAadhaarName.setVisibility(View.VISIBLE);
        //editLayoutAadhaarDob.setVisibility(View.VISIBLE);
        ntAADHAAR.setVisibility(View.VISIBLE);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void datePicker() {
        // Get the current date
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        // Create a DatePickerDialog
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                AadhaarVerificationActivity.this,
                (view, selectedYear, selectedMonth, selectedDay) -> {
                    // Handle the selected date and format it as "dd/MM/yyyy"
                    Calendar selectedCalendar = Calendar.getInstance();
                    selectedCalendar.set(selectedYear, selectedMonth, selectedDay);
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                    String formattedDate = sdf.format(selectedCalendar.getTime());
                    edtAadhaarDob.setText(formattedDate);
                }, year, month, day);

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        // Show the date picker dialog
        datePickerDialog.show();
    }

}
