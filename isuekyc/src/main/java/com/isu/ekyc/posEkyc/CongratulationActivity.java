package com.isu.ekyc.posEkyc;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.isu.ekyc.R;
import com.isu.ekyc.ekycDataBase.EkycDataTable;
import com.isu.ekyc.ekycDataBase.EkycViewModelFactory;
import com.isu.ekyc.ekycDataBase.UserEkycRepository;
import com.isu.ekyc.ekycDataBase.viewmodel.UserEkycViewModel;

public class CongratulationActivity extends AppCompatActivity {

    private TextView tvSuccessMessage;
    private Button login;
    private EkycDataTable ekycDataTable;

    private UserEkycViewModel userEkycViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_congratulation);

        tvSuccessMessage = findViewById(R.id.tv_success_message);
        tvSuccessMessage.setText(R.string.sdk_text_onboarding);
        UserEkycRepository userEkycRepository = new UserEkycRepository(getApplication());
        userEkycViewModel = new ViewModelProvider(this, new EkycViewModelFactory(userEkycRepository)).get(UserEkycViewModel.class);
        ekycDataTable = new EkycDataTable();
        ekycDataTable = userEkycViewModel.getAllData();


        login = findViewById(R.id.login_proceed);
        login.setOnClickListener(v -> {
            ekycDataTable.setSubStatus("0");
            userEkycViewModel.update(ekycDataTable);
            Intent intent = new Intent();
            intent.putExtra("ActivityName", "CompleteOB");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            finishAffinity();
            System.exit(0);
        });
        //On back pressed handled
        OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                finish();
            }
        };
        // Add the callback to the onBackPressedDispatcher
        getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback);
    }

}