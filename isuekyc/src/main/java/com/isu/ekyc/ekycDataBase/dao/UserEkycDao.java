package com.isu.ekyc.ekycDataBase.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.isu.ekyc.ekycDataBase.EkycDataTable;

@Dao
public interface UserEkycDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertData(EkycDataTable ekycDataTable);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateData(EkycDataTable dataTable);

    @Delete
    void deleteData(EkycDataTable dataTable);

    @Query("SELECT * FROM EkycDataTable")
    EkycDataTable getDetails();

    @Query("DELETE FROM EkycDataTable")
    void deleteAllData();
}
