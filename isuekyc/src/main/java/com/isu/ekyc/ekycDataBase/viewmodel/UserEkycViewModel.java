package com.isu.ekyc.ekycDataBase.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.isu.ekyc.ekycDataBase.EkycDataTable;
import com.isu.ekyc.ekycDataBase.UserEkycRepository;

public class UserEkycViewModel extends AndroidViewModel {

    private final UserEkycRepository userEkycRepository;
    private final EkycDataTable data;

    public UserEkycViewModel(@NonNull Application application, UserEkycRepository bbpsRepository) {
        super(application);
        userEkycRepository = bbpsRepository;
        data = userEkycRepository.getAllData();
    }

    public void insert(EkycDataTable data) {
        userEkycRepository.insertData(data);
    }

    public void update(EkycDataTable data) {
        userEkycRepository.updateData(data);
    }

    public void delete() {
        EkycDataTable getAllDataFromRoomDatabase = userEkycRepository.getAllData();
        userEkycRepository.deleteData(getAllDataFromRoomDatabase);
    }

    public void deleteAllData() {
        userEkycRepository.deleteAllData();
    }

    public EkycDataTable getAllData() {
        return data;
    }
}
