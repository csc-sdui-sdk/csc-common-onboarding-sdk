package com.isu.ekyc.ekycDataBase;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.isu.ekyc.ekycDataBase.dao.UserEkycDao;

@Database(entities = {EkycDataTable.class}, version = 3, exportSchema = false)
public abstract class UserEkycDatabase extends RoomDatabase {
    public abstract UserEkycDao ekycDao();

    public static UserEkycDatabase INSTANCE;

    public static UserEkycDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (UserEkycDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context, UserEkycDatabase.class, "EKYC_DATABASE")
                            .allowMainThreadQueries()
                            .fallbackToDestructiveMigration().build();
                }
            }
        }
        return INSTANCE;
    }

    private static final Callback roomdatabase = new Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDBAsyncTask(INSTANCE).execute();
        }
    };

    private static class PopulateDBAsyncTask extends AsyncTask<Void, Void, Void> {
        PopulateDBAsyncTask(UserEkycDatabase instance) {
            UserEkycDao userEkycDao = instance.ekycDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }
    }
}
