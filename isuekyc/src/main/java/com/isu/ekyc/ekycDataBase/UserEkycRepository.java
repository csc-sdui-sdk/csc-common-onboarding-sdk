package com.isu.ekyc.ekycDataBase;

import android.app.Application;
import android.os.AsyncTask;

import com.isu.ekyc.ekycDataBase.dao.UserEkycDao;

public class UserEkycRepository {
    private final UserEkycDao userEkycDao;
    private final EkycDataTable liveData;

    public UserEkycRepository(Application application) {
        UserEkycDatabase db = UserEkycDatabase.getDatabase(application);
        userEkycDao = db.ekycDao();
        liveData = userEkycDao.getDetails();
    }

    public EkycDataTable getAllData() {
        return liveData;
    }

    public void insertData(EkycDataTable dataTable) {
        new DataInsertionAsyncTask(userEkycDao).execute(dataTable);
    }

    public void updateData(EkycDataTable dataTable) {
        new DataUpdateAsyncTask(userEkycDao).execute(dataTable);
    }

    public void deleteData(EkycDataTable dataTable) {
        new DataDeletionAsyncTask(userEkycDao).execute(dataTable);
    }

    public void deleteAllData() {
        new DeleteAllDataAsyncTask(userEkycDao).execute();
    }

    //AsyncTask to insert data
    private static class DataInsertionAsyncTask extends AsyncTask<EkycDataTable, Void, Void> {
        private final UserEkycDao userEkycDao;

        public DataInsertionAsyncTask(UserEkycDao userEkycDao) {
            this.userEkycDao = userEkycDao;
        }

        @Override
        protected Void doInBackground(EkycDataTable... ekycDataTables) {
            userEkycDao.insertData(ekycDataTables[0]);
            return null;
        }
    }

    //AsyncTask to delete data
    private static class DataDeletionAsyncTask extends AsyncTask<EkycDataTable, Void, Void> {
        private final UserEkycDao userEkycDao;

        public DataDeletionAsyncTask(UserEkycDao userEkycDao) {
            this.userEkycDao = userEkycDao;
        }

        @Override
        protected Void doInBackground(EkycDataTable... ekycDataTables) {
            userEkycDao.deleteData(ekycDataTables[0]);
            return null;
        }
    }

    //AsyncTask to update data
    private static class DataUpdateAsyncTask extends AsyncTask<EkycDataTable, Void, Void> {
        private final UserEkycDao userEkycDao;

        public DataUpdateAsyncTask(UserEkycDao userEkycDao) {
            this.userEkycDao = userEkycDao;
        }

        @Override
        protected Void doInBackground(EkycDataTable... ekycDataTables) {
            userEkycDao.updateData(ekycDataTables[0]);
            return null;
        }
    }

    //AsyncTask to delete all data
    private static class DeleteAllDataAsyncTask extends AsyncTask<EkycDataTable, Void, Void> {
        private final UserEkycDao userEkycDao;

        public DeleteAllDataAsyncTask(UserEkycDao userEkycDao) {
            this.userEkycDao = userEkycDao;
        }

        @Override
        protected Void doInBackground(EkycDataTable... ekycDataTables) {
            userEkycDao.deleteAllData();
            return null;
        }
    }
}
