package com.isu.ekyc.ekycDataBase;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.isu.ekyc.ekycDataBase.viewmodel.UserEkycViewModel;


public class EkycViewModelFactory implements ViewModelProvider.Factory {
    UserEkycRepository bbpsRepository;

    public EkycViewModelFactory(UserEkycRepository bbpsRepository) {
        this.bbpsRepository = bbpsRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new UserEkycViewModel(new Application(),bbpsRepository);
    }
}