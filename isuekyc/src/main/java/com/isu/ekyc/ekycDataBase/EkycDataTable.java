package com.isu.ekyc.ekycDataBase;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "EkycDataTable")
public class EkycDataTable {
//    @PrimaryKey(autoGenerate = true)
//    @ColumnInfo(name = "Id")
//    private int id;

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "Username")
    private String Username;

    @ColumnInfo(name = "Type")
    private String Type="";

    @ColumnInfo(name = "SubStatus")
    private String SubStatus;

    @ColumnInfo(name = "Phone")
    private String Phone;

    @ColumnInfo(name = "PhoneOtp")
    private String PhoneOtp;

    @ColumnInfo(name = "Email")
    private String Email;

    @ColumnInfo(name = "EmailOtp")
    private String EmailOtp;

    @ColumnInfo(name = "PanNumber")
    private String PanNumber;

    @ColumnInfo(name = "PanName")
    private String PanName;

    @ColumnInfo(name = "PanDateOfBirth")
    private String PanDateOfBirth;

    @ColumnInfo(name = "PanImage")
    private String PanImage;

    @ColumnInfo(name = "PanFathersName")
    private String PanFathersName;

    @ColumnInfo(name = "AadhaarNumber")
    private String AadhaarNumber;

    @ColumnInfo(name = "AadhaarName")
    private String AadhaarName;

    @ColumnInfo(name = "AadhaarAddress")
    private String AadhaarAddress;

    @ColumnInfo(name = "AadhaarState")
    private String AadhaarState;

    @ColumnInfo(name = "AadhaarCity")
    private String AadhaarCity;

    @ColumnInfo(name = "AadhaarDistrict")
    private String AadhaarDistrict;

    @ColumnInfo(name = "AadhaarPinCode")
    private String AadhaarPinCode;

    @ColumnInfo(name = "AadhaarFrontImage")
    private String AadhaarFrontImage;

    @ColumnInfo(name = "AadhaarBackImage")
    private String AadhaarBackImage;

    @ColumnInfo(name = "AadhaarDateOfBirth")
    private String AadhaarDateOfBirth;

    @ColumnInfo(name = "NameMatchScore")
    private int NameMatchScore;

    @ColumnInfo(name = "SelfieImage")
    private String SelfieImage;

    @ColumnInfo(name = "SelfieMatchScore")
    private String SelfieMatchScore;

    @ColumnInfo(name = "ShopOutsideImage")
    private String ShopOutsideImage;

    @ColumnInfo(name = "ShopInsideImage")
    private String ShopInsideImage;

    @ColumnInfo(name = "ShopName")
    private String ShopName;

    @ColumnInfo(name = "ShopAddress")
    private String ShopAddress;

    @ColumnInfo(name = "ShopPin")
    private String ShopPin;

    @ColumnInfo(name = "GstNumber")
    private String GstNumber;

    @ColumnInfo(name = "Income")
    private String Income;

    @ColumnInfo(name = "BankName")
    private String BankName;

    @ColumnInfo(name = "BankIFSC")
    private String BankIFSC;

    @ColumnInfo(name = "BankMCC")
    private String BankMCC;

    @ColumnInfo(name = "ReferralCode")
    private String ReferralCode;

    @ColumnInfo(name = "ReferralName")
    private String ReferralName;

    @ColumnInfo(name = "Doi")
    private String Doi;

    public String getDoi() {
        return Doi;
    }

    public void setDoi(String doi) {
        Doi = doi;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    @ColumnInfo(name = "Gender")
    private String Gender;

//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getSubStatus() {
        return SubStatus;
    }

    public void setSubStatus(String subStatus) {
        SubStatus = subStatus;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getPhoneOtp() {
        return PhoneOtp;
    }

    public void setPhoneOtp(String phoneOtp) {
        PhoneOtp = phoneOtp;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getEmailOtp() {
        return EmailOtp;
    }

    public void setEmailOtp(String emailOtp) {
        EmailOtp = emailOtp;
    }

    public String getPanNumber() {
        return PanNumber;
    }

    public void setPanNumber(String panNumber) {
        PanNumber = panNumber;
    }

    public String getPanName() {
        return PanName;
    }

    public void setPanName(String panName) {
        PanName = panName;
    }

    public String getPanDateOfBirth() {
        return PanDateOfBirth;
    }

    public void setPanDateOfBirth(String panDateOfBirth) {
        PanDateOfBirth = panDateOfBirth;
    }

    public String getPanImage() {
        return PanImage;
    }

    public void setPanImage(String panImage) {
        PanImage = panImage;
    }

    public String getPanFathersName() {
        return PanFathersName;
    }

    public void setPanFathersName(String panFathersName) {
        PanFathersName = panFathersName;
    }

    public String getAadhaarNumber() {
        return AadhaarNumber;
    }

    public void setAadhaarNumber(String aadhaarNumber) {
        AadhaarNumber = aadhaarNumber;
    }

    public String getAadhaarName() {
        return AadhaarName;
    }

    public void setAadhaarName(String aadhaarName) {
        AadhaarName = aadhaarName;
    }

    public String getAadhaarAddress() {
        return AadhaarAddress;
    }

    public void setAadhaarAddress(String aadhaarAddress) {
        AadhaarAddress = aadhaarAddress;
    }

    public String getAadhaarState() {
        return AadhaarState;
    }

    public void setAadhaarState(String aadhaarState) {
        AadhaarState = aadhaarState;
    }

    public String getAadhaarCity() {
        return AadhaarCity;
    }

    public void setAadhaarCity(String aadhaarCity) {
        AadhaarCity = aadhaarCity;
    }

    public String getAadhaarDistrict() {
        return AadhaarDistrict;
    }

    public void setAadhaarDistrict(String aadhaarDistrict) {
        AadhaarDistrict = aadhaarDistrict;
    }

    public String getAadhaarPinCode() {
        return AadhaarPinCode;
    }

    public void setAadhaarPinCode(String aadhaarPinCode) {
        AadhaarPinCode = aadhaarPinCode;
    }

    public String getAadhaarFrontImage() {
        return AadhaarFrontImage;
    }

    public void setAadhaarFrontImage(String aadhaarFrontImage) {
        AadhaarFrontImage = aadhaarFrontImage;
    }

    public String getAadhaarBackImage() {
        return AadhaarBackImage;
    }

    public void setAadhaarBackImage(String aadhaarBackImage) {
        AadhaarBackImage = aadhaarBackImage;
    }

    public String getAadhaarDateOfBirth() {
        return AadhaarDateOfBirth;
    }

    public void setAadhaarDateOfBirth(String aadhaarDateOfBirth) {
        AadhaarDateOfBirth = aadhaarDateOfBirth;
    }

    public int getNameMatchScore() {
        return NameMatchScore;
    }

    public void setNameMatchScore(int nameMatchScore) {
        NameMatchScore = nameMatchScore;
    }

    public String getSelfieImage() {
        return SelfieImage;
    }

    public void setSelfieImage(String selfieImage) {
        SelfieImage = selfieImage;
    }

    public String getSelfieMatchScore() {
        return SelfieMatchScore;
    }

    public void setSelfieMatchScore(String selfieMatchScore) {
        SelfieMatchScore = selfieMatchScore;
    }

    public String getShopOutsideImage() {
        return ShopOutsideImage;
    }

    public void setShopOutsideImage(String shopOutsideImage) {
        ShopOutsideImage = shopOutsideImage;
    }

    public String getShopInsideImage() {
        return ShopInsideImage;
    }

    public void setShopInsideImage(String shopInsideImage) {
        ShopInsideImage = shopInsideImage;
    }

    public String getShopName() {
        return ShopName;
    }

    public void setShopName(String shopName) {
        ShopName = shopName;
    }

    public String getShopAddress() {
        return ShopAddress;
    }

    public void setShopAddress(String shopAddress) {
        ShopAddress = shopAddress;
    }

    public String getShopPin() {
        return ShopPin;
    }

    public void setShopPin(String shopPin) {
        ShopPin = shopPin;
    }

    public String getGstNumber() {
        return GstNumber;
    }

    public void setGstNumber(String gstNumber) {
        GstNumber = gstNumber;
    }

    public String getIncome() {
        return Income;
    }

    public void setIncome(String income) {
        Income = income;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getBankIFSC() {
        return BankIFSC;
    }

    public void setBankIFSC(String bankIFSC) {
        BankIFSC = bankIFSC;
    }

    public String getBankMCC() {
        return BankMCC;
    }

    public void setBankMCC(String bankMCC) {
        BankMCC = bankMCC;
    }

    public String getReferralCode() {
        return ReferralCode;
    }

    public void setReferralCode(String referralCode) {
        ReferralCode = referralCode;
    }

    public String getReferralName() {
        return ReferralName;
    }

    public void setReferralName(String referralName) {
        ReferralName = referralName;
    }

}
