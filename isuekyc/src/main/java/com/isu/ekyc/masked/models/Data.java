package com.isu.ekyc.masked.models;

import com.google.gson.annotations.SerializedName;

public class Data {
    @SerializedName("image")
    private String image;

    public String getImage(){
        return image;
    }

}

