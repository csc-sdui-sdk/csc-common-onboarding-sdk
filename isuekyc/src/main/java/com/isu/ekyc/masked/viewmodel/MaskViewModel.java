package com.isu.ekyc.masked.viewmodel;

import androidx.lifecycle.MutableLiveData;

import com.isu.ekyc.masked.models.MaskReqModel;
import com.isu.ekyc.masked.models.MaskResModel;
import com.isu.ekyc.masked.repository.MaskRepo;

public class MaskViewModel {
    private final MaskRepo maskRepo;
    private static MutableLiveData<MaskResModel> mutableLiveData = new MutableLiveData<>();

    public MaskViewModel() {
        maskRepo = new MaskRepo();
    }

    public MutableLiveData<MaskResModel> getMaskcheck(MaskReqModel dataRequestApi) {
        try {
            mutableLiveData = maskRepo.reqMaskAadhaarNum(dataRequestApi);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return mutableLiveData;
    }
}
