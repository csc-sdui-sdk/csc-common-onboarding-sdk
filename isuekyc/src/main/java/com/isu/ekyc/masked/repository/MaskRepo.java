package com.isu.ekyc.masked.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.isu.ekyc.masked.models.MaskReqModel;
import com.isu.ekyc.masked.models.MaskResModel;
import com.isu.ekyc.masked.networkmodule.MaskApiService;
import com.isu.ekyc.masked.networkmodule.MaskRetro;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MaskRepo {
    public MutableLiveData<MaskResModel> reqMaskAadhaarNum(MaskReqModel genericfundMERequest) {
        final MutableLiveData<MaskResModel> mutableLiveData = new MutableLiveData<>();
        MaskApiService maskApiService = MaskRetro.MaskAadhaarInstance().create(MaskApiService.class);
        Call<MaskResModel> fetchRespo = maskApiService.reqMaskNum(genericfundMERequest);
        fetchRespo.enqueue(new Callback<MaskResModel>() {
            @Override
            public void onResponse(Call<MaskResModel> call, Response<MaskResModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    mutableLiveData.setValue(response.body());
                }else{
                    mutableLiveData.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<MaskResModel> call, Throwable t) {
                mutableLiveData.setValue(null);
                //throw error
                Log.e("TAG", "onFailure: " + t.getMessage());
            }
        });
        return mutableLiveData;
    }
}
