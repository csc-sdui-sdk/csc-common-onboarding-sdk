package com.isu.ekyc.masked.models;

import com.google.gson.annotations.SerializedName;

public class MaskResModel{



	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private Integer status;

	@SerializedName("data")
	private Data data;


	public Data getData() {
		return data;
	}

	public String getMessage(){
		return message;
	}

	public Integer getStatus(){
		return status;
	}
}