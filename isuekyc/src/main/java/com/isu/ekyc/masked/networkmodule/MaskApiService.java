package com.isu.ekyc.masked.networkmodule;

import com.isu.ekyc.masked.models.MaskReqModel;
import com.isu.ekyc.masked.models.MaskResModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface MaskApiService {

    @POST("/aadhaarmasking")
    Call<MaskResModel> reqMaskNum(@Body MaskReqModel maskReqModel);
}
