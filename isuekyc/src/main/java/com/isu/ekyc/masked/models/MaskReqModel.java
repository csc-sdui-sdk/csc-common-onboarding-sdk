package com.isu.ekyc.masked.models;

import com.google.gson.annotations.SerializedName;

public class MaskReqModel{
	public MaskReqModel(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	@SerializedName("imageUrl")
	private String imageUrl;

	public String getImageUrl(){
		return imageUrl;
	}
}