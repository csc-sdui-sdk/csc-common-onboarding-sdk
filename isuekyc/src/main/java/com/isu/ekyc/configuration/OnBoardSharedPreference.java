package com.isu.ekyc.configuration;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class OnBoardSharedPreference {
    private static final String USER_PREFS = "ISERVEU";
    private SharedPreferences appSharedPrefs;
    private SharedPreferences.Editor prefsEditor;

    public OnBoardSharedPreference(Context context){
        this.appSharedPrefs = context.getSharedPreferences(USER_PREFS, Activity.MODE_PRIVATE);
        this.prefsEditor = appSharedPrefs.edit();
    }

    public String getStringValue(String key){
        return appSharedPrefs.getString(key, "");
    }

    public void setStringValue(String key, String value){
        prefsEditor.putString(key, value).commit();
    }

    public void clearBoardingPref(){
        prefsEditor.clear();
        prefsEditor.apply();
    }
}
