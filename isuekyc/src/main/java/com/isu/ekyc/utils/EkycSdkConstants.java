package com.isu.ekyc.utils;

public class EkycSdkConstants {
    public static String SIGNUP_URL = "https://self-signup-ca-prod.iserveu.tech/user/";
    public static String FETCH_URL = "https://sdui-module-prod.iserveu.tech/";
    public static String PACKAGE_NAME = "";
    public static String frontAadhaarImageUrl = "";
    public static String backAadhaarImageUrl = "";
    public static String selfieImageUrl = "";
    public static String pin = "";
    public static String city = "";
    public static String state = "";
    public static String address = "";
    public static String aadhaarName = "";
    public static String aadhaarNumber = "";
    public static String dob = "";
    public static String fathersName = "";

    public static String gender = "";
    public static String aadhaarFaceImage = "";

    public static String Status = "";

    public static String USER_NAME = "";
    public static String ADMIN_NAME = "";
    public static String BANK_CODE = "";

    public static String OFAC_STATUS = "";
    public static String FETCH_MERCHANT_STATUS = "";
    public static String COMMONONBOARDING = "";

    public static String USER_MOBILE_NO = "";

    public static String USER_EMAIL = "";

    public static String ekycStatus = "";
    public static String panName = "";
    public static String district = "";
    public static String aadhaarBackNumber = "";


    /**
     * Onboarding details
     */

    public static String TYPE = "";
    public static String NAME = "";
    public static boolean shown = false;
    public static String EkycStatusCode = "";
    public static boolean UpdateOnboardFlag = false;

    /**
     * URL
     */
    public static String sSendOtpUrl = "";
    public static String sVerifyOtpUrl = "";
    public static String sSendEmailUrl = "";
    public static String sVerifyEmailUrl = "";
    public static String sValidatePanUrl = "";
    public static String sOnboardUrl = "";
    public static String sFetchStatusUrl = "";
    public static String sResendOtpUrl = "";
    public static String sUpdateDetailsUrl = "";
    public static String sPinValidateUrl = "";
    public static String sFetchStateLocationUrl = "";
}
