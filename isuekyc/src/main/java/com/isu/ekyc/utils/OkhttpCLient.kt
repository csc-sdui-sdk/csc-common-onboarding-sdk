package com.isu.ekyc.utils

import com.isu.ekyc.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

class OkhttpCLient {
    val client: OkHttpClient
        get() {
            val loggingInterceptor = HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.BODY)
            val client: OkHttpClient.Builder = OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
            if (BuildConfig.DEBUG) client.addInterceptor(loggingInterceptor)
            return client.build()
        }

    companion object {
        var client: OkHttpClient? = OkhttpCLient().client
    }
}