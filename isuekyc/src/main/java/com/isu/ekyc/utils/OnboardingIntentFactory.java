package com.isu.ekyc.utils;

import android.content.Context;
import android.content.Intent;

import com.isu.ekyc.checkStatus.NewAadhaarVerificationActivity;
import com.isu.ekyc.checkStatus.NewEmailVerificationActivity;
import com.isu.ekyc.checkStatus.NewGSTActivity;
import com.isu.ekyc.checkStatus.NewPanVerificationActivity;
import com.isu.ekyc.checkStatus.NewVerifyActivity;
import com.isu.ekyc.emailverfication.EmailVerificationActivity;
import com.isu.ekyc.otpverification.PhoneNumberActivity;
import com.isu.ekyc.otpverification.VerifyOtpActivity;
import com.isu.ekyc.otpverification.WelcomeActivity;
import com.isu.ekyc.posEkyc.AadhaarVerificationActivity;
import com.isu.ekyc.posEkyc.CongratulationActivity;
import com.isu.ekyc.posEkyc.EkycAgreementActivity;
import com.isu.ekyc.posEkyc.EkycBasicInfoActivity;
import com.isu.ekyc.posEkyc.PanVerificationActivity;

public class OnboardingIntentFactory {
    private OnboardingIntentFactory() {
    }
    /**
     * @param context of that activity from where it will call
     * @return to PhoneNumberActivity
     */
    public static Intent returnPhoneNumberActivityIntent(Context context) {
        return new Intent(context, PhoneNumberActivity.class);
    }
    /**
     * @param context of that activity from where it will call
     * @return to EmailVerificationActivity
     */
    public static Intent returnEmailVerificationActivityIntent(Context context) {
        return new Intent(context, EmailVerificationActivity.class);
    }
    /**
     * @param context of that activity from where it will call
     * @return to VerifyOtpActivity
     */
    public static Intent returnVerifyOtpActivityIntent(Context context) {
        return new Intent(context, VerifyOtpActivity.class);
    }
    /**
     * @param context of that activity from where it will call
     * @return to WelcomeActivity
     */
    public static Intent returnWelcomeActivityIntent(Context context) {
        return new Intent(context, WelcomeActivity.class);
    }
    /**
     * @param context of that activity from where it will call
     * @return to EkycAgreementActivity
     */
    public static Intent returnEkycAgreementActivityIntent(Context context) {
        return new Intent(context, EkycAgreementActivity.class);
    }
    /**
     * @param context of that activity from where it will call
     * @return to PanVerificationActivity
     */
    public static Intent returnPanVerificationActivityIntent(Context context) {
        return new Intent(context, PanVerificationActivity.class);
    }
    /**
     * @param context of that activity from where it will call
     * @return to AadhaarVerificationActivity
     */
    public static Intent returnAadhaarVerificationActivityIntent(Context context) {
        return new Intent(context, AadhaarVerificationActivity.class);
    }
    /**
     * @param context of that activity from where it will call
     * @return to EkycBasicInfoActivity
     */
    public static Intent returnEkycBasicInfoActivityIntent(Context context) {
        return new Intent(context, EkycBasicInfoActivity.class);
    }
    /**
     * @param context of that activity from where it will call
     * @return to CongratulationActivity
     */
    public static Intent returnCongratulationActivityIntent(Context context) {
        return new Intent(context, CongratulationActivity.class);
    }
    /**
     * @param context of that activity from where it will call
     * @return to NewEmailVerificationActivity
     */
    public static Intent returnNewEmailVerificationActivityIntent(Context context) {
        return new Intent(context, NewEmailVerificationActivity.class);
    }
    /**
     * @param context of that activity from where it will call
     * @return to NewVerifyActivity
     */
    public static Intent returnNewVerifyActivityIntent(Context context) {
        return new Intent(context, NewVerifyActivity.class);
    }
    /**
     * @param context of that activity from where it will call
     * @return to NewPanVerificationActivity
     */
    public static Intent returnNewPanVerificationActivityIntent(Context context) {
        return new Intent(context, NewPanVerificationActivity.class);
    }
    /**
     * @param context of that activity from where it will call
     * @return to NewAadhaarVerificationActivity
     */
    public static Intent returnNewAadhaarVerificationActivityIntent(Context context) {
        return new Intent(context, NewAadhaarVerificationActivity.class);
    }
    /**
     * @param context of that activity from where it will call
     * @return to NewGSTActivity
     */
    public static Intent returnNewGSTActivityIntent(Context context) {
        return new Intent(context, NewGSTActivity.class);
    }
}
