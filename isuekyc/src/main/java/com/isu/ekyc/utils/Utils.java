package com.isu.ekyc.utils;

import static com.isu.ekyc.utils.CSCStringConstants.FAULT;
import static com.isu.ekyc.utils.CSCStringConstants.FAULT_STRING;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.icu.util.Calendar;
import android.os.Build;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.google.gson.JsonParseException;
import com.isu.awssdk.core.AwsSdkConstants;
import com.isu.awssdk.core.StringConstants;
import com.isu.awssdk.ui.AwsSdkActivity;
import com.isu.ekyc.R;
import com.isu.ekyc.emailverfication.EmailVerificationActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import okhttp3.ResponseBody;

public class Utils {

    public static Dialog progressDialog;

    /**
     * show progress dialog
     *
     * @param context pass context
     * @param message pass message to show in dialog
     */
    public static void showProgressDialog(Context context, String message) {
        if (context != null) {
            if (progressDialog == null) { // Initialize the ProgressDialog if it's null
                progressDialog = new Dialog(context);
                progressDialog.setContentView(R.layout.dialog_loading);
                Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                TextView tvMessage = progressDialog.findViewById(R.id.tvLoaderMessage);
                tvMessage.setText(message);
            }

            if (!progressDialog.isShowing()) {
                progressDialog.create();
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        }
    }

    /**
     * Hide progress dialog
     */
    public static void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null; // Release the ProgressDialog reference
        }
    }


    /**
     * show Toast message
     *
     * @param context pass context
     * @param message pass message to show
     */
    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * To close application with valid reason
     *
     * @param context pass context
     * @param message pass message to show
     */
    public static void closeApplication(Context context, String message) {
        new AlertDialog.Builder(context, R.style.AlertDialogTheme)
                .setTitle("")
                .setCancelable(false)
                .setMessage(message)
                .setPositiveButton(context.getString(R.string.sdk_ok_btn), (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    ((Activity) context).finishAffinity();
                })
                .show();
    }

    /**
     * To confirm before closing the application
     *
     * @param context pass context
     * @param message pass message to show
     */
    public static void exitTheProcess(Context context, String message) {
        new AlertDialog.Builder(context, R.style.AlertDialogTheme)
                .setTitle(context.getString(R.string.sdk_alert))
                .setCancelable(false)
                .setMessage(message)
                .setPositiveButton(context.getString(R.string.sdk_yes_btn), (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    ((Activity) context).finishAffinity();
                })
                .setNegativeButton(context.getString(R.string.sdk_no_btn), (dialogInterface, i) -> dialogInterface.dismiss())
                .show();
    }

    /**
     * To show error alert dialog
     *
     * @param context pass context
     * @param message pass message to show
     */
    public static void showErrorAlert(Context context, String message) {
        new AlertDialog.Builder(context, R.style.AlertDialogTheme)
                .setTitle(context.getString(R.string.sdk_alert))
                .setCancelable(false)
                .setMessage(message)
                .setPositiveButton(context.getString(R.string.sdk_ok_btn), (dialogInterface, i) -> dialogInterface.dismiss())
                .show();
    }

    /**
     * hide keyboard pop-up
     */
    public static void hideKeyboard(Context context, View view) {
        if (view != null) {
            InputMethodManager manager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            manager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * format date
     *
     * @param date
     * @return
     */
    public static String formatDate(String getDate) {
        String pattern = "dd/MM/yyyy";
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Date date = null;
        if (getDate != null) {
            try {
                date = simpleDateFormat.parse(getDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        return formatter.format(date);
    }

    /**
     * Check internet connection and dismiss dialog
     */
    public static Boolean connectionCheck(Context context) {
        ConnectionDetector connectionDetector = new ConnectionDetector(context);
        if (connectionDetector.isConnectingToInternet()) {
            return true;
        } else {
            if (context != null) {
                new AlertDialog.Builder(context, R.style.AlertDialogTheme)
                        .setTitle(context.getString(R.string.sdk_alert))
                        .setCancelable(false)
                        .setMessage(context.getString(R.string.sdk_no_internet_connection))
                        .setPositiveButton(context.getString(R.string.retry), (dialogInterface, i) -> {
                            if (connectionCheck(context)) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setNegativeButton(context.getString(R.string.sdk_exit), (dialogInterface, i) -> {
                            dialogInterface.dismiss();
                            ((Activity) context).finishAffinity();
                        })
                        .show();
            }
            return false;
        }
    }

    /**
     * Intent to AWS SDK
     */
    public static void intentToSDK(Context context, String token, String key) {
        AwsSdkConstants.INSTANCE.setADMIN_NAME(EkycSdkConstants.ADMIN_NAME);
        AwsSdkConstants.INSTANCE.setUSER_NAME(EkycSdkConstants.USER_NAME);
        AwsSdkConstants.INSTANCE.setMOBILE(EkycSdkConstants.USER_MOBILE_NO);
        AwsSdkConstants.INSTANCE.setTOKEN(token);
        Intent intent = new Intent(context, AwsSdkActivity.class);
        intent.putExtra(StringConstants.IMAGE_TYPE_KEY, key);
        context.startActivity(intent);
    }


    /**
     * Handle token expired fault response
     *
     * @param errorBody
     * @return
     */
    public static void handleErrorResponse(Context context, ResponseBody errorBody) {
        if (errorBody != null) {
            try {
                String errorBodyString = errorBody.string();
                JSONObject errorObject = new JSONObject(errorBodyString);

                if (errorObject.has(FAULT)) {
                    JSONObject faultObject = errorObject.getJSONObject(FAULT);
                    if (faultObject.has(FAULT_STRING)) {
                        closeApplication(context, context.getString(R.string.session_expired));
                    } else {
                        showToast(context, context.getString(R.string.went_wrong));
                    }
                } else {
                    showToast(context, context.getString(R.string.went_wrong));
                }

            } catch (IOException | JSONException | JsonParseException e) {
                showToast(context, context.getString(R.string.went_wrong));
            }
        } else {
            showToast(context, context.getString(R.string.went_wrong));
        }
    }


}
