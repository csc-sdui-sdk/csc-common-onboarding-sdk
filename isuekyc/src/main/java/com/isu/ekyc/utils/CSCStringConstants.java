package com.isu.ekyc.utils;

public class CSCStringConstants {
    private CSCStringConstants() {
        throw new IllegalStateException("Utility class");
    }
    public static final String DATA = "data";
    public static final String STATE_NAME = "state_name";
    public static final String STATUS_CODE = "statusCode";
    public static final String STATUS = "status";
    public static final String STATUS_DESC = "statusDesc";
    public static final String CITY = "city";
    public static final String PIN = "pin";
    //Validate pan response key
    public static final String PAN_INQUIRY_RESPONSE = "panInquiryResponse";
    public static final String BODY = "Body";
    public static final String PAN_DETAILS = "panDetails";
    public static final String FIRST_NAME = "firstname";
    public static final String LAST_NAME = "lastname";
    public static final String MIDDLE_NAME = "middlename";
    public static final String LAST_UPDATE_DATE = "last-update-date";
    public static final String PAN_1 = "pan1";
    public static final String MESSAGE_STRING = "message";
    public static final String PAN_NUMBER = "pan";

    //Fetch status request response key
    public static final String SUB_STATUS_0 = "0";
    public static final String SUB_STATUS_1 = "1";
    public static final String SUB_STATUS_2 = "2";
    public static final String SUB_STATUS_3 = "3";
    public static final String SUB_STATUS_4 = "4";
    public static final String SUB_STATUS_5 = "5";
    public static final String SUB_STATUS_6 = "6";
    public static final String NULL_CHECK = "null";
    public static final String DESCRIPTION = "Description";
    public static final String REMARKS = "remarks";
    public static final String CODE = "code";
    public static final String REUPLOAD = "REUPLOAD";
    public static final String REJECTED = "REJECTED";
    public static final String APPROVED = "APPROVED";
    public static final String PENDING = "PENDING";
    public static final String COMMON_ONBOARD_STATUS = "common_onboard_status";
    public static final String COMMENTS = "Comments";
    public static final String STEPS = "steps";
    public static final String COMMON_ONBOARDING = "common_onboarding";
    public static final String KYC_IS = "Your KYC is";

    //Phone number keys
    public static final String PARAM_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    public static final String OTP_SMALL = "otp";
    public static final String VERIFY_OTP_MOBILE_NO = "mobileNo";
    public static final String PARAMS = "params";
    public static final String MOBILE = "MobileNumber";
    public static final String EMAIL_REQUEST = "Email";
    public static final String WHATS_APP_MOBILE = "WhatsappMobile";
    public static final String MESSAGE_DATA = "messageData";
    public static final String USER_NAME = "user_name";
    public static final String ADMIN_NAME = "admin_name";
    public static final String FEATURE_NAME = "feature_name";
    public static final String OPERATION_PERFORMED = "operation_performed";
    public static final String OTP_CAPS = "OTP";
    //Email key
    public static final String EMAIL_SMALL = "email";
    public static final String EXISTING = "existing";
    public static final String FAULT = "fault";
    public static final String FAULT_STRING = "faultstring";
}
